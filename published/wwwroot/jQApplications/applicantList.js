﻿
    $(document).ready(function () {
        $('#datatable').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });

            var table = $('#datatable').DataTable();

            // Edit record
            table.on('click', '.edit', function () {
        $tr = $(this).closest('tr');

                var data = table.row($tr).data();
                alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
            });

            // Delete a record
            table.on('click', '.remove', function (e) {
        $tr = $(this).closest('tr');
                var data = table.row($tr).data();
                var id = data[1];
                //alert(data[1]);
                swal({
        title: "Are you sure you want to cancel this application?",
                    text: "Confirmation",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: false
                }, function () {
        swal({
            title: 'Please Wait..!',
            text: 'Processing',
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
            showCancelButton: false,
            showConfirmButton: false,
            onOpen: () => {
                swal.showLoading()
            }
        })
                        $.ajax({
        type: "POST",
                            url: "@Url.Action('UpdateStatus', 'JobApplications')",
                            data: {
        status: "cancelled",
                                id: id
                            },
                            success: function (data) {
        swal({
            title: "Cancelled!",
            text: "",
            type: "success",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "OK",
            closeOnConfirm: true
        }, function () {
            location.reload();
        });
                            }
                        })
                });
                //table.row($tr).remove().draw();
                //e.preventDefault();
            });

            //Like record
            table.on('click', '.like', function () {
        alert('You clicked on Like button');
            });
        });
