﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using Recruitment.Models;
using Recruitment.Models.IRepo;
using Recruitment.Models.Repo;

namespace Recruitment
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddSingleton<IMongoClient>(s => new MongoClient(Configuration.GetConnectionString("MongoDb")));
            services.AddDbContext<SQLCon>(options => options.UseSqlServer(Configuration.GetConnectionString("sqlCon"), sqlServerOptions => sqlServerOptions.CommandTimeout(300)));
            services.AddDbContext<AppDbContext>();

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
             .AddCookie(options =>
             {
                 options.ExpireTimeSpan = TimeSpan.FromDays(7);
                 options.LoginPath = "/Account/Login";
             }
             );

         

            //services.AddIdentity<appPersonalData, IdentityRole>(opt =>
            //{
            //    //opt.Password.RequiredLength = 7;
            //    //opt.Password.RequireDigit = false;
            //    //opt.Password.RequireUppercase = false;

            //    //opt.User.RequireUniqueEmail = true;

            //    opt.SignIn.RequireConfirmedEmail = true;
            //});

            services.AddTransient<iUserManager, UserManager>();
            services.AddTransient<iAccountRepository, AccountRepository>();
            services.AddTransient<iRegistration, Registration>();
            services.AddTransient<iFamily, FamilyRepository>();
            services.AddTransient<iEducation, EducationRepository>();
            services.AddTransient<iEmployment, EmploymentRepository>();
            services.AddTransient<iTraining, TrainingRepository>();
            services.AddTransient<iMembership, MembershipRepository>();
            services.AddTransient<iAwards, AwardsRepository>();
            services.AddTransient<iJobVacancies, JobVancaciesRepository>();
            services.AddTransient<iAdminUsers, AdminUsersRepository>();
            services.AddTransient<iBranch, BranchRepository>();
            services.AddTransient<iJobApplications, JobApplicationsRepository>();
            services.AddTransient<iSkills, SkillsRepository>();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Account}/{action=Login}/{id?}");
            });
        }
    }
}
