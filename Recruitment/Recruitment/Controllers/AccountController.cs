﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Recruitment.Models;
using Recruitment.Models.IRepo;
using Recruitment.Email;
using System.IO;
using System.Text;
using System.Web;
using Microsoft.AspNetCore.Html;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Recruitment.Controllers
{
    public class AccountController : Controller
    {
        private iAccountRepository _repoAccount;
        private iUserManager _userManager;
        private iRegistration _repoReg;
        iJobVacancies _jobVac;

        public AccountController(iAccountRepository repoAccount, iUserManager userManager,
            iRegistration repoReg, iJobVacancies jobVac)
        {
            _repoAccount = repoAccount;
            _userManager = userManager;
            _repoReg = repoReg;
            _jobVac = jobVac;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Login(string returnUrl)
        {
            return View(new LoginViewModel
            {
                ReturnUrl = returnUrl
            });
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        [AllowAnonymous]
        public IActionResult Login(LoginViewModel lv, string returnUrl = null)
        {
            this.ViewData["ReturnUrl"] = returnUrl;

            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("Error", "User name/password not found");
                return View(lv);
            }

            var user = _userManager.Validate(lv.email);

            if (user != null)
            {
                var model = _repoAccount.login(lv.email);
                var hashedPassword = model.password;
                bool validPassword = BCrypt.Net.BCrypt.Verify(lv.password, hashedPassword);

                if (validPassword)
                {
                    _userManager.SignIn(this.HttpContext, user, false);

                    var d = User.Identity.IsAuthenticated;
                    Console.Write("auth: " + d);
                    //if (string.IsNullOrEmpty(lv.ReturnUrl))
                    //    return RedirectToAction("Index", "Home");

                    //return View(lv.ReturnUrl);
                    var s = _userManager.GetCurrentUser(this.HttpContext).email.ToString();
                    if (!String.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                        return Redirect(returnUrl);
                    else
                    if (model.isAdmin == true)
                        return RedirectToAction("Dashboard", "Home");
                    return RedirectToAction("Index", "Home");

                }
            }

            ModelState.AddModelError("Error", "User name/password not found");
            return View(lv);

        }


        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Register(RegistrationViewModel r)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            var isIDexist = _repoReg.checkAlreadyRegistered(r.email);
            if (isIDexist)
            {
                ViewBag.Message = "Exist";
                ModelState.AddModelError("Error", "Email already exist.");
                return View(r);
            }
            else
            {
                ViewBag.Message = "NotExist";
                var hashedPassword = BCrypt.Net.BCrypt.HashPassword(r.password);
                systemUsers reg = new systemUsers();
                reg.email = r.email;
                reg.lastName = r.lastName;
                reg.firstName = r.firstName;
                reg.middleName = r.middleName;
                reg.password = hashedPassword;
                reg.token = global.slugify(64);
                reg.slug = global.slugify(32);
                reg.referenceNo = global.slugify(12).ToUpper();
                reg.isVerified = true;
                reg.branch = "";
                reg.department = "";
                _repoReg.add(reg);

                //var confirmationLink = Url.Action("ConfirmEmail", "Account", new { reg.token, email = reg.email }, Request.Scheme);
                //_repoReg.confirmEmail(reg.token, reg.email);

                var loginLink = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";

                //string to = "rbsamortin@kmbi.org.ph";
                var sb = new StringBuilder();
                sb.Append("Dear <b>" + reg.firstName + " " + reg.lastName + "</b>, <br/><br/>");
                sb.Append("Masaya dito sa <b>KMBI!</b><br/><br/>");
                sb.Append("Thank you for your application to KMBI online.<br/><br/>");
                sb.Append("After our validation, we are pleased to inform you that your application with reference number <b>" + reg.referenceNo + "</b> has been processed<br/><br/>");
                sb.Append("Status of your registered account is listed below:<br/><br/>");
                sb.Append("Username: <b>" + reg.email + "</b><br/>");
                sb.Append("Password: <b>" + r.password + "</b><br/>");
                sb.Append("Status: <b>Approved</b><br/><br/>");
                sb.Append("To access your account online, just click the link below and enter your User ID and Password in the appropriate fields:<br/>");
                sb.Append(loginLink + "<br/>");
                sb.Append("Should you have any inquiries regarding your account, please contact KMBI Help Desk at (000) 000-000-000. For your convenience, our Help Desk is open 24 hours a day, 7 days a week. You may also contact us by email at <a href='ithelpdesk@kmbi.org.ph'>ithelpdesk@kmbi.org.ph</a><br/><br/>");
                sb.Append("Thank you for applying online with us.<br/><br/>");
                sb.Append("<b>KMBI Online</b><br/><br/>");


                string to = reg.email;
                string subject = "KMBI Online Registration";
                string body = sb.ToString();
                bool emailResponse = EmailHelper.SendEmail(to, subject, body, "no-reply@kmbi.org.ph");

                //EmailHelper emailHelper = new EmailHelper();
                //bool emailResponse = emailHelper.SendEmail(reg.email, confirmationLink);

                if (emailResponse)
                    ModelState.AddModelError("Error", "Registration Successful. Please check your email");
                else
                {
                    // log email failed 
                }

                return View(r);
            }

        }

        public JsonResult checkAlreadyRegistered(string email)
        {
            var check = _repoReg.checkAlreadyRegistered(email);
            if (check)
                return Json(false);
            return Json(true);
        }

        [HttpGet]
        public IActionResult ConfirmEmail(string token, string email)
        {
            token = Request.Query["token"];
            email = Request.Query["email"];
            _repoReg.confirmEmail(token, email);
            return View();
        }

        [HttpPost]
        public IActionResult Update([Bind] applicationViewModel data)
        {

            int id = data.systemUsers.id;
            string filePicturePath;
            string fileResumePath;
            string currURL = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";

            if (data.pictureFile != null)
            {
                Stream stream;

                string UploadPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/applicantPictures/");
                string filename = Path.GetFileName(data.pictureFile.FileName);
                filename = filename.Trim().Replace(" ", "");
                filePicturePath = currURL + "/applicantPictures/" + filename;

                stream = new FileStream(UploadPath + filename, FileMode.Create, FileAccess.ReadWrite);

                data.pictureFile.CopyTo(stream);
                stream.Close();

            }
            else
            {

                //string UploadPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/applicantPictures/");
                //string filename = "default-avatar.png";
                //filePicturePath = currURL + "/applicantPictures/" + filename;
                filePicturePath = data.appPersonalData.picture;
            }

            ///RESUME
            if (data.resumeFile != null)
            {
                Stream stream;

                string UploadResume = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/applicantResume/");
                string filename = Path.GetFileName(data.resumeFile.FileName);
                filename = filename.Trim().Replace(" ", "");
                fileResumePath = currURL + "/applicantResume/" + filename;

                stream = new FileStream(UploadResume + filename, FileMode.Create, FileAccess.ReadWrite);

                data.resumeFile.CopyTo(stream);
                stream.Close();

            }
            else
            {
                fileResumePath = data.appPersonalData.resume;
            }

            data.appPersonalData.picture = filePicturePath;
            data.appPersonalData.resume = fileResumePath;
            _repoAccount.updatePData(data, id);

            TempData["name"] = "Updated";
            //SEND EMAIL
            var jobPos = _jobVac.getOneBySlug(data.appPersonalData.firstChoice).jobPosition;
            var webPage = "https://www.kmbi.org.ph/who-we-are/careers";
            var fbPage = "https://www.facebook.com/KMBIMFNGO";

            var sb = new StringBuilder();
            sb.Append("Dear <b>" + data.systemUsers.firstName + " " + data.systemUsers.lastName + "</b>, <br/><br/>");
            sb.Append("We're thrilled that you'd like to join us here at Kabalikat para sa Maunlad na Buhay, Inc. (Microfinance NGO)<br/><br/>");
            sb.Append("We have got your application that you sent us about the <b>" + jobPos + "</b> position. We have looked through some applications as they come in and we will be in touch within two weeks if we'd like to meet you for an interview.<br/><br/>");
            sb.Append("You can read more about us on our company career page <a href='" + webPage + "'>" + webPage + "</a> or follow us on social media on Facebook <a href='" + fbPage + "'>" + fbPage + "</a> to get the latest updates<br/><br/>");
            sb.Append("Masaya dito sa KMBI!<br/><br/>");
            sb.Append("<b>KMBI Online</b><br/><br/>");

            string to = data.systemUsers.email;
            string subject = "KMBI Online Application";
            string body = sb.ToString();
            bool emailResponse = EmailHelper.SendEmail(to, subject, body, "no-reply@kmbi.org.ph");

            //return View(data);
            return RedirectToAction("Index", "Home");

        }

        [HttpPost]
        public IActionResult UpdateAdmin([Bind] adminPersonalViewModel data)
        {

            int id = data.systemUsers.id;
            string filePicturePath;
            string currURL = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";

            if (data.pictureFile != null)
            {
                Stream stream;

                string UploadPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/applicantPictures/");
                string filename = Path.GetFileName(data.pictureFile.FileName);
                filename = filename.Trim().Replace(" ", "");
                filePicturePath = currURL + "/applicantPictures/" + filename;

                stream = new FileStream(UploadPath + filename, FileMode.Create, FileAccess.ReadWrite);

                data.pictureFile.CopyTo(stream);
                stream.Close();

            }
            else
            {
                //string UploadPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/applicantPictures/");
                //string filename = "default-avatar.png";
                //filePicturePath = currURL + "/applicantPictures/" + filename;
                filePicturePath = data.picture;
            }


            data.picture = filePicturePath;
            _repoAccount.updateAdminData(data, id);

            TempData["name"] = "Updated";

            //return View(data);
            return RedirectToAction("Index", "Admin");

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Logout()
        {
            _userManager.SignOut(this.HttpContext);
            return this.RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public void ChangePassword(string password, int id)
        {
            var hashedPassword = BCrypt.Net.BCrypt.HashPassword(password);
            _repoAccount.changePassword(hashedPassword, id);
        }

        public void ResetNewPassword(string password, string email)
        {
            var hashedPassword = BCrypt.Net.BCrypt.HashPassword(password);
            _repoAccount.resetPassword(hashedPassword, email);
        }

        [HttpGet]
        public bool CheckIfPersonalDataExist(int applicantId)
        {
            return _repoAccount.checkPersonalData(applicantId);
        }

        public void ForgotPassword(string email)
        {
            var token = _repoAccount.retrieveToken(email);
            var loginLink = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";

            //string to = "rbsamortin@kmbi.org.ph";
            var sb = new StringBuilder();
            sb.Append("We have sent you this email in response to your request to reset your password. <br/><br/>");
            sb.Append("To reset your password, please follow the link below<br/>");
           
            sb.Append(loginLink + "/Account/ResetPassword?email=" + email + "&token=" + token + "<br/>");
            sb.Append("<b>KMBI Online</b><br/><br/>");

            string to = email;
            string subject = "KMBI Online Registration";
            string body = sb.ToString();
            EmailHelper.SendEmail(to, subject, body,"no-reply@kmbi.org.ph");

        }

        [HttpGet]
        public IActionResult ResetPassword(string email, string token)
        {
            token = Request.Query["token"];
            email = Request.Query["email"];
            return View();
        }

    }
}
