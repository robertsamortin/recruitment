﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Recruitment.Models;
using Recruitment.Models.IRepo;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Recruitment.Controllers
{
    public class EmploymentController : Controller
    {
        private iEmployment _repo;

        public EmploymentController(iEmployment repo)
        {
            _repo = repo;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public void UpdateEmployment(appEmployment data)
        {
            _repo.updateEmployment(data);
            //return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public void AddEmployment(appEmployment data)
        {
            _repo.insertEmployment(data);
           // return RedirectToAction("Index","Home");
        }

        [HttpDelete]
        public void DeleteEmployment(int id)
        {
            _repo.deleteEmployment(id);
            //return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult GetEmployment(int id)
        {
            var result = _repo.getEmployBackground(id);
            return Json(new { data = result });
        }

        [HttpGet]
        public appEmploymentViewModel GetSingleEmployment(int id)
        {
            var result = _repo.getSingleEmployBackground(id);
            // return Json(new { data = result });
            return result;
        }
    }
}
