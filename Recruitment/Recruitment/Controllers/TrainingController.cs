﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Recruitment.Models;
using Recruitment.Models.IRepo;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Recruitment.Controllers
{
    public class TrainingController : Controller
    {
        private iTraining _repo;

        public TrainingController(iTraining repo)
        {
            _repo = repo;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public void UpdateTraining(appTraining data)
        {
            _repo.updateTraining(data);
            //return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public void AddTraining(appTraining data)
        {
            _repo.insertTraining(data);
           // return RedirectToAction("Index","Home");
        }

        [HttpDelete]
        public void DeleteTraining(int id)
        {
            _repo.deleteTraining(id);
            //return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult GetTraining(int id)
        {
            var result = _repo.getTraining(id);
            return Json(new { data = result });
        }

        [HttpGet]
        public appTrainingViewModel GetSingleTraining(int id)
        {
            var result = _repo.getSingleTraining(id);
            // return Json(new { data = result });
            return result;
        }
    }
}
