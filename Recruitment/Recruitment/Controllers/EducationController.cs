﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Recruitment.Models;
using Recruitment.Models.IRepo;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Recruitment.Controllers
{
    public class EducationController : Controller
    {
        private iEducation _repo;

        public EducationController(iEducation repo)
        {
            _repo = repo;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public void UpdateEducation(appEducation data)
        {
            _repo.updateEducation(data);
            //return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public void AddEducation(appEducation data)
        {
            _repo.insertEducation(data);
           // return RedirectToAction("Index","Home");
        }

        [HttpDelete]
        public void DeleteEducation(int id)
        {
            _repo.deleteEducation(id);
            //return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult GetEducation(int id)
        {
            var result = _repo.getEducBackground(id);
            return Json(new { data = result });
        }

        [HttpGet]
        public appEducationViewModel GetSingleEducation(int id)
        {
            var result = _repo.getSingleEducBackground(id);
            // return Json(new { data = result });
            return result;
        }
    }
}
