﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Recruitment.Models;
using Recruitment.Models.IRepo;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Recruitment.Controllers
{
    public class MembershipController : Controller
    {
        private iMembership _repo;

        public MembershipController(iMembership repo)
        {
            _repo = repo;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public void UpdateMembership(appMembership data)
        {
            _repo.updateMembership(data);
            //return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public void AddMembership(appMembership data)
        {
            _repo.insertMembership(data);
           // return RedirectToAction("Index","Home");
        }

        [HttpDelete]
        public void DeleteMembership(int id)
        {
            _repo.deleteMembership(id);
            //return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult GetMembership(int id)
        {
            var result = _repo.getMembership(id);
            return Json(new { data = result });
        }

        [HttpGet]
        public appMembershipViewModel GetSingleMembership(int id)
        {
            var result = _repo.getSingleMembership(id);
            // return Json(new { data = result });
            return result;
        }
    }
}
