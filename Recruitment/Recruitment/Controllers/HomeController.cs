﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Recruitment.Models;
using Recruitment.Models.IRepo;

namespace Recruitment.Controllers
{
   [Authorize]
    public class HomeController : Controller
    {
        private iUserManager _userManager;
        iAccountRepository _user;

        public HomeController(iUserManager userManager, iAccountRepository user)
        {
            _userManager = userManager;
            _user = user;
        }

        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {

                var id = _userManager.GetCurrentUser(this.HttpContext).id;
                var model = _user.getInfoDetails(id);
                if (model != null)
                {
                    return View(model);
                }
                else
                {
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }           
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult Dashboard()
        {
            return View();
        }
    }
}
