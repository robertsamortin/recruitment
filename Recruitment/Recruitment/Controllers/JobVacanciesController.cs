﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Recruitment.Models;
using Recruitment.Models.IRepo;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Recruitment.Controllers
{
    [Authorize]
    public class JobVacanciesController : Controller
    {
        private iJobVacancies _repo;

        public JobVacanciesController(iJobVacancies repo)
        {
            _repo = repo;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        // GET: /<controller>/
        public IActionResult JobVacancies()
        {
            return View();
        }

        [HttpPost]
        public void Update(jobVacancies data)
        {
            _repo.update(data);
            //return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public void Add(jobVacancies data)
        {
            _repo.insert(data);
           // return RedirectToAction("Index","Home");
        }

        [HttpDelete]
        public void Delete(int id)
        {
            _repo.delete(id);
            //return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult GetAll()
        {
            var result = _repo.getAll();
            return Json(new { data = result });
        }

        [HttpGet]
        public List<JobVacanciesViewModel> GetAllActive()
        {
            var result = _repo.getAllViewModel();
            return result;
        }

        [HttpGet]
        public List<jobVacancies> PopulateControl()
        {
            var result = _repo.getAll();
            //return Json(new { data = result });
            return result;
        }

        [HttpGet]
        public jobVacancies GetOne(int id)
        {
            var result = _repo.getOne(id);
            // return Json(new { data = result });
            return result;
        }

        [HttpGet]
        public JobVacanciesViewModelApplicant GetOneSlug(string slug)
        {
            var result = _repo.getOneBySlugApplicant(slug);
            // return Json(new { data = result });
            return result;
        }

        [HttpGet]
        public IActionResult List()
        {
            var result = _repo.getAllActiveViewModel();
            return View(result);
        }


        [HttpGet]
        public ActionResult GetAllViewModel()
        {
            var result = _repo.getAllViewModel();
            return Json(new { data = result });
        }
    }
}
