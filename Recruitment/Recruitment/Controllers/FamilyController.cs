﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Recruitment.Models;
using Recruitment.Models.IRepo;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Recruitment.Controllers
{
    public class FamilyController : Controller
    {
        private iFamily _repo;

        public FamilyController(iFamily repo)
        {
            _repo = repo;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public void UpdateFamily(appFamilyBackground data)
        {
            _repo.updateFamily(data);
            //return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public void AddFamily(appFamilyBackground data)
        {
            _repo.insertFamily(data);
           // return RedirectToAction("Index","Home");
        }

        [HttpDelete]
        public void DeleteFamily(int id)
        {
            _repo.deleteFamily(id);
            //return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult GetFamily(int id)
        {
            var result = _repo.getFamBackground(id);
            return Json(new { data = result });
        }

        [HttpGet]
        public appFamilyViewModel GetSingleFamily(int id)
        {
            var result = _repo.getSingleFamBackground(id);
            // return Json(new { data = result });
            return result;
        }
    }
}
