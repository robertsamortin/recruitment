﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Recruitment.Email;
using Recruitment.Models;
using Recruitment.Models.IRepo;
using Syncfusion.HtmlConverter;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Recruitment.Controllers
{
    [Authorize]
    public class JobApplicationsController : Controller
    {
        private iJobApplications _repo;
        private iAccountRepository _account;
        private readonly IHostingEnvironment _hostingEnvironment;
        iAdminUsers _sysuser;
        iJobVacancies _jobVac;
        iUserManager _userManager;
        public JobApplicationsController(iJobApplications repo, IHostingEnvironment hostingEnvironment, iAccountRepository account, iAdminUsers sysuser, iJobVacancies jobVac, iUserManager userManager)
        {
            _repo = repo;
            _hostingEnvironment = hostingEnvironment;
            _account = account;
            _sysuser = sysuser;
            _jobVac = jobVac;
            _userManager = userManager;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        // GET: /<controller>/
        public IActionResult JobApplications()
        {
            return View();
        }

        

        [HttpPost]
        public void Update(jobApplications data)
        {
            _repo.update(data);
            //return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public void Add(jobApplications data)
        {
            _repo.insert(data);
           // return RedirectToAction("Index","Home");
        }

        [HttpPost]
        public void AddUpdate(jobApplications data)
        {
            _repo.updateBranch(data);
            var acc = _sysuser.getOne(data.applicantId);
            var job = _repo.getOneBySlugApplicant(data.jobSlug, data.applicantId);
            //SEND EMAIL
            var webPage = "https://www.kmbi.org.ph/who-we-are/careers";
            var fbPage = "https://www.facebook.com/KMBIMFNGO";

            var sb = new StringBuilder();
            sb.Append("Dear <b>" + acc.firstName + " " + acc.lastName + "</b>, <br/><br/>");
            sb.Append("We're thrilled that you'd like to join us here at Kabalikat para sa Maunlad na Buhay, Inc. (Microfinance NGO)<br/><br/>");
            sb.Append("We have got your application that you sent us about the <b>" + job.jobPosition + "</b> position. We have looked through some applications as they come in and we will be in touch within two weeks if we'd like to meet you for an interview.<br/><br/>");
            sb.Append("You can read more about us on our company career page <a href='" + webPage + "'>" + webPage + "</a> or follow us on social media on Facebook <a href='" + fbPage + "'>" + fbPage + "</a> to get the latest updates<br/><br/>");
            sb.Append("Masaya dito sa KMBI!<br/><br/>");
            sb.Append("<b>KMBI Online</b><br/><br/>");

            string to = acc.email;
            string subject = "KMBI Online Application";
            string body = sb.ToString();
            bool emailResponse = EmailHelper.SendEmail(to, subject, body, "hcd-staffing@kmbi.org.ph");
            // return RedirectToAction("Index","Home");
        }

        [HttpDelete]
        public void Delete(int id)
        {
            _repo.delete(id);
            //return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult GetAll()
        {
            var result = _repo.getAll();
            return Json(new { data = result });
        }

        [HttpGet]
        public List<jobApplications> GetAllActive()
        {
            var result = _repo.getAllActive();
            return result;
        }

        [HttpGet]
        public List<jobApplications> PopulateControl()
        {
            var result = _repo.getAll();
            //return Json(new { data = result });
            return result;
        }

        [HttpGet]
        public jobApplications GetOne(int id)
        {
            var result = _repo.getOne(id);
            // return Json(new { data = result });
            return result;
        }

        [HttpGet]
        public jobApplications GetOneSlug(string slug)
        {
            var result = _repo.getOneBySlug(slug);
            // return Json(new { data = result });
            return result;
        }

        [HttpGet]
        public IActionResult AdminList(string branch, string department)
        {
            branch = _userManager.GetCurrentUser(this.HttpContext).branch;
            department = _userManager.GetCurrentUser(this.HttpContext).department;
            var model = _repo.GetAllAdmin(branch, department);
            return View(model);
        }
        
        public IActionResult ApplicantList(int applicantId)
        {
            var model = _repo.GetAllApplicant(applicantId);
            return View(model);
        }

        [HttpGet]
        public IActionResult Statuses(int jobId)
        {
            var model = _repo.GetApplicantStatus(jobId);
            return View(model);
        }

        [HttpGet]
        public IActionResult StatusesHo(int jobId)
        {
            var model = _repo.GetApplicantStatus(jobId);
            return View(model);
        }

        [HttpGet]
        public List<jobApplications> ApplicantApplications(int applicantId)
        {
            var model = _repo.getOneByApplicantId(applicantId);
            return model;
        }

        [HttpGet]
        public bool CheckIfJobExist(string jobSlug, int applicantId, string branch)
        {
            var result = _repo.getOneByJobSlugBranch(jobSlug, applicantId, branch);
            if (result != null)
                return true;
            return false;
        }

        [HttpPost]
        public void UpdateStatus(string status, int id)
        {
            _repo.updateStatus(status, id);
        }

        [HttpGet]
        public IActionResult DownloadPDF(int id)
        {

            var model = _account.getInfoDetails(id);
            
            //Initialize HTML to PDF converter with Blink rendering engine
            HtmlToPdfConverter htmlConverter = new HtmlToPdfConverter(HtmlRenderingEngine.Blink);

            BlinkConverterSettings blinkConverterSettings = new BlinkConverterSettings();
            string HTMLContent = "<html lang='en'><head>" +
                    "<meta charset='UTF-8'>" +
                    "<meta http-equiv='X-UA-Compatible' content='IE=edge'>" +
                    "<meta name='viewport' content='width=device-width, initial-scale=1.0'>" +
                    "<link rel='preconnect' href='https://fonts.gstatic.com'>" +
                    "<link href='https://fonts.googleapis.com/css2?family=Roboto&display=swap' rel='stylesheet'>" +
                    "<title>Title Here</title>" +
                    "<link href=" + Path.Combine(_hostingEnvironment.ContentRootPath, "ReportTemplates/style.css") + " rel='stylesheet' />" +
                    "</head><body>" +
                    "<div class='container'>" +

                    "<table class='header'>" +
                        "<tr>" +
                            "<th rowspan='5' colspan='2' style='width: 22%;'>" +
                                "<div>" +
                                    "<img src='./img/logo.png' alt=''>" +
                                "</div>" +
                                "KMBI <br>(A Microfinance NGO)" +
                            "</th>" +
                            "</th>" +
                            "<th colspan='5' class='form-title'>" +
                                "EMPLOYMENT APPLICATION FORM <br>" +
                                "<small><i>Form No. HCDS-002</i></small>" +
                        "</th>" +
                        "</tr>" +

                        "<tr>" +
                            "<td colspan='3'>Referred by/ thru : " + model.appPersonalData.referredBy + "</td>" +
                            "<td>Date: value here</td>" +
                            "<td rowspan='4' style='width: 13%;'>ID PICTURE (2x2)<br> taken within the last 6 months</td>" +
                        "</tr>" +

                        "<tr>" +
                            "<th colspan='3'>SOCIAL MEDIA ACCOUNTS</th>" +
                            "<th>DESIRED SALARY</th>" +
                        "</tr>" +

                        "<tr>" +
                            "<th>Facebook</th>" +
                            "<th>Instagram</th>" +
                            "<th>Twitter</th>" +
                            "<th></th>" +
                        "</tr>" +

                        "<tr>" +
                            "<th></th>" +
                            "<th></th>" +
                            "<th></th>" +
                            "<th></th>" +
                        "</tr>" +
                    "</table>" +

                    "<table class='personal-data'>" +
                        "<tr>" +
                            "<td class='section-title' rowspan='7'>" +
                            "<div style='text-align:center'>PERSONAL DATA</div>" +
                            "</td>" +
                            "<td colspan='3'><span>FULL NAME</span> (Last Name, First Name, Middle Name) <br> " + model.systemUsers.lastName + ", " + model.systemUsers.firstName + " " + model.systemUsers.middleName + "</td>" +
                            "<td>Nickname <br> " + model.appPersonalData.nickName + "</td>" +
                            "<td>Gender <br> " + model.appPersonalData.sex + "</td>" +
                            "<td>Blood Type <br> " + model.appPersonalData.bloodType + "</td>" +
                        "</tr>" +

                    "<tr>" +
                    "<td>Birthplace <br> value here</td>" +
                    "<td>Citizenship <br> value here</td>" +
                    "<td>Weight <br> value here</td>" +
                    "<td>Height <br> value here</td>" +
                    "<td>Civil Status <br> value here</td>" +
                    "<td>Religious Affiliation <br> value here</td>" +
                    "</tr>" +

                    "<tr>" +
                    "<td>SSS / GSIS No. <br> value here</td>" +
                    "<td colspan='2'>TIN <br> value here</td>" +
                    "<td>Tax Status <br> value here</td>" +
                    "<td>Pag-ibig No. <br> value here</td>" +
                    "<td>Philhealth No. <br> value here</td>" +
                    "</tr>" +


                    "<tr>" +
                    "<td colspan='2'>Landline No. <br> value here</td>" +
                    "<td colspan='2'>Mobile No. <br> value here</td>" +
                    "<td colspan='2'>E-mail Address <br> value here</td>" +
                    "</tr>" +

                    "<tr>" +
                    "<td colspan='3'>Present Address <br> value here</td>" +
                    "<td colspan='3'>" +
                    "Years of Stay <u>value here</u> : ( &check; ) Own <br> ( &check; ) Rent at Php <u>value here</u>/ mo" +
                    "Live with <u>value here</u>" +
                    "</td>" +
                    "</tr>" +


                    "<tr>" +
                    "<td colspan='3'>Permanent / Provincial Address <br> value here</td>" +
                    "<td colspan='2'>Contact No <br> value here</td>" +
                    "<td>Zip Code <br> value here</td>" +
                    "</tr>" +

                    "<tr>" +
                    "<td colspan='3'>Other known addresses <br> value here</td>" +
                    "<td colspan='2'>Contact No <br> value here</td>" +
                    "<td>Zip Code <br> value here</td>" +
                    "</tr>" +

                    "</table>" +
                    "</div>" +
                    "</body></html>";
            string baseUrl = @"D:/Hey";
            //Set the BlinkBinaries folder path
            blinkConverterSettings.BlinkPath = Path.Combine(_hostingEnvironment.ContentRootPath, "BlinkBinariesWindows");

            //Assign Blink converter settings to HTML converter
            htmlConverter.ConverterSettings = blinkConverterSettings;
            blinkConverterSettings.Margin = new PdfMargins { All = 5 };
            //Convert URL to PDF
            string url = Path.Combine(_hostingEnvironment.ContentRootPath, "ReportTemplates/PersonalData.cshtml");
            PdfDocument document = htmlConverter.Convert(HTMLContent, baseUrl);
            MemoryStream stream = new MemoryStream();
            document.Save(stream);
            string mimeType = "application/pdf";
            Response.Headers.Add("content-disposition", "inline;filename=" + "PDFfile.pdf");
            return File(stream.ToArray(), mimeType);
            //return File(stream.ToArray(), System.Net.Mime.MediaTypeNames.Application.Pdf, "Output.pdf");
        }

        [HttpGet]
        public IActionResult PersonalData(int applicantId)
        {
            var model = _account.getInfoDetailsReport(applicantId);
            return View(model);
        }

        [HttpGet]
        public statuses GetStatuses(int applicantId)
        {
            var model = _repo.GetStatuses(applicantId);
            return model;
        }

        //FAILED EMAILS
        [HttpPost]
        public void SendFailedEmailResume (statuses data)
        {
           // _repo.insertStatus(data);
            _repo.updateStatus("failed", data.id);
            var acc = _sysuser.getOne(data.applicantId);
            var job = _repo.getOneBySlugApplicant(data.jobSlug, data.applicantId);

            //CCD
            var stringArrayOfbranches = job.branches;
            string branches = string.Join(", ", stringArrayOfbranches);
            List<string> listBranches = branches.Split(',').ToList();
            List<string> emailAddress = new List<string>();
            foreach (var item in listBranches)
            {
                var q = _sysuser.emailAddress(item);
                foreach (var i in q)
                {
                    emailAddress.Add(i);
                }
            }
            var id = _userManager.GetCurrentUser(this.HttpContext).id;
            var currUser = _sysuser.getOne(id);
            var currEmail = currUser.email;
            emailAddress.Add(currEmail);
            emailAddress.Add("hcd-staffing@kmbi.org.ph");
            var cc = string.Join(", ", emailAddress);
            //SEND EMAIL
            var email1 = "jmonato@kmbi.org.ph";
            var email2 = "jbbayaca@kmbi.org.ph";
            var kmbiNumber = "(02) 8291-1485";

            var sb = new StringBuilder();
            sb.Append("Dear <b>" + acc.firstName + " " + acc.lastName + "</b>, <br/><br/>");
            sb.Append("Good Day! <br/><br/>");
            sb.Append("Thank you for your application for the " + job.jobPosition + " at Kabalikat para sa Maunlad na Buhay, Inc. (a Microfinance NGO).<br/>");
            sb.Append("We really appreciate your interest in joining our company and we want to thank you for the time and energy you invested in applying for our job opening.<br/><br/>");
            sb.Append("We received a large number of applications, and after carefully reviewing all of them, unfortunately, we have to inform you that this time we won’t be able to invite you to the next phase of our selection process.<br/><br/>");
            sb.Append("We really do appreciate your interest in our company. We hope you’ll keep us in mind and apply again in the future should you see a job opening for which you qualify. <br/><br/>");
            sb.Append("If you have any questions or need additional information, please don’t hesitate to contact us by email <a href='" + email1 + "'>" + email1 + "</a> / <a href='" + email2 + "'>" + email2 + " </a> or phone " + kmbiNumber + ". <br/><br/>");
            sb.Append("We wish you every personal and professional success in your future endeavors. <br/><br/>");
            sb.Append("Once again, thank you for your interest in applying with us. <br/><br/>");
            sb.Append("God Bless. <br/><br/>");
            sb.Append("Regards<br/>");
            sb.Append("<b>KMBI HR</b><br/><br/>");

            string to = acc.email;
            string subject = "KMBI Online Application";
            string body = sb.ToString();
            bool emailResponse = EmailHelper.SendEmail(to, subject, body,cc);
            // return RedirectToAction("Index","Home");
        }

        [HttpPost]
        public void SendFailedEmailInitialInterview(statuses data)
        {
           // _repo.insertStatus(data);
            _repo.updateStatus("failed", data.id);
            var acc = _sysuser.getOne(data.applicantId);
            var job = _repo.getOneBySlugApplicant(data.jobSlug, data.applicantId);
            var id = _userManager.GetCurrentUser(this.HttpContext).id;
            var currUser = _sysuser.getOne(id);
            var hrName = currUser.firstName + " " + currUser.middleName.Substring(0, 1) + ". " + currUser.lastName;
            var hrPosition = currUser.position;

            //CCD
            var stringArrayOfbranches = job.branches;
            string branches = string.Join(", ", stringArrayOfbranches);
            List<string> listBranches = branches.Split(',').ToList();
            List<string> emailAddress = new List<string>();
            foreach (var item in listBranches)
            {
                var q = _sysuser.emailAddress(item);
                foreach (var i in q)
                {
                    emailAddress.Add(i);
                }
            }
            var currEmail = currUser.email;
            emailAddress.Add("hcd-staffing@kmbi.org.ph");
            emailAddress.Add(currEmail);
            var cc = string.Join(", ", emailAddress);
            //SEND EMAIL

            var sb = new StringBuilder();
            sb.Append("Dear <b>" + acc.firstName + " " + acc.lastName + "</b>, <br/><br/>");
            sb.Append("Thank you for taking the time to be interviewed with our team, it was a pleasure to learn more about your skills and accomplishments.<br/><br/>");
            sb.Append("Unfortunately, our team did not select you for further consideration.<br/>");
            sb.Append("Now that we’ve had the chance to know more about you, we will be keeping your resume on file for future openings that better fit your profile.<br/><br/>");
            sb.Append("Thank you and God bless! <br/><br/>");
            sb.Append("Sincerely,<br/><br/>");
            sb.Append("<b>" + hrName + "</b><br/>");
            sb.Append(hrPosition + "<br/>");
            sb.Append("Contact No. 8291-1485 </b><br/><br/>");

            string to = acc.email;
            string subject = "KMBI Online Application";
            string body = sb.ToString();
            bool emailResponse = EmailHelper.SendEmail(to, subject, body, cc);
            // return RedirectToAction("Index","Home");
        }

        [HttpPost]
        public void SendFailedEmailExam(statuses data)
        {
           // _repo.insertStatus(data);
            _repo.updateStatus("failed", data.id);
            var acc = _sysuser.getOne(data.applicantId);
            var job = _repo.getOneBySlugApplicant(data.jobSlug, data.applicantId);
            var id = _userManager.GetCurrentUser(this.HttpContext).id;
            var currUser = _sysuser.getOne(id);
            var hrName = currUser.firstName + " " + currUser.middleName.Substring(0, 1) + ". " + currUser.lastName;
            var hrPosition = currUser.position;

            //CCD
            var stringArrayOfbranches = job.branches;
            string branches = string.Join(", ", stringArrayOfbranches);
            List<string> listBranches = branches.Split(',').ToList();
            List<string> emailAddress = new List<string>();
            foreach (var item in listBranches)
            {
                var q = _sysuser.emailAddress(item);
                foreach (var i in q)
                {
                    emailAddress.Add(i);
                }
            }
            var currEmail = currUser.email;
            emailAddress.Add("hcd-staffing@kmbi.org.ph");
            emailAddress.Add(currEmail);
            var cc = string.Join(", ", emailAddress);
            //SEND EMAIL

            var sb = new StringBuilder();
            sb.Append("Dear <b>" + acc.firstName + " " + acc.lastName + "</b>, <br/><br/>");
            sb.Append("Good day! <br/><br/>");
            sb.Append("Thank you for the time and effort you invested to apply for the " + job.jobPosition + ". However, we regret to inform you that you did not meet the standard score as you took the online assessment.<br/><br/>");
            sb.Append("Again, thank you for applying and we wish you all the best on your future endeavors.<br/><br/>");
            sb.Append("Sincerely,<br/><br/>");
            sb.Append("<b>" + hrName + "</b><br/>");
            sb.Append(hrPosition + "<br/>");
            sb.Append("Contact No. 8291-1485 </b><br/><br/>");

            string to = acc.email;
            string subject = "KMBI Online Application";
            string body = sb.ToString();
            bool emailResponse = EmailHelper.SendEmail(to, subject, body,cc);
            // return RedirectToAction("Index","Home");
        }

        [HttpPost]
        public void SendFailedEmailFinalInterview(statuses data)
        {
            //_repo.insertStatus(data);
            _repo.updateStatus("failed", data.id);
            var acc = _sysuser.getOne(data.applicantId);
            var job = _repo.getOneBySlugApplicant(data.jobSlug, data.applicantId);
            var id = _userManager.GetCurrentUser(this.HttpContext).id;
            var currUser = _sysuser.getOne(id);
            var hrName = currUser.firstName + " " + currUser.middleName.Substring(0, 1) + ". " + currUser.lastName;
            var hrPosition = currUser.position;

            //CCD
            var stringArrayOfbranches = job.branches;
            string branches = string.Join(", ", stringArrayOfbranches);
            List<string> listBranches = branches.Split(',').ToList();
            List<string> emailAddress = new List<string>();
            foreach (var item in listBranches)
            {
                var q = _sysuser.emailAddress(item);
                foreach (var i in q)
                {
                    emailAddress.Add(i);
                }
            }
            var currEmail = currUser.email;
            emailAddress.Add(currEmail);
            emailAddress.Add("hcd-staffing@kmbi.org.ph");
            var cc = string.Join(", ", emailAddress);
            //SEND EMAIL

            var sb = new StringBuilder();
            sb.Append("Dear <b>" + acc.firstName + " " + acc.lastName + "</b>, <br/><br/>");
            sb.Append("Thank you for meeting with us to discuss the " + job.jobPosition + " position at KMBI. We enjoyed meeting you and learning more about your skills and achievements.<br/><br/>");
            sb.Append("Unfortunately, you have not been selected for further consideration for this position.<br/><br/>");
            sb.Append("We had many excellent candidates apply for this position, however we can only select one person to fill the role and had to make a difficult decision between our top candidates.<br/><br/>");
            sb.Append("We would be happy to answer any of your questions if you are interested in specific feedback about your interview or application.<br/><br/>");
            sb.Append("Now that we have had the opportunity to get to know you and find out more about your work history and talents, we will keep your resume and information on file for any future job openings that might be a good fit.<br/><br/>");
            sb.Append("Thank you again for applying for a position at KMBI. <br/><br/>");
            sb.Append("God bless!<br/><br/>");
            sb.Append("Sincerely,<br/><br/>");
            sb.Append("<b>Joy Anne O. Villajuan</b><br/>");
            sb.Append("HC-Officer for Staffing<br/>");
            sb.Append("Staffing & Employee Relation Division<br/>");
            sb.Append("Human Capital Department<br/>");
            sb.Append("Contact No. 8291-1485 </b><br/><br/>");

            string to = acc.email;
            string subject = "KMBI Online Application";
            string body = sb.ToString();
            bool emailResponse = EmailHelper.SendEmail(to, subject, body,cc);
            // return RedirectToAction("Index","Home");
        }

        [HttpPost]
        public void SendFailedEmaiGideon(statuses data)
        {
            //_repo.insertStatus(data);
            _repo.updateStatus("failed", data.id);
            var acc = _sysuser.getOne(data.applicantId);
            var job = _repo.getOneBySlugApplicant(data.jobSlug, data.applicantId);
            var id = _userManager.GetCurrentUser(this.HttpContext).id;
            var currUser = _sysuser.getOne(id);
            var hrName = currUser.firstName + " " + currUser.middleName.Substring(0, 1) + ". " + currUser.lastName;
            var hrPosition = currUser.position;
            //CCD
            var stringArrayOfbranches = job.branches;
            string branches = string.Join(", ", stringArrayOfbranches);
            List<string> listBranches = branches.Split(',').ToList();
            List<string> emailAddress = new List<string>();
            foreach (var item in listBranches)
            {
                var q = _sysuser.emailAddress(item);
                foreach (var i in q)
                {
                    emailAddress.Add(i);
                }
            }
            var currEmail = currUser.email;
            emailAddress.Add(currEmail);
            emailAddress.Add("hcd-staffing@kmbi.org.ph");
            var cc = string.Join(", ", emailAddress);


            //SEND EMAIL

            var sb = new StringBuilder();
            sb.Append("Dear <b>" + acc.firstName + " " + acc.lastName + "</b>, <br/><br/>");
            sb.Append("Good day! <br/><br/>");
            sb.Append("Thank you for the time and effort you invested for the Gideon Training Program conducted at " + branches + ". However, we regret to inform you that you did not meet the performance standard need for the training.<br/><br/>");
            sb.Append("Again, thank you for applying and we wish you all the best on your future endeavors.<br/><br/>");
            sb.Append("Sincerely,<br/><br/>");
            sb.Append("<b>" + hrName + "</b><br/>");
            sb.Append(hrPosition + "<br/>");
            sb.Append("Contact No. 8291-1485 </b><br/><br/>");

            string to = acc.email;
            string subject = "KMBI Online Application";
            string body = sb.ToString();
            bool emailResponse = EmailHelper.SendEmail(to, subject, body, cc);
            // return RedirectToAction("Index","Home");
        }


        //NOTIFICATION FOR INITIAL INTERVIEW FOR BRANCH OFFICE
        [HttpPost]
        public void InitialInterviewEmailBranch(statuses data)
        {
            _repo.insertStatus(data);
            _repo.updateStatus(data.status, data.id);
            var acc = _sysuser.getOne(data.applicantId);
            var job = _repo.getOneBySlugApplicant(data.jobSlug, data.applicantId);
            //CCD
            var stringArrayOfbranches = job.branches;
            string branches = string.Join(", ", stringArrayOfbranches);
            List<string> listBranches = branches.Split(',').ToList();
            List<string> emailAddress = new List<string>();
            foreach (var item in listBranches)
            {
                var q = _sysuser.emailAddress(item);
                foreach (var i in q)
                {
                    emailAddress.Add(i);
                }
            }
            var id = _userManager.GetCurrentUser(this.HttpContext).id;
            var currUser = _sysuser.getOne(id);
            var currEmail = currUser.email;
            emailAddress.Add(currEmail);
            emailAddress.Add("hcd-staffing@kmbi.org.ph");
            var cc = string.Join(", ", emailAddress);

            var hrName = currUser.firstName + " " + currUser.middleName.Substring(0, 1) + ". " + currUser.lastName;
            var hrPosition = currUser.position;
            //SEND EMAIL

            var sb = new StringBuilder();
            sb.Append("Dear <b>" + acc.firstName + " " + acc.lastName + "</b>, <br/><br/>");
            sb.Append("We would like to get to know you better! <br/><br/>");
            sb.Append("After reviewing your application for the position of " + job.jobPosition + " at KMBI " + branches + " branch, we have decided to select you for the next phase of our recruitment process.</br><br/>");
            sb.Append("Thus, we would like to invite you for a job interview on " + data.inclusiveDate + ".<br/><br/>");
            sb.Append("Please bring along with you a copy of your updated resume and any valid ID.<br/><br/>");
            sb.Append("Please feel free to contact the undersigned should there be any further inquiries. <br/><br/>");
            sb.Append("We are looking forward to meeting you! <br/><br/>");
            sb.Append("Sincerely, <br/><br/>");
            sb.Append("<b>" + hrName + "</b><br/>");
            sb.Append(hrPosition + "<br/>");
            sb.Append("Contact No. 8291-1485 </b><br/><br/>");

            string to = acc.email;
            string subject = "KMBI Online Application";
            string body = sb.ToString();
            bool emailResponse = EmailHelper.SendEmail(to, subject, body, cc);
            // return RedirectToAction("Index","Home");
        }

        //NOTIFICATION FOR INITIAL INTERVIEW FOR HEAD OFFICE
        [HttpPost]
        public void InitialInterviewEmailHo(statuses data)
        {
            _repo.insertStatus(data);
            _repo.updateStatus(data.status, data.id);
            var acc = _sysuser.getOne(data.applicantId);
            var job = _repo.getOneBySlugApplicant(data.jobSlug, data.applicantId);
            //CCD
            var stringArrayOfbranches = job.branches;
            string branches = string.Join(", ", stringArrayOfbranches);
            List<string> listBranches = branches.Split(',').ToList();
            List<string> emailAddress = new List<string>();
            foreach (var item in listBranches)
            {
                var q = _sysuser.emailAddress(item);
                foreach (var i in q)
                {
                    emailAddress.Add(i);
                }
            }
            var id = _userManager.GetCurrentUser(this.HttpContext).id;
            var currUser = _sysuser.getOne(id);
            var currEmail = currUser.email;
            emailAddress.Add(currEmail);
            emailAddress.Add("hcd-staffing@kmbi.org.ph");
            var cc = string.Join(", ", emailAddress);
            //SEND EMAIL

            var sb = new StringBuilder();
            sb.Append("Dear <b>" + acc.firstName + " " + acc.lastName + "</b>, <br/><br/>");
            sb.Append("We would like to get to know you better! <br/><br/>");
            sb.Append("After reviewing your application for the position of " + job.jobPosition + " at KMBI " + branches + ", we have decided to select you for the next phase of our recruitment process. The invitation for interview shall be sent to your email separately.</br><br/>");
            sb.Append("Please feel free to contact the undersigned should there be any further inquiries. <br/><br/>");
            sb.Append("Thank you and God bless! <br/><br/>");
            sb.Append("Sincerely <br/><br/>");
            sb.Append("<b>Joy Anne O. Villajuan</b><br/>");
            sb.Append("HC-Officer for Staffing<br/>");
            sb.Append("Staffing & Employee Relation Division<br/>");
            sb.Append("Human Capital Department<br/>");
            sb.Append("Contact number: 8291-1485<br/></br>");

            string to = acc.email;
            string subject = "KMBI Online Application";
            string body = sb.ToString();
            bool emailResponse = EmailHelper.SendEmail(to, subject, body, cc);
            // return RedirectToAction("Index","Home");
        }

        //NOTIFICATION FOR ONLINE EXAMINATION FOR HO & BO
        [HttpPost]
        public void OnlineExamEmailBranchHo(statuses data)
        {
            _repo.insertStatus(data);
            _repo.updateStatus(data.status, data.id);
            var acc = _sysuser.getOne(data.applicantId);
            var job = _repo.getOneBySlugApplicant(data.jobSlug, data.applicantId);
            var id = _userManager.GetCurrentUser(this.HttpContext).id;
            var currUser = _sysuser.getOne(id);
            var hrName = currUser.firstName + " " + currUser.middleName.Substring(0, 1) + ". " + currUser.lastName;
            var hrPosition = currUser.position;
            //CCD
            var stringArrayOfbranches = job.branches;
            string branches = string.Join(", ", stringArrayOfbranches);
            List<string> listBranches = branches.Split(',').ToList();
            List<string> emailAddress = new List<string>();
            foreach (var item in listBranches)
            {
                var q = _sysuser.emailAddress(item);
                foreach (var i in q)
                {
                    emailAddress.Add(i);
                }
            }
            var currEmail = currUser.email;
            emailAddress.Add(currEmail);
            emailAddress.Add("hcd-staffing@kmbi.org.ph");
            var cc = string.Join(", ", emailAddress);
            //SEND EMAIL

            var sb = new StringBuilder();
            sb.Append("Dear <b>" + acc.firstName + " " + acc.lastName + "</b>, <br/><br/>");
            sb.Append("Good day!<br/><br/>");
            sb.Append("We would like to congratulate you that you have passed the interview, thus, we would like to proceed your application for the online examination. Kindly refer to this link <a href='https://portal.peopledynamics.asia'> https://portal.peopledynamics.asia </a>. The username and password shall be sent separately.</br><br/>");
            sb.Append("Please feel free to contact the undersigned should there be any further inquiries. <br/><br/>");
            sb.Append("Thank you and God bless! <br/><br/>");
            sb.Append("Sincerely <br/><br/>");
            sb.Append("<b>" + hrName + "</b><br/>");
            sb.Append(hrPosition + "<br/>");
            sb.Append("Contact No. 8291-1485 </b><br/><br/>");

            string to = acc.email;
            string subject = "KMBI Online Application";
            string body = sb.ToString();
            bool emailResponse = EmailHelper.SendEmail(to, subject, body, cc);
            // return RedirectToAction("Index","Home");
        }

        //WHEN PASSED IN EXAM HO & BO
        [HttpPost]
        public void PassedOnlineExamBranchHo(statuses data)
        {
           
            var acc = _sysuser.getOne(data.applicantId);
            var job = _repo.getOneBySlugApplicant(data.jobSlug, data.applicantId);
            var id = _userManager.GetCurrentUser(this.HttpContext).id;
            var currUser = _sysuser.getOne(id);
            var hrName = currUser.firstName + " " + currUser.middleName.Substring(0, 1) + ". " + currUser.lastName;
            var hrPosition = currUser.position;

            //CCD
            var stringArrayOfbranches = job.branches;
            string branches = string.Join(", ", stringArrayOfbranches);
            List<string> listBranches = branches.Split(',').ToList();
            List<string> emailAddress = new List<string>();
            foreach (var item in listBranches)
            {
                var q = _sysuser.emailAddress(item);
                foreach (var i in q)
                {
                    emailAddress.Add(i);
                }
            }
            var currEmail = currUser.email;
            emailAddress.Add(currEmail);
            emailAddress.Add("hcd-staffing@kmbi.org.ph");
            var cc = string.Join(", ", emailAddress);
            //SEND EMAIL

            var sb = new StringBuilder();
            sb.Append("Dear <b>" + acc.firstName + " " + acc.lastName + "</b>, <br/><br/>");
            sb.Append("Congratulations!<br/><br/>");
            sb.Append("We wish to inform you that you have PASSED our online assessment.<br/>");
            sb.Append("Please expect a call/text or email from our team for the next process.<br/><br/>");
            sb.Append("Thank you and God bless! <br/><br/>");
            sb.Append("Sincerely,<br/><br/>");
            sb.Append("<b>" + hrName + "</b><br/>");
            sb.Append(hrPosition + "<br/>");
            sb.Append("Contact No. 8291-1485 </b><br/><br/>");

            string to = acc.email;
            string subject = "KMBI Online Application";
            string body = sb.ToString();
            bool emailResponse = EmailHelper.SendEmail(to, subject, body, cc);
            // return RedirectToAction("Index","Home");
        }

        //NOTIFICATION FOR FINAL INTERVIEW(HO)
        [HttpPost]
        public void FinalInterviewHo(statuses data)
        {
            _repo.insertStatus(data);
            _repo.updateStatus(data.status, data.id);
            var acc = _sysuser.getOne(data.applicantId);
            var job = _repo.getOneBySlugApplicant(data.jobSlug, data.applicantId);
            var id = _userManager.GetCurrentUser(this.HttpContext).id;
            var currUser = _sysuser.getOne(id);
            var hrName = currUser.firstName + " " + currUser.middleName.Substring(0, 1) + ". " + currUser.lastName;
            var hrPosition = currUser.position;

            //CCD
            var stringArrayOfbranches = job.branches;
            string branches = string.Join(", ", stringArrayOfbranches);
            List<string> listBranches = branches.Split(',').ToList();
            List<string> emailAddress = new List<string>();
            foreach (var item in listBranches)
            {
                var q = _sysuser.emailAddress(item);
                foreach (var i in q)
                {
                    emailAddress.Add(i);
                }
            }
            var currEmail = currUser.email;
            emailAddress.Add(currEmail);
            emailAddress.Add("hcd-staffing@kmbi.org.ph");
            var cc = string.Join(", ", emailAddress);
            //SEND EMAIL

            var sb = new StringBuilder();
            sb.Append("Dear <b>" + acc.firstName + " " + acc.lastName + "</b>, <br/><br/>");
            sb.Append("Congratulation for passing the recent interviews for the " + job.jobPosition + " at KMBI. With this, we would like to proceed your application for final interview. The interview schedule shall be sent to you separately. <br/><br/>");
            sb.Append("Kindly acknowledge this email for confirmation.<br/>");
            sb.Append("Please expect a call/text or email from our team for the next process.<br/><br/>");
            sb.Append("Thank you and God bless! <br/><br/>");
            sb.Append("Sincerely,<br/><br/>");
            sb.Append("<b>Joy Anne O. Villajuan</b><br/>");
            sb.Append("HC-Officer for Staffing<br/>");
            sb.Append("Staffing & Employee Relation Division<br/>");
            sb.Append("Human Capital Department<br/>");
            sb.Append("Contact No. 8291-1485 </b><br/><br/>");

            string to = acc.email;
            string subject = "KMBI Online Application";
            string body = sb.ToString();
            bool emailResponse = EmailHelper.SendEmail(to, subject, body,cc);
            // return RedirectToAction("Index","Home");
        }

        //NOTIFICATION FOR FINAL INTERVIEW(branch)
        [HttpPost]
        public void FinalInterviewBranch(statuses data)
        {
            _repo.insertStatus(data);
            _repo.updateStatus(data.status, data.id);
            var acc = _sysuser.getOne(data.applicantId);
            var job = _repo.getOneBySlugApplicant(data.jobSlug, data.applicantId);
            var id = _userManager.GetCurrentUser(this.HttpContext).id;
            var currUser = _sysuser.getOne(id);
            var hrName = currUser.firstName + " " + currUser.middleName.Substring(0, 1) + ". " + currUser.lastName;
            var hrPosition = currUser.position;
            //CCD
            var stringArrayOfbranches = job.branches;
            string branches = string.Join(", ", stringArrayOfbranches);
            List<string> listBranches = branches.Split(',').ToList();
            List<string> emailAddress = new List<string>();
            foreach (var item in listBranches)
            {
                var q = _sysuser.emailAddress(item);
                foreach (var i in q)
                {
                    emailAddress.Add(i);
                }
            }
            var currEmail = currUser.email;
            emailAddress.Add(currEmail);
            emailAddress.Add("hcd-staffing@kmbi.org.ph");
            var cc = string.Join(", ", emailAddress);
            //SEND EMAIL

            var sb = new StringBuilder();
            sb.Append("Dear <b>" + acc.firstName + " " + acc.lastName + "</b>, <br/><br/>");
            sb.Append("In line with your application at " + branches + " for the " + job.jobPosition + ", we would like to invite you for final interview on " + data.inclusiveDate + ".<br/><br/>");
            sb.Append("Kindly confirm your attendance by contacting the undersigned.<br/>");
            sb.Append("Thank you and God bless! <br/><br/>");
            sb.Append("Sincerely,<br/><br/>");
            sb.Append("<b>" + hrName + "</b><br/>");
            sb.Append(hrPosition + "<br/>");
            sb.Append("Contact No. 8291-1485 </b><br/><br/>");

            string to = acc.email;
            string subject = "KMBI Online Application";
            string body = sb.ToString();
            bool emailResponse = EmailHelper.SendEmail(to, subject, body,cc);
            // return RedirectToAction("Index","Home");
        }

        //ENDORSEMENT FOR JOB OFFER TO SOD (HO)
        public void EndorsementJobOfferHo(statuses data)
        {
            _repo.insertStatus(data);
            _repo.updateStatus(data.status, data.id);
            var acc = _sysuser.getOne(data.applicantId);
            var job = _repo.getOneBySlugApplicant(data.jobSlug, data.applicantId);
            var id = _userManager.GetCurrentUser(this.HttpContext).id;
            var currUser = _sysuser.getOne(id);
            var hrName = currUser.firstName + " " + currUser.middleName.Substring(0, 1) + ". " + currUser.lastName;
            var hrPosition = currUser.position;
            var directorEmail = _sysuser.email("HCD DIRECTOR");
            List<string> emailAddress = new List<string>();
            emailAddress.Add("hcd-staffing@kmbi.org.ph");
            var cc = string.Join(", ", emailAddress);
            //var stringArrayOfbranches = job.branches.Select(p => p.name).ToArray();
            //string branches = string.Join(", ", stringArrayOfbranches);
            var stringArrayOfbranches = job.branches;
            string branches = string.Join(", ", stringArrayOfbranches);
            //SEND EMAIL

            var sb = new StringBuilder();
            sb.Append("Dear Ma’am Sha,<br/><br/>");
            sb.Append("Good day! <br/><br/>");
            sb.Append("This is to endorse you " + acc.firstName + " " + acc.lastName + " the candidate for " + job.jobPosition + " position, who passed the assessments.  Kindly refer to this (link) for the profile of the said candidate.<br/>");
            sb.Append("Thank you and God bless! <br/><br/>");
            sb.Append("Sincerely,<br/><br/>");
            sb.Append("<b>Joy Anne O. Villajuan</b><br/>");
            sb.Append("HC-Officer for Staffing<br/>");
            sb.Append("Staffing & Employee Relation Division<br/>");
            sb.Append("Human Capital Department<br/>");
            sb.Append("Contact No. 8291-1485 </b><br/><br/>");

            string to = acc.email;
            string subject = "KMBI Online Application";
            string body = sb.ToString();
            bool emailResponse = EmailHelper.SendEmail(directorEmail, subject, body, cc);
            // return RedirectToAction("Index","Home");
        }


        //INVITATION FOR 3-DAY GIDEON TRAINING PROGRAM
        [HttpPost]
        public void GideonInvitation(statuses data)
        {
            _repo.insertStatus(data);
            _repo.updateStatus(data.status, data.id);
            var acc = _sysuser.getOne(data.applicantId);
            var job = _repo.getOneBySlugApplicant(data.jobSlug, data.applicantId);
            var id = _userManager.GetCurrentUser(this.HttpContext).id;
            var currUser = _sysuser.getOne(id);
            var hrName = currUser.firstName + " " + currUser.middleName.Substring(0, 1) + ". " + currUser.lastName;
            var hrPosition = currUser.position;
            //CCD
            var stringArrayOfbranches = job.branches;
            string branches = string.Join(", ", stringArrayOfbranches);
            List<string> listBranches = branches.Split(',').ToList();
            List<string> emailAddress = new List<string>();
            foreach (var item in listBranches)
            {
                var q = _sysuser.emailAddress(item);
                foreach (var i in q)
                {
                    emailAddress.Add(i);
                }
            }
            var currEmail = currUser.email;
            emailAddress.Add(currEmail);
            emailAddress.Add("hcd-staffing@kmbi.org.ph");
            var cc = string.Join(", ", emailAddress);

            //SEND EMAIL

            var sb = new StringBuilder();
            sb.Append("Dear <b>" + acc.firstName + " " + acc.lastName + "</b>, <br/><br/>");
            sb.Append("Congratulations!<br/><br/>");
            sb.Append("You’ve passed the initial assessment for the " + job.jobPosition + " at KMBI, thus, we would like to invite you for the 3-day Gideon Training Program on " + data.inclusiveDate + " that will be held at " + branches + ".<br/><br/>");
            sb.Append("For confirmation, kindly call or text the undersigned.<br/><br/>");
            sb.Append("Thank you and God bless! <br/><br/>");
            sb.Append("Sincerely,<br/><br/>");
            sb.Append("<b>" + hrName + "</b><br/>");
            sb.Append(hrPosition + "<br/>");
            sb.Append("Contact No. 8291-1485 </b><br/><br/>");

            string to = acc.email;
            string subject = "KMBI Online Application";
            string body = sb.ToString();
            bool emailResponse = EmailHelper.SendEmail(to, subject, body,cc);

            // return RedirectToAction("Index","Home");
        }

        //NOTIFICATION WHEN PASSED THE GIDEON TRAINING PROGRAM
        [HttpPost]
        public void GideonPassed(statuses data)
        {
            
            var acc = _sysuser.getOne(data.applicantId);
            var job = _repo.getOneBySlugApplicant(data.jobSlug, data.applicantId);
            var id = _userManager.GetCurrentUser(this.HttpContext).id;
            var currUser = _sysuser.getOne(id);
            var hrName = currUser.firstName + " " + currUser.middleName.Substring(0, 1) + ". " + currUser.lastName;
            var hrPosition = currUser.position;
            //CCD
            var stringArrayOfbranches = job.branches;
            string branches = string.Join(", ", stringArrayOfbranches);
            List<string> listBranches = branches.Split(',').ToList();
            List<string> emailAddress = new List<string>();
            foreach (var item in listBranches)
            {
                var q = _sysuser.emailAddress(item);
                foreach (var i in q)
                {
                    emailAddress.Add(i);
                }
            }
            var currEmail = currUser.email;
            emailAddress.Add(currEmail);
            emailAddress.Add("hcd-staffing@kmbi.org.ph");
            var cc = string.Join(", ", emailAddress);
            //SEND EMAIL

            var sb = new StringBuilder();
            sb.Append("Dear <b>" + acc.firstName + " " + acc.lastName + "</b>, <br/><br/>");
            sb.Append("Congratulations!<br/><br/>");
            sb.Append("We wish to inform you that you have PASSED the three (3) day Gideon Training Program, thus, please expect a call/text or email from our team for the next process.<br/>");
            sb.Append("Thank you and God bless! <br/><br/>");
            sb.Append("Sincerely,<br/><br/>");
            sb.Append("<b>" + hrName + "</b><br/>");
            sb.Append(hrPosition + "<br/>");
            sb.Append("Contact No. 8291-1485 </b><br/><br/>");

            string to = acc.email;
            string subject = "KMBI Online Application";
            string body = sb.ToString();
            bool emailResponse = EmailHelper.SendEmail(to, subject, body, cc);

            // return RedirectToAction("Index","Home");
        }

        //NOTIFICATION FOR SUBMISSION OF REQUIREMENTS (HO & BO)
        [HttpPost]
        public void SubmissionRequirements(statuses data)
        {
            
            string currURL = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";

            var acc = _sysuser.getOne(data.applicantId);
            var slug = acc.slug;
            var link = currURL + "/JobApplications/UploadDocs?slug=" + slug;
            var job = _repo.getOneBySlugApplicant(data.jobSlug, data.applicantId);
            var id = _userManager.GetCurrentUser(this.HttpContext).id;
            var currUser = _sysuser.getOne(id);
            var hrName = currUser.firstName + " " + currUser.middleName.Substring(0, 1) + ". " + currUser.lastName;
            var hrPosition = currUser.position;
            //CCD
            var stringArrayOfbranches = job.branches;
            string branches = string.Join(", ", stringArrayOfbranches);
            List<string> listBranches = branches.Split(',').ToList();
            List<string> emailAddress = new List<string>();
            foreach (var item in listBranches)
            {
                var q = _sysuser.emailAddress(item);
                foreach (var i in q)
                {
                    emailAddress.Add(i);
                }
            }
            var currEmail = currUser.email;
            emailAddress.Add(currEmail);
            emailAddress.Add("hcd-staffing@kmbi.org.ph");
            var cc = string.Join(", ", emailAddress);

            if (branches.Contains("HEAD OFFICE"))
            {
                _repo.insertStatus(data);
                _repo.updateStatus(data.status, data.id);
            }
            //SEND EMAIL

            var sb = new StringBuilder();
            sb.Append("Dear <b>" + acc.firstName + " " + acc.lastName + "</b>, <br/><br/>");
            sb.Append("Good day! <br/><br/>");
            sb.Append("In behalf of the Human Capital Department, we would like to congratulate you for passing the overall assessment as you applied for " + job.jobPosition + " at KMBI. As part of the hiring process, kindly submit the following requirements:<br/>");
            sb.Append("<b><u>PRE-EMPLOYMENT REQUIREMENTS</u></b><br/><br/>");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.	Standard Company ID picture <i>(Colored Soft copy)</i><br/>");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a.	Female Attire: Blouse with collar and Blazer <br/>");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.	Photocopy of SSS Employment Static Report or E1 Copy for newly enrolled <br/>");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.	Photocopy of Philhealth & Pag ibig form/ID <br/>");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.	Photocopy of BIR Form 2316 <i>(Present year of employment)</i> <br/>");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.	Photocopy of TIN ID/form<br/>");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6.	Photocopy of School Records:   <br/>");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a.	College Diploma <br/>");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b.	Transcript of Records <i>(Certified by the School registrar)</i><br/>");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7.	Photocopy of Physical Examination Report <i>(updated)</i><br/>");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a.	Medical Certificate <br/>");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b.	X-ray result  <br/>");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c.	Drug Test result <i>(should be accredited by the DOH)</i> <br/>");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d.	Laboratory results; Urinalysis, Hematology & Fecalysis <br/>");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8.	Photocopy of Clearances:  <br/>");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a.	NBI Clearance result <i>( w/One year validity period)</i>  <br/>");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b.	Police Clearance or Barangay Clearance <br/>");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c.	Certificate of Employment/s <i>(if previously employed) (Pls. Provide contact numbers of all previous employers)</i> <br/>");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;9.	PSA copy of Birth certificate  <br/>");
            sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10. PSA copy of Marriage Certificate <i>(if Married)</i> <br/><br/>");
            sb.Append("Kindly upload the abovementioned requirements to " + link + "<br/><br/>");
            sb.Append("Should you have any question or concerns, feel free to contact the undersigned. <br/><br/>");
            sb.Append("Thank you and God bless! <br/><br/>");
            sb.Append("Sincerely,<br/><br/>");
            sb.Append("<b>" + hrName + "</b><br/>");
            sb.Append(hrPosition + "<br/>");
            sb.Append("Contact No. 8291-1485 </b><br/><br/>");

            string to = acc.email;
            string subject = "KMBI Online Application";
            string body = sb.ToString();
            bool emailResponse = EmailHelper.SendEmail(to, subject, body, cc);
            // return RedirectToAction("Index","Home");
        }

        //NOTIFICATION FOR 2ND SET OF EXAM (PO, AO & BAA)
        [HttpPost]
        public void SecondSetExam(statuses data)
        {
            _repo.insertStatus(data);
            _repo.updateStatus(data.status, data.id);
            var acc = _sysuser.getOne(data.applicantId);
            var job = _repo.getOneBySlugApplicant(data.jobSlug, data.applicantId);
            var id = _userManager.GetCurrentUser(this.HttpContext).id;
            var currUser = _sysuser.getOne(id);
            var hrName = currUser.firstName + " " + currUser.middleName.Substring(0, 1) + ". " + currUser.lastName;
            var hrPosition = currUser.position;
            //CCD
            var stringArrayOfbranches = job.branches;
            string branches = string.Join(", ", stringArrayOfbranches);
            List<string> listBranches = branches.Split(',').ToList();
            List<string> emailAddress = new List<string>();
            foreach (var item in listBranches)
            {
                var q = _sysuser.emailAddress(item);
                foreach (var i in q)
                {
                    emailAddress.Add(i);
                }
            }
            var currEmail = currUser.email;
            emailAddress.Add(currEmail);
            emailAddress.Add("hcd-staffing@kmbi.org.ph");
            var cc = string.Join(", ", emailAddress);
            //SEND EMAIL

            var sb = new StringBuilder();
            sb.Append("Dear <b>" + acc.firstName + " " + acc.lastName + "</b>, <br/><br/>");
            sb.Append("Good day! <br/><br/>");
            sb.Append("In line with your submitted requirements, we would like you to take the 2nd set of examination on " + data.inclusiveDate + " at " + branches + " <br/></br>");
            sb.Append("Thank you and God bless! <br/><br/>");
            sb.Append("Sincerely,<br/><br/>");
            sb.Append("<b>" + hrName + "</b><br/>");
            sb.Append(hrPosition + "<br/>");
            sb.Append("Contact No. 8291-1485 </b><br/><br/>");

            string to = acc.email;
            string subject = "KMBI Online Application";
            string body = sb.ToString();
            bool emailResponse = EmailHelper.SendEmail(to, subject, body, cc);
            // return RedirectToAction("Index","Home");
        }

        //DEPLOYMENT ADVISORY  
        [HttpPost]
        public void DEPLOYMENT(statuses data, string ccd)
        {
            _repo.insertStatus(data);
            _repo.updateStatus(data.status, data.id);
            var acc = _sysuser.getOne(data.applicantId);
            var job = _repo.getOneBySlugApplicant(data.jobSlug, data.applicantId);
            var id = _userManager.GetCurrentUser(this.HttpContext).id;
            var currUser = _sysuser.getOne(id);
            var hrName = currUser.firstName + " " + currUser.middleName.Substring(0, 1) + ". " + currUser.lastName;
            var hrPosition = currUser.position;
            //CCD
            var stringArrayOfbranches = job.branches;
            string branches = string.Join(", ", stringArrayOfbranches);
            List<string> listBranches = branches.Split(',').ToList();
            List<string> emailAddress = new List<string>();
            foreach (var item in listBranches)
            {
                var q = _sysuser.emailAddress(item);
                foreach (var i in q)
                {
                    emailAddress.Add(i);
                }
            }
            var currEmail = currUser.email;
            emailAddress.Add(currEmail);
            emailAddress.Add("hcd-staffing@kmbi.org.ph");
            emailAddress.Add(ccd);
            var cc = string.Join(", ", emailAddress);
            //SEND EMAIL

            var sb = new StringBuilder();
            sb.Append("Dear <b>" + acc.firstName + " " + acc.lastName + "</b>, <br/><br/>");
            sb.Append("Good day! <br/><br/>");
            sb.Append("The Human Capital Department is pleased to confirm your employment for " + job.jobPosition + " at the <b>Kabalikat Para sa Maunlad na Buhay Inc (A Microfinance NGO)</b>.  With this, please report on " + data.inclusiveDate.ToString("MMM dd, yyyy") + " at KMBI " + branches + ".  Please bring your two (2) valid IDs and a copy of your two (2) pcs 2x2 ID picture for processing of your ATM as well as your blazer or coat for taking of ID picture. <br/></br>");
            sb.Append("Further, kindly bring also the original copy of your requirements for authentication purposes and bring the Barangay Health Certificate from your Barangay Health Emergency Rescue Team (BHERT) declaring that you are not under PUI or PUM. The Health Certificate should be secured within 24 hours prior entering the office<br/><br/>");
            sb.Append("Please refer to the table below for company dress code: <br/><br/>");
            sb.Append("<table  style='border: thin;'>" +
                "<tr style='background-color:blue; color:white; text-align:center'><td style='border-style:solid; border-width:thin' colspan='2' ><b>MALE</b></td></tr>" +
                "<tr><td style='border-style:solid; border-width:thin'><b>Monday to Thursday</b></td>" +
                "<td style='border-style:solid; border-width:thin'><ul><li>Polo Barong or Polo with long or short sleeve </li>" +
                "<li>Slacks</li>" +
                "<li>Formal shoes(sandals and/or rubber shoes are not allowed)</li></ul></td></tr>" +
                "<tr><td style='border-style:solid; border-width:thin'><b>Friday</b></td>" +
                 "<td style='border-style:solid; border-width:thin'><b>Smart Casual</b></br>" +
                 "<ul><li>Polo shirt with collar </li>" +
                 "<li>Jeans (not rip jeans)  </li>" +
                 "<li>You can wear rubber shoes</li></ul></td></tr>" +
                 "<tr style='background-color:purple; color:white; text-align:center'><td style='border-style:solid; border-width:thin' colspan='2'><b>FEMALE</b></td></tr>" +
                  "<tr><td style='border-style:solid; border-width:thin'><b>Monday to Thursday</b></td>" +
                "<td style='border-style:solid; border-width:thin'><ul><li>Short-sleeved, Long-sleeved or blouse with collar  </li>" +
                "<li>Slacks or skirt </li>" +
                "<li>Formal shoes (flat shoes are acceptable except for sandals, jelly shoes and rubber shoes) </li></ul></td></tr>" +
                "<tr><td style='border-style:solid; border-width:thin'><b>Friday</b></td>" +
                 "<td style='border-style:solid; border-width:thin'><b>Smart Casual</b></br>" +
                 "<ul><li>Polo shirt with collar or blouse with collar </li>" +
                 "<li>Jeans (not rip jeans)  </li>" +
                 "<li>You can wear rubber shoes</li></ul></td></tr>" +
                "</table><br/><br/>");
            sb.Append("Again, congratulation and welcome to KMBI!<br/><br/>");
            sb.Append("Sincerely<br/><br/>");
            sb.Append("<b>" + hrName + "</b><br/>");
            sb.Append(hrPosition + "<br/>");
            sb.Append("Contact No. 8291-1485 </b><br/><br/>");

            string to = acc.email;
            string subject = "KMBI Online Application";
            string body = sb.ToString();
            bool emailResponse = EmailHelper.SendEmail(to, subject, body, cc);
            // return RedirectToAction("Index","Home");
        }

        [HttpPost]
        public void AddStatus(statuses data)
        {
            _repo.insertStatus(data);
            // return RedirectToAction("Index","Home");
        }

        [HttpGet]
        public List<statuses> GetStat(int jobId, int applicantId)
        {
            var model = _repo.GetStat(jobId, applicantId);
            return model;
        }

        [HttpGet]
        public statuses GetLastStat(int jobId, int applicantId)
        {
            var model = _repo.GetLastStat(jobId, applicantId);
            return model;
        }

        [HttpPost]
        public void UpdatePassed(int jobId, int applicantId, bool passed, string status)
        {
            _repo.updatePassed(jobId, applicantId, passed, status);
        }

        public IActionResult UploadDocs(int applicantId)
        {
            applicantId = _userManager.GetCurrentUser(HttpContext).id;
            var model = _repo.getAllDocs(applicantId);
            return View(model);
        }

        public IActionResult GetUploadDocs(int applicantId, string docNumber)
        {
            //applicantId = _userManager.GetCurrentUser(HttpContext).id;
            var model = _repo.getAllDocsSameDocNumber(applicantId, docNumber);
            return Json(new { data = model });
            //return model;
        }

        public IActionResult UploadedRequirements(int applicantId)
        {
            var model = _repo.getAllDocs(applicantId);
            return View(model);
        }

        public void UploadRequirements(documentsViewModel docs)
        {
            string filePicturePath;
            string currURL = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
            string filename;
            if (docs.file != null)
            {
                Stream stream;

                string UploadPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/requiredDocs/");
                filename = Path.GetFileName(docs.file.FileName);
                filename = filename.Trim().Replace(" ", "");
                filePicturePath = currURL + "/requiredDocs/" + filename;

                stream = new FileStream(UploadPath + filename, FileMode.Create, FileAccess.ReadWrite);

                docs.file.CopyTo(stream);
                stream.Close();
            }
            else
            {
                filePicturePath = "";
                filename = "";
            }
            documents viewModel = new documents();
            viewModel.id = docs.id;
            viewModel.applicantId = docs.applicantId;
            viewModel.docNumber = docs.docNumber;
            viewModel.filePath = filePicturePath;
            viewModel.filename = filename;
            _repo.insertDocs(viewModel);
        }

        public void DeleteDocs(int applicantId, string docNumber, string filename)
        {
            _repo.deleteDocs(applicantId, docNumber, filename);
        }

        public void UpdateComments(int applicantId, string docNumber, string comments)
        {
           _repo.updateDocs(applicantId, docNumber, comments);
        }

        //public List<documents GetAllDocs(int applicantId, string docNumber)
        //{
        //    var model = _repo.getAllDocs(applicantId, docNumber);
        //    return model;

        //}
        //[HttpGet]
        //public FileContentResult DownloadPDF()
        //{
        //    string HTMLContent = "<html lang='en'><head>" +
        //        "<meta charset='UTF-8'>" +
        //        "<meta http-equiv='X-UA-Compatible' content='IE=edge'>" +
        //        "<meta name='viewport' content='width=device-width, initial-scale=1.0'>" +
        //        "<link rel='preconnect' href='https://fonts.gstatic.com'>" +
        //        "<link href='https://fonts.googleapis.com/css2?family=Roboto&display=swap' rel='stylesheet'>" +
        //        "<title>Title Here</title>" +
        //        "<style>" +
        //        "body {width: 1368px;margin: auto;font-family: 'Roboto', sans-serif;}" +
        //        ".container {padding: 20px;}" +
        //        "table, td, th {border: 1px solid #000;padding: 10px;}" +
        //        "table {width: 100%;border-collapse: collapse;}" +
        //        ".section-title {background-color: #292929;position: relative;width: 4%;border-bottom: 2px solid #fff;}" +
        //        ".section-title > div {position: absolute;transform: translateX(45%) rotate(-90deg);height: 80px;max-width: 50px;white-space: nowrap;display: block;font-weight: 900;color: #fff;bottom: 0;}}" +
        //        ".section-title .fix-line {max-width: 40px;}" +
        //        ".header {}" +
        //        ".header .form-title {background: #292929;color: #fff;font-weight: 900;}" +
        //        ".personal-data span {font-weight: 900;}" +
        //        ".fam-background {min-height: 250px;}" +
        //        ".fam-background .header-title {border-bottom: 1px solid transparent; height: 1px;background: #e7e7e7;padding: 0px 10px;}" +
        //        ".fam-background .sub-title {height: 1px;background: #e7e7e7;padding: 0px 10px;}" +
        //        "</style>" +
        //        "</head><body>" +
        //        "<div>sadfasdfsdaf</div>" +
        //        "</body></html>";
        //    Response.Clear();
        //    //Response.ContentType = "application/pdf";
        //    byte[] doc = PDF.GetPDF(HTMLContent);
        //    string mimeType = "application/pdf";
        //    Response.Headers.Add("content-disposition", "inline;filename=" + "PDFfile.pdf");
        //    //Response.Headers[HeaderNames.CacheControl] = "no-cache";
        //    // Response..SetCacheability(HttpCacheability.NoCache);
        //    //Response.Body.Write(PDF.GetPDF(HTMLContent),0, PDF.GetPDF(HTMLContent).Length);
        //    //Response.Clear();
        //    return File(doc, mimeType);
        //}
    }
}
