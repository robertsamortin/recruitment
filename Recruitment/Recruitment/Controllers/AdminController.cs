﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Recruitment.Models;
using Recruitment.Models.IRepo;

namespace Recruitment.Controllers
{
   [Authorize]
    public class AdminController : Controller
    {
        private iUserManager _userManager;
        iAccountRepository _user;
        iAdminUsers _repo;
        iSkills _skill;

        public AdminController(iUserManager userManager, iAccountRepository user, iAdminUsers repo, iSkills skill)
        {
            _userManager = userManager;
            _user = user;
            _repo = repo;
            _skill = skill;
        }

        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {

                var id = _userManager.GetCurrentUser(this.HttpContext).id;
                var model = _user.getInfoAdminDetails(id);
                if (model != null)
                {
                    return View(model);
                }
                else
                {
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }           
        }

        [HttpPost]
        public void Update(systemUsers data)
        {
            _repo.update(data);
            //return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public void UpdateSkills(skills data)
        {
            _skill.update(data);
            //return RedirectToAction("Index", "Home");
        }

        public IActionResult Users()
        {
            if (User.Identity.IsAuthenticated)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public IActionResult SkillsMaintenance()
        {
            if (User.Identity.IsAuthenticated)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [HttpPost]
        public void Add(systemUsers data)
        {
            var hashedPassword = BCrypt.Net.BCrypt.HashPassword(data.password);
            data.password = hashedPassword;
            _repo.insert(data);
            // return RedirectToAction("Index","Home");
        }

        [HttpPost]
        public void AddSkills(skills data)
        {
           
            _skill.insert(data);
            // return RedirectToAction("Index","Home");
        }

        [HttpDelete]
        public void Delete(int id)
        {
            _repo.delete(id);
            //return RedirectToAction("Index", "Home");
        }

        [HttpDelete]
        public void DeleteSkills(int id)
        {
            _skill.delete(id);
            //return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult GetAll()
        {
            var result = _repo.getAll();
            return Json(new { data = result });
        }

        [HttpGet]
        public ActionResult GetAllSkills()
        {
            var result = _skill.getAll();
            return Json(new { data = result });
        }

        [HttpGet]
        public systemUsers GetOne(int id)
        {
            var result = _repo.getOne(id);
            // return Json(new { data = result });
            return result;
        }

        [HttpGet]
        public skills GetOneSkills(int id)
        {
            var result = _skill.getOne(id);
            // return Json(new { data = result });
            return result;
        }

        [HttpGet]
        public IActionResult Skills(string searchString)
        {
            ViewData["searchString"] = searchString;
            var model =  _user.getSkills(searchString); 
            return View(model);
        }
    }
}
