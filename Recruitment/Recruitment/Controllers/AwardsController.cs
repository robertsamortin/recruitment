﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Recruitment.Models;
using Recruitment.Models.IRepo;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Recruitment.Controllers
{
    public class AwardsController : Controller
    {
        private iAwards _repo;

        public AwardsController(iAwards repo)
        {
            _repo = repo;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public void UpdateAwards(appAwards data)
        {
            _repo.updateAwards(data);
            //return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public void AddAwards(appAwards data)
        {
            _repo.insertAwards(data);
           // return RedirectToAction("Index","Home");
        }

        [HttpDelete]
        public void DeleteAwards(int id)
        {
            _repo.deleteAwards(id);
            //return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult GetAwards(int id)
        {
            var result = _repo.getAwards(id);
            return Json(new { data = result });
        }

        [HttpGet]
        public appAwardsViewModel GetSingleAwards(int id)
        {
            var result = _repo.getSingleAwards(id);
            // return Json(new { data = result });
            return result;
        }
    }
}
