﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models
{
    public class appEmploymentViewModel
    {
        public int id { get; set; }
        public int applicantId { get; set; }
        public string employer { get; set; }
        public string position { get; set; }
        public string department { get; set; }
        public string address { get; set; }
        public string jobFunctions { get; set; }
        public string isName { get; set; }
        public string isPosition { get; set; }
        public string contact { get; set; }
        public string appointmentStatus { get; set; }
        public string empFrom { get; set; }
        public string empTo { get; set; }
        public bool empPresent { get; set; }
        public string startSalary { get; set; }
        public double endSalary { get; set; }
        public string reasonLeaving { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }
}
