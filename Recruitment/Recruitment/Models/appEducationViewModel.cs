﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models
{
    public class appEducationViewModel
    {
        public int id { get; set; }
        public int applicantId { get; set; }
        public string elementary { get; set; }
        public string elemAddress { get; set; }
        public string elemDegree { get; set; }
        public string elemFrom { get; set; }
        public string elemTo { get; set; }
        public string elemAwards { get; set; }
        public string highSchool { get; set; }
        public string highAddress { get; set; }
        public string highDegree { get; set; }
        public string highFrom { get; set; }
        public string highTo { get; set; }
        public string highAwards { get; set; }
        public string college { get; set; }
        public string collAddress { get; set; }
        public string collDegree { get; set; }
        public string collCourse { get; set; }
        public string collFrom { get; set; }
        public string collTo { get; set; }
        public string collAwards { get; set; }
        public string others { get; set; }
        public string othAddress { get; set; }
        public string othDegree { get; set; }
        public string othFrom { get; set; }
        public string othTo { get; set; }
        public string othAwards { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }
}
