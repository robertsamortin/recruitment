﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using Recruitment.Models.IRepo;

namespace Recruitment.Models.Repo
{
    public class AccountRepository : iAccountRepository
    {
        //private readonly AppDbContext _db;
        private readonly SQLCon _db;
        public AccountRepository(SQLCon db)
        {
            _db = db;
        }

        public systemUsers login(string email)
        {
            return _db.systemUsers.Where(u => u.email == email).Where(e => e.isVerified == true).FirstOrDefault();
        }

        public systemUsers adminLogin(string email)
        {
            return _db.systemUsers.Where(u => u.email == email).FirstOrDefault();
        }

        public adminPersonalViewModel getInfoAdminDetails(int id)
        {
            adminPersonalViewModel viewModel = new adminPersonalViewModel();
            var pUsers = _db.systemUsers.Where(e => e.id == id).FirstOrDefault();
            var pPersonal = _db.appPersonalData.Where(e => e.applicantId == id).FirstOrDefault();
            viewModel.systemUsers = new systemUsers
            {
                id = pUsers.id,
                slug = pUsers.slug?.Trim(),
                lastName = pUsers.lastName?.Trim(),
                firstName = pUsers.firstName?.Trim(),
                middleName = pUsers.middleName?.Trim(),
                email = pUsers.email?.Trim(),
                position = pUsers.position?.Trim(),
                branch = pUsers.branch,
                referenceNo = pUsers.referenceNo
            };
            if (pPersonal != null) {
                viewModel.picture = pPersonal.picture;
            }
             return viewModel;

        }
        public applicationViewModel getInfoDetails(int id)
        {
            applicationViewModel viewModel = new applicationViewModel();

            //GET personal data
            var pUsers = _db.systemUsers.Where(e => e.id == id).FirstOrDefault();
            var pPersonal = _db.appPersonalData.Where(e => e.applicantId == id).FirstOrDefault();
            //GET education
            var pEducation = _db.appEducation.Where(e => e.applicantId == id).FirstOrDefault();
            //GET other
            var pOthers = _db.appOtherInformation.Where(e => e.applicantId == id).FirstOrDefault();
            //GET declaration
            var pDeclaration = _db.appDeclaration.Where(e => e.applicantId == id).FirstOrDefault();
            //GET references
            var pReferences = _db.appReferences.Where(e => e.applicantId == id).FirstOrDefault();
            ////GET Job applications
            ////var pJobApplications = _db.jobApplications.Where(e => e.applicantId == id).ToList();
            //List<JobApplicationViewModel> JobApplications = new List<JobApplicationViewModel>();
            //var pJobApplications = _db.jobApplications.Where(e => e.applicantId == id).ToList();
            //if (pJobApplications.Count() > 0)
            //{
            //    foreach (var item in pJobApplications)
            //    {
            //        var jobVac = _db.jobVacancies.Where(c => c.slug == item.jobSlug).FirstOrDefault();
            //        //var appData = _db.appPersonalData.Where(c => c.applicantId == item.applicantId).FirstOrDefault();
            //        //var sysUsers = _db.systemUsers.Where(c => c.id == item.applicantId).FirstOrDefault();

            //        //get branch name
            //        List<string> slug = new List<string> { "" };
            //        List<string> branches = new List<string>();

            //        var branchName = "";
            //        string Joined = "";
            //        if (item.branch != null)
            //        {
            //            slug = item.branch.Split(',').ToList();
            //            foreach (var i in slug)
            //            {
            //                branchName = _db.branch.Where(e => e.slug == i).FirstOrDefault().name;
            //                branches.Add(branchName);
            //            }
            //            Joined = string.Join(",", branches).Replace(",", "\n");
            //        }
            //        //

            //        JobApplications.Add(new JobApplicationViewModel
            //        {
            //            id = item.id,
            //            applicantId = item.applicantId,
            //            slug = item.slug,
            //            picture = pPersonal.picture,
            //            resume = pPersonal.resume,
            //            lastName = pUsers.lastName,
            //            firstName = pUsers.firstName,
            //            middleName = pUsers.middleName,
            //            jobSlug = item.jobSlug,
            //            dateApplied = item.dateFiled.ToString("MMM, dd, yyyy"),
            //            jobPosition = jobVac.jobPosition,
            //            jobSummary = jobVac.jobSummary,
            //            jobQualifications = jobVac.jobSummary,
            //            branches = Joined,
            //            status = item.status,
            //            remarks = item.remarks,
            //        });
            //    }
            //    viewModel.jobApplications = JobApplications;
            //}

            //GET Job firstchoice
            jobVacancies pJobFirst = new jobVacancies();
            jobVacancies pJobSecond = new jobVacancies();
            jobVacancies pJobThird = new jobVacancies();
            var firstBranchName = "";
            var secondBranchName = "";
            var thirdBranchName = "";
            if (pPersonal != null)
            {
                pJobFirst = _db.jobVacancies.Where(e => e.slug == pPersonal.firstChoice).FirstOrDefault();
                if (pJobFirst != null)
                {
                    firstBranchName = _db.branch.Where(e => e.slug == pJobFirst.branches).FirstOrDefault().name;
                }
                
                //GET Job secondchoice
                pJobSecond = _db.jobVacancies.Where(e => e.slug == pPersonal.secondChoice).FirstOrDefault();
                if (pJobSecond != null)
                {
                    secondBranchName = _db.branch.Where(e => e.slug == pJobSecond.branches).FirstOrDefault().name;
                }
                
                //GET Job thirdchoice
                pJobThird = _db.jobVacancies.Where(e => e.slug == pPersonal.thirdChoice).FirstOrDefault();
                if (pJobThird != null)
                {
                    thirdBranchName = _db.branch.Where(e => e.slug == pJobThird.branches).FirstOrDefault().name;
                }
                
            }

            
            viewModel.systemUsers = new systemUsers
            {
                id = pUsers.id,
                slug = pUsers.slug?.Trim(),
                lastName = pUsers.lastName?.Trim(),
                firstName = pUsers.firstName?.Trim(),
                middleName = pUsers.middleName?.Trim(),
                email = pUsers.email?.Trim(),
            };

            if (pPersonal != null)
            {
                viewModel.appPersonalData = new appPersonalData
                {
                    dateFiled = pPersonal.dateFiled,
                    referredBy = pPersonal.referredBy?.Trim(),
                    aboutMe = pPersonal.aboutMe?.Trim(),
                    facebook = pPersonal.facebook?.Trim(),
                    instagram = pPersonal.instagram?.Trim(),
                    twitter = pPersonal.twitter?.Trim(),
                    desiredSalary = pPersonal.desiredSalary,
                    negotiable = pPersonal.negotiable,
                    firstChoice = pPersonal.firstChoice?.Trim(),
                    secondChoice = pPersonal.secondChoice?.Trim(),
                    thirdChoice = pPersonal.thirdChoice?.Trim(),
                    nickName = pPersonal.nickName?.Trim(),
                    sex = pPersonal.sex?.Trim(),
                    bloodType = pPersonal.bloodType?.Trim(),
                    picture = pPersonal.picture?.Trim(),
                    resume = pPersonal.resume?.Trim(),
                    birthPlace = pPersonal.birthPlace?.Trim(),
                    citizenship = pPersonal.citizenship?.Trim(),
                    weight = pPersonal.weight?.Trim(),
                    height = pPersonal.height?.Trim(),
                    civilStatus = pPersonal.civilStatus?.Trim(),
                    religiousAffiliation = pPersonal.religiousAffiliation?.Trim(),
                    sss = pPersonal.sss?.Trim(),
                    tin = pPersonal.tin?.Trim(),
                    taxStatus = pPersonal.taxStatus?.Trim(),
                    pagibig = pPersonal.pagibig?.Trim(),
                    philhealth = pPersonal.philhealth?.Trim(),
                    landline = pPersonal.landline?.Trim(),
                    mobile = pPersonal.mobile?.Trim(),
                    presentAddress = pPersonal.presentAddress?.Trim(),
                    permanentAddress = pPersonal.permanentAddress?.Trim(),
                    permanentAddressContact = pPersonal.permanentAddressContact?.Trim(),
                    permanentAddressZipCode = pPersonal.permanentAddressZipCode?.Trim(),
                    otherAddress = pPersonal.otherAddress?.Trim(),
                    otherAddressContact = pPersonal.otherAddressContact?.Trim(),
                    otherAddressZipCode = pPersonal.otherAddressZipCode?.Trim(),
                    yearsStay = pPersonal.yearsStay,
                    own = pPersonal.own,
                    rent = pPersonal.rent,
                    rentAmount = pPersonal.rentAmount,
                    liveWith = pPersonal.liveWith?.Trim(),
                    location = pPersonal.location?.Trim()
                };
            }
            
            //EDUCATION
            if (pEducation != null)
            {
                viewModel.appEducation = new appEducation
                {
                    elementary = pEducation.elementary?.Trim(),
                    elemAddress = pEducation.elemAddress?.Trim(),
                    elemDegree = pEducation.elemDegree?.Trim(),
                    elemFrom = pEducation.elemFrom,
                    elemTo = pEducation.elemTo,
                    elemAwards = pEducation.elemAwards?.Trim(),
                    highSchool = pEducation.highSchool?.Trim(),
                    highAddress = pEducation.highAddress?.Trim(),
                    highDegree = pEducation.highDegree?.Trim(),
                    highFrom = pEducation.highFrom,
                    highTo = pEducation.highTo,
                    highAwards = pEducation.highAwards?.Trim(),
                    college = pEducation.college?.Trim(),
                    collAddress = pEducation.collAddress?.Trim(),
                    collDegree = pEducation.collDegree?.Trim(),
                    collCourse = pEducation.collCourse?.Trim(),
                    collFrom = pEducation.collFrom,
                    collTo = pEducation.collTo,
                    collAwards = pEducation.collAwards?.Trim(),
                    others = pEducation.others?.Trim(),
                    othAddress = pEducation.othAddress?.Trim(),
                    othDegree = pEducation.othDegree?.Trim(),
                    othFrom = pEducation.othFrom,
                    othTo = pEducation.othTo,
                    othAwards = pEducation.othAwards?.Trim(),
                };
            }

            //OTHER INFO
            if (pOthers != null)
            {
                viewModel.appOtherInformation = new appOtherInformation
                {
                    slanguage1 = pOthers.slanguage1?.Trim(),
                    slanguage2 = pOthers.slanguage2?.Trim(),
                    slanguage3 = pOthers.slanguage3?.Trim(),
                    wlanguage1 = pOthers.wlanguage1?.Trim(),
                    wlanguage2 = pOthers.wlanguage2?.Trim(),
                    wlanguage3 = pOthers.wlanguage3?.Trim(),
                    officeMachine = pOthers.officeMachine?.Trim(),
                    hobbies = pOthers.hobbies?.Trim(),
                    vehicles = pOthers.vehicles?.Trim(),
                    workFromHome = pOthers.workFromHome
                };
            }

            //DECLARATION
            if (pDeclaration != null)
            {
                viewModel.appDeclaration = new appDeclaration
                {
                    kmbiRelative = pDeclaration.kmbiRelative,
                    kmbiRelativePosition = pDeclaration.kmbiRelativePosition?.Trim(),
                    cCase = pDeclaration.cCase,
                    cCaseDetail = pDeclaration.cCaseDetail?.Trim(),
                    discharged = pDeclaration.discharged,
                    dischargedDetails = pDeclaration.dischargedDetails?.Trim(),
                    illness = pDeclaration.illness,
                    illnessDetails = pDeclaration.illnessDetails?.Trim(),
                    incaseEmergency = pDeclaration.incaseEmergency?.Trim(),
                    incaseContact = pDeclaration.incaseContact?.Trim(),
                    incaseRelationship = pDeclaration.incaseRelationship?.Trim()
                };
            }

            //REFERENCES
            if (pReferences != null)
            {
                viewModel.appReferences = new appReferences
                {
                    ref1Name = pReferences.ref1Name?.Trim(),
                    ref1Occupation = pReferences.ref1Occupation?.Trim(),
                    ref1Contact = pReferences.ref1Contact?.Trim(),
                    ref2Name = pReferences.ref2Name?.Trim(),
                    ref2Occupation = pReferences.ref2Occupation?.Trim(),
                    ref2Contact = pReferences.ref2Contact?.Trim(),
                    ref3Name = pReferences.ref3Name?.Trim(),
                    ref3Occupation = pReferences.ref3Occupation?.Trim(),
                    ref3Contact = pReferences.ref3Contact?.Trim()
                };
            }

            //JOB VACANCIES
            if (pJobFirst != null)
            {
                viewModel.firstJobVacancies = new jobVacancies
                {
                    jobPosition = pJobFirst.jobPosition,
                    jobSummary = pJobFirst.jobSummary,
                    jobQualifications = pJobFirst.jobQualifications,
                    branches = firstBranchName,
                    department = pJobFirst.department
                };
            }
            if (pJobSecond != null)
            {
                viewModel.secondJobVacancies = new jobVacancies
                {
                    jobPosition = pJobSecond.jobPosition,
                    jobSummary = pJobSecond.jobSummary,
                    jobQualifications = pJobSecond.jobQualifications,
                    branches = secondBranchName,
                    department = pJobSecond.department
                };
            }
            if (pJobThird != null)
            {
                viewModel.thirdJobVacancies = new jobVacancies
                {
                    jobPosition = pJobThird.jobPosition,
                    jobSummary = pJobThird.jobSummary,
                    jobQualifications = pJobThird.jobQualifications,
                    branches = thirdBranchName,
                    department = pJobThird.department
                };
            }

            //JOB APPLIED


            return viewModel;


            ////FAMILY
            //List<appFamilyBackground> familyData = new List<appFamilyBackground>();
            //if (pFamily != null)
            //{
            //    foreach (var item in pFamily)
            //    {
            //        familyData.Add(new appFamilyBackground
            //        {
            //            name = item.name?.Trim(),
            //            birthDate = item.birthDate,
            //            relationship = item.relationship?.Trim(),
            //            occupation = item.occupation?.Trim(),
            //            school = item.school?.Trim(),
            //            course = item.course?.Trim(),
            //            contact = item.contact?.Trim()
            //        });
            //    }
            //    viewModel.appFamilyBackground = familyData;
            //}

        }

        public void updatePData(applicationViewModel data, int id)
        {
            var sysUsers = _db.systemUsers.Where(e => e.id == id);
            var sysRes = sysUsers.FirstOrDefault();
            if (sysUsers.Count() > 0)
            {
                sysRes.lastName = data.systemUsers.lastName?.ToUpper();
                sysRes.firstName = data.systemUsers.firstName?.ToUpper();
                sysRes.middleName = data.systemUsers.middleName?.ToUpper();
                sysRes.email = data.systemUsers.email;
            }

            var model = _db.appPersonalData.Where(e => e.applicantId == id);
            var res = model.FirstOrDefault();

            if (model.Count() > 0)
            {
                //jobapplications
                var job1 = _db.jobApplications.Where(e => e.applicantId == id).Where(e => e.jobSlug == res.firstChoice).Where(e => e.remarks == "first choice");
                var resJob1 = job1.FirstOrDefault();

                if (job1.Count() > 0)
                {
                    resJob1.jobSlug = data.appPersonalData.firstChoice;
                    resJob1.dateFiled = DateTime.Now;
                    resJob1.status = "active";
                    resJob1.remarks = "first choice";
                    resJob1.branch = data.firstBranchSlug;
                    resJob1.department = data.firstJobVacancies.department;
                    resJob1.updatedAt = DateTime.Now;
                }
                else
                {
                    if (data.appPersonalData.firstChoice != null && data.appPersonalData.firstChoice != "")
                    {
                        jobApplications jobApp1 = new jobApplications();
                        jobApp1.applicantId = sysRes.id;
                        jobApp1.slug = global.slugify(24);
                        jobApp1.jobSlug = data.appPersonalData.firstChoice;
                        jobApp1.dateFiled = DateTime.Now;
                        jobApp1.status = "active";
                        jobApp1.remarks = "first choice";
                        jobApp1.branch = data.firstBranchSlug;
                        jobApp1.department = data.firstJobVacancies.department;
                        jobApp1.createdAt = DateTime.Now;
                        jobApp1.updatedAt = DateTime.Now;
                        _db.jobApplications.Add(jobApp1);
                    }
                }


                var job2 = _db.jobApplications.Where(e => e.applicantId == id).Where(e => e.jobSlug == res.secondChoice).Where(e => e.remarks == "second choice");
                var resJob2 = job2.FirstOrDefault();

                if (job2.Count() > 0)
                {
                    resJob2.jobSlug = data.appPersonalData.secondChoice;
                    resJob2.dateFiled = DateTime.Now;
                    resJob2.status = "active";
                    resJob2.remarks = "second choice";
                    resJob2.branch = data.secondBranchSlug;
                    resJob2.department = data.secondJobVacancies.department;
                    resJob2.updatedAt = DateTime.Now;
                }
                else
                {
                    if (data.appPersonalData.secondChoice != null && data.appPersonalData.secondChoice != "")
                    {
                        jobApplications jobApp2 = new jobApplications();
                        jobApp2.applicantId = sysRes.id;
                        jobApp2.slug = global.slugify(24);
                        jobApp2.jobSlug = data.appPersonalData.secondChoice;
                        jobApp2.dateFiled = DateTime.Now;
                        jobApp2.status = "active";
                        jobApp2.remarks = "second choice";
                        jobApp2.branch = data.secondBranchSlug;
                        jobApp2.department = data.secondJobVacancies.department;
                        jobApp2.createdAt = DateTime.Now;
                        jobApp2.updatedAt = DateTime.Now;
                        _db.jobApplications.Add(jobApp2);
                    }
                }


                var job3 = _db.jobApplications.Where(e => e.applicantId == id).Where(e => e.jobSlug == res.thirdChoice).Where(e => e.remarks == "third choice");
                var resJob3 = job3.FirstOrDefault();

                if (job3.Count() > 0)
                {
                    resJob3.jobSlug = data.appPersonalData.thirdChoice;
                    resJob3.dateFiled = DateTime.Now;
                    resJob3.status = "active";
                    resJob3.remarks = "third choice";
                    resJob3.branch = data.thirdBranchSlug;
                    resJob3.department = data.thirdJobVacancies.department;
                    resJob3.updatedAt = DateTime.Now;
                }
                else
                {
                    if (data.appPersonalData.thirdChoice != null && data.appPersonalData.thirdChoice != "")
                    {
                        jobApplications jobApp3 = new jobApplications();
                        jobApp3.applicantId = sysRes.id;
                        jobApp3.slug = global.slugify(24);
                        jobApp3.jobSlug = data.appPersonalData.thirdChoice;
                        jobApp3.dateFiled = DateTime.Now;
                        jobApp3.status = "active";
                        jobApp3.remarks = "third choice";
                        jobApp3.branch = data.thirdBranchSlug;
                        jobApp3.department = data.thirdJobVacancies.department;
                        jobApp3.createdAt = DateTime.Now;
                        jobApp3.updatedAt = DateTime.Now;
                        _db.jobApplications.Add(jobApp3);
                    }
                }

                //STATUSES
                //var s = _db.statuses.Where(e => e.applicantId == id);
                //var resS = s.FirstOrDefault();
                //if (s.Count() > 0)
                //{
                //    resS.status = "active";
                //    resS.inclusiveDate = DateTime.Now;
                //    resS.currentStatus = true;
                //}
                //else
                //{
                //    statuses statData = new statuses();
                //    statData.applicantId = id;
                //    statData.status = "active";
                //    statData.inclusiveDate = DateTime.Now;
                //    statData.currentStatus = true;
                //    _db.statuses.Add(statData);
                //}

                res.aboutMe = data.appPersonalData.aboutMe?.ToUpper();
                res.dateFiled = DateTime.Now;
                res.referredBy = data.appPersonalData.referredBy;
                res.facebook = data.appPersonalData.facebook;
                res.instagram = data.appPersonalData.instagram;
                res.twitter = data.appPersonalData.twitter;
                res.desiredSalary = data.appPersonalData.desiredSalary;
                res.negotiable = data.appPersonalData.negotiable;
                res.firstChoice = data.appPersonalData.firstChoice;
                res.secondChoice = data.appPersonalData.secondChoice;
                res.thirdChoice = data.appPersonalData.thirdChoice;
                res.nickName = data.appPersonalData.nickName?.ToUpper();
                res.sex = data.appPersonalData.sex?.ToUpper();
                res.bloodType = data.appPersonalData.bloodType?.ToUpper();
                res.picture = data.appPersonalData.picture;
                res.resume = data.appPersonalData.resume;
                res.birthPlace = data.appPersonalData.birthPlace?.ToUpper();
                res.citizenship = data.appPersonalData.citizenship?.ToUpper();
                res.weight = data.appPersonalData.weight;
                res.height = data.appPersonalData.height;
                res.civilStatus = data.appPersonalData.civilStatus?.ToUpper();
                res.religiousAffiliation = data.appPersonalData.religiousAffiliation?.ToUpper();
                res.sss = data.appPersonalData.sss;
                res.tin = data.appPersonalData.tin;
                res.taxStatus = data.appPersonalData.taxStatus?.ToUpper();
                res.pagibig = data.appPersonalData.pagibig;
                res.philhealth = data.appPersonalData.philhealth;
                res.landline = data.appPersonalData.landline;
                res.mobile = data.appPersonalData.mobile;
                res.presentAddress = data.appPersonalData.presentAddress;
                res.otherAddressContact = data.appPersonalData.otherAddressContact;
                res.otherAddressZipCode = data.appPersonalData.otherAddressZipCode;
                res.permanentAddress = data.appPersonalData.permanentAddress;
                res.permanentAddressContact = data.appPersonalData.permanentAddressContact;
                res.permanentAddressZipCode = data.appPersonalData.permanentAddressZipCode;
                res.otherAddress = data.appPersonalData.otherAddress;
                res.yearsStay = data.appPersonalData.yearsStay;
                res.own = data.appPersonalData.own;
                res.rent = data.appPersonalData.rent;
                res.rentAmount = data.appPersonalData.rentAmount;
                res.liveWith = data.appPersonalData.liveWith;
                res.location = data.appPersonalData.location;
                res.updatedAt = DateTime.Now;

                _db.SaveChanges();
            }
            else
            {
                appPersonalData appPPdata = new appPersonalData();
                appPPdata.applicantId = sysRes.id;
                appPPdata.dateFiled = data.appPersonalData.dateFiled;
                appPPdata.aboutMe = data.appPersonalData.aboutMe?.ToUpper();
                appPPdata.referredBy = data.appPersonalData.referredBy;
                appPPdata.facebook = data.appPersonalData.facebook;
                appPPdata.instagram = data.appPersonalData.instagram;
                appPPdata.twitter = data.appPersonalData.twitter;
                appPPdata.desiredSalary = data.appPersonalData.desiredSalary;
                appPPdata.negotiable = data.appPersonalData.negotiable;
                appPPdata.firstChoice = data.appPersonalData.firstChoice;
                appPPdata.secondChoice = data.appPersonalData.secondChoice;
                appPPdata.thirdChoice = data.appPersonalData.thirdChoice;
                appPPdata.nickName = data.appPersonalData.nickName?.ToUpper();
                appPPdata.sex = data.appPersonalData.sex?.ToUpper();
                appPPdata.bloodType = data.appPersonalData.bloodType?.ToUpper();
                appPPdata.picture = data.appPersonalData.picture;
                appPPdata.resume = data.appPersonalData.resume;
                appPPdata.birthPlace = data.appPersonalData.birthPlace?.ToUpper();
                appPPdata.citizenship = data.appPersonalData.citizenship?.ToUpper();
                appPPdata.weight = data.appPersonalData.weight;
                appPPdata.height = data.appPersonalData.height;
                appPPdata.civilStatus = data.appPersonalData.civilStatus?.ToUpper();
                appPPdata.religiousAffiliation = data.appPersonalData.religiousAffiliation?.ToUpper();
                appPPdata.sss = data.appPersonalData.sss;
                appPPdata.tin = data.appPersonalData.tin;
                appPPdata.taxStatus = data.appPersonalData.taxStatus?.ToUpper();
                appPPdata.pagibig = data.appPersonalData.pagibig;
                appPPdata.philhealth = data.appPersonalData.philhealth;
                appPPdata.landline = data.appPersonalData.landline;
                appPPdata.mobile = data.appPersonalData.mobile;
                appPPdata.presentAddress = data.appPersonalData.presentAddress;
                appPPdata.otherAddressContact = data.appPersonalData.otherAddressContact;
                appPPdata.otherAddressZipCode = data.appPersonalData.otherAddressZipCode;
                appPPdata.permanentAddress = data.appPersonalData.permanentAddress;
                appPPdata.permanentAddressContact = data.appPersonalData.permanentAddressContact;
                appPPdata.permanentAddressZipCode = data.appPersonalData.permanentAddressZipCode;
                appPPdata.otherAddress = data.appPersonalData.otherAddress;
                appPPdata.yearsStay = data.appPersonalData.yearsStay;
                appPPdata.own = data.appPersonalData.own;
                appPPdata.rent = data.appPersonalData.rent;
                appPPdata.rentAmount = data.appPersonalData.rentAmount;
                appPPdata.liveWith = data.appPersonalData.liveWith;
                appPPdata.location = data.appPersonalData.location;
                appPPdata.createdAt = DateTime.Now;
                appPPdata.updatedAt = DateTime.Now;
                _db.appPersonalData.Add(appPPdata);

                //jobapplications
                jobApplications jobApp1 = new jobApplications();
                jobApp1.applicantId = sysRes.id;
                jobApp1.slug = global.slugify(24);
                jobApp1.jobSlug = data.appPersonalData.firstChoice;
                jobApp1.dateFiled = DateTime.Now;
                jobApp1.status = "active";
                jobApp1.remarks = "first choice";
                jobApp1.branch = data.firstBranchSlug;
                jobApp1.createdAt = DateTime.Now;
                jobApp1.updatedAt = DateTime.Now;
                _db.jobApplications.Add(jobApp1);

                jobApplications jobApp2 = new jobApplications();
                jobApp2.applicantId = sysRes.id;
                jobApp2.slug = global.slugify(24);
                jobApp2.jobSlug = data.appPersonalData.secondChoice;
                jobApp2.dateFiled = DateTime.Now;
                jobApp2.status = "active";
                jobApp2.remarks = "second choice";
                jobApp2.branch = data.secondBranchSlug;
                jobApp2.createdAt = DateTime.Now;
                jobApp2.updatedAt = DateTime.Now;
                _db.jobApplications.Add(jobApp2);

                if (data.appPersonalData.thirdChoice != null && data.appPersonalData.thirdChoice != "")
                {
                    jobApplications jobApp3 = new jobApplications();
                    jobApp3.applicantId = sysRes.id;
                    jobApp3.slug = global.slugify(24);
                    jobApp3.jobSlug = data.appPersonalData.thirdChoice;
                    jobApp3.dateFiled = DateTime.Now;
                    jobApp3.status = "active";
                    jobApp3.remarks = "third choice";
                    jobApp3.branch = data.thirdBranchSlug;
                    jobApp3.createdAt = DateTime.Now;
                    jobApp3.updatedAt = DateTime.Now;
                    _db.jobApplications.Add(jobApp3);
                }

                _db.SaveChanges();
            }


            //update EDUCATION
            var educ = _db.appEducation.Where(e => e.applicantId == id);
            var rEduc = educ.FirstOrDefault();
            if (educ.Count() > 0)
            {
                rEduc.elementary = data.appEducation.elementary?.ToUpper();
                rEduc.elemAddress = data.appEducation.elemAddress?.ToUpper();
                rEduc.elemDegree = data.appEducation.elemDegree?.ToUpper();
                rEduc.elemFrom = data.appEducation.elemFrom;
                rEduc.elemTo = data.appEducation.elemTo;
                rEduc.elemAwards = data.appEducation.elemAwards?.ToUpper();
                rEduc.highSchool = data.appEducation.highSchool?.ToUpper();
                rEduc.highAddress = data.appEducation.highAddress?.ToUpper();
                rEduc.highDegree = data.appEducation.highDegree?.ToUpper();
                rEduc.highFrom = data.appEducation.highFrom;
                rEduc.highTo = data.appEducation.highTo;
                rEduc.highAwards = data.appEducation.highAwards?.ToUpper();
                rEduc.college = data.appEducation.college?.ToUpper();
                rEduc.collAddress = data.appEducation.collAddress?.ToUpper();
                rEduc.collDegree = data.appEducation.collDegree?.ToUpper();
                rEduc.collCourse = data.appEducation.collCourse?.ToUpper();
                rEduc.collFrom = data.appEducation.collFrom;
                rEduc.collTo = data.appEducation.collTo;
                rEduc.collAwards = data.appEducation.collAwards?.ToUpper();
                rEduc.others = data.appEducation.others?.ToUpper();
                rEduc.othAddress = data.appEducation.othAddress?.ToUpper();
                rEduc.othDegree = data.appEducation.othDegree?.ToUpper();
                rEduc.othFrom = data.appEducation.othFrom;
                rEduc.othTo = data.appEducation.othTo;
                rEduc.othAwards = data.appEducation.othAwards?.ToUpper();
                rEduc.updatedAt = DateTime.Now;
                _db.SaveChanges();
            }
            else
            {
                appEducation appEduc = new appEducation();
                appEduc.applicantId = sysRes.id;
                appEduc.elementary = data.appEducation.elementary?.ToUpper();
                appEduc.elemAddress = data.appEducation.elemAddress?.ToUpper();
                appEduc.elemDegree = data.appEducation.elemDegree?.ToUpper();
                appEduc.elemFrom = data.appEducation.elemFrom;
                appEduc.elemTo = data.appEducation.elemTo;
                appEduc.elemAwards = data.appEducation.elemAwards?.ToUpper();
                appEduc.highSchool = data.appEducation.highSchool?.ToUpper();
                appEduc.highAddress = data.appEducation.highAddress?.ToUpper();
                appEduc.highDegree = data.appEducation.highDegree?.ToUpper();
                appEduc.highFrom = data.appEducation.highFrom;
                appEduc.highTo = data.appEducation.highTo;
                appEduc.highAwards = data.appEducation.highAwards?.ToUpper();
                appEduc.college = data.appEducation.college?.ToUpper();
                appEduc.collAddress = data.appEducation.collAddress?.ToUpper();
                appEduc.collDegree = data.appEducation.collDegree?.ToUpper();
                appEduc.collCourse = data.appEducation.collCourse?.ToUpper();
                appEduc.collFrom = data.appEducation.collFrom;
                appEduc.collTo = data.appEducation.collTo;
                appEduc.collAwards = data.appEducation.collAwards?.ToUpper();
                appEduc.others = data.appEducation.others?.ToUpper();
                appEduc.othAddress = data.appEducation.othAddress?.ToUpper();
                appEduc.othDegree = data.appEducation.othDegree?.ToUpper();
                appEduc.othFrom = data.appEducation.othFrom;
                appEduc.othTo = data.appEducation.othTo;
                appEduc.othAwards = data.appEducation.othAwards?.ToUpper();
                appEduc.createdAt = DateTime.Now;
                appEduc.updatedAt = DateTime.Now;
                _db.appEducation.Add(appEduc);
                _db.SaveChanges();
            }

            //update OTHERS
            var oth = _db.appOtherInformation.Where(e => e.applicantId == id);
            var rOth = oth.FirstOrDefault();
            if (oth.Count() > 0)
            {
                rOth.slanguage1 = data.appOtherInformation.slanguage1?.ToUpper();
                rOth.slanguage2 = data.appOtherInformation.slanguage2?.ToUpper();
                rOth.slanguage3 = data.appOtherInformation.slanguage3?.ToUpper();
                rOth.wlanguage1 = data.appOtherInformation.wlanguage1?.ToUpper();
                rOth.wlanguage2 = data.appOtherInformation.wlanguage2?.ToUpper();
                rOth.wlanguage3 = data.appOtherInformation.wlanguage3?.ToUpper();
                rOth.officeMachine = data.appOtherInformation.officeMachine?.ToUpper();
                rOth.hobbies = data.appOtherInformation.hobbies?.ToUpper();
                rOth.vehicles = data.appOtherInformation.vehicles?.ToUpper();
                rOth.workFromHome = data.appOtherInformation.workFromHome;
                _db.SaveChanges();
            }
            else
            {
                appOtherInformation appOth = new appOtherInformation();
                appOth.applicantId = sysRes.id;
                appOth.slanguage1 = data.appOtherInformation.slanguage1?.ToUpper();
                appOth.slanguage2 = data.appOtherInformation.slanguage2?.ToUpper();
                appOth.slanguage3 = data.appOtherInformation.slanguage3?.ToUpper();
                appOth.wlanguage1 = data.appOtherInformation.wlanguage1?.ToUpper();
                appOth.wlanguage2 = data.appOtherInformation.wlanguage2?.ToUpper();
                appOth.wlanguage3 = data.appOtherInformation.wlanguage3?.ToUpper();
                appOth.officeMachine = data.appOtherInformation.officeMachine?.ToUpper();
                appOth.hobbies = data.appOtherInformation.hobbies?.ToUpper();
                appOth.vehicles = data.appOtherInformation.vehicles?.ToUpper();
                appOth.workFromHome = data.appOtherInformation.workFromHome;
                _db.appOtherInformation.Add(appOth);
                _db.SaveChanges();
            }

            //update DECLARATIONS
            var decla = _db.appDeclaration.Where(e => e.applicantId == id);
            var rDecl = decla.FirstOrDefault();
            if (decla.Count() > 0)
            {
                rDecl.kmbiRelative = data.appDeclaration.kmbiRelative;
                rDecl.kmbiRelativePosition = data.appDeclaration.kmbiRelativePosition?.ToUpper();
                rDecl.cCase = data.appDeclaration.cCase;
                rDecl.cCaseDetail = data.appDeclaration.cCaseDetail;
                rDecl.discharged = data.appDeclaration.discharged;
                rDecl.dischargedDetails = data.appDeclaration.dischargedDetails;
                rDecl.illness = data.appDeclaration.illness;
                rDecl.illnessDetails = data.appDeclaration.illnessDetails;
                rDecl.incaseEmergency = data.appDeclaration.incaseEmergency?.ToUpper();
                rDecl.incaseContact = data.appDeclaration.incaseContact?.ToUpper();
                rDecl.incaseRelationship = data.appDeclaration.incaseRelationship?.ToUpper();
                _db.SaveChanges();
            }
            else
            {
                appDeclaration appDecl = new appDeclaration();
                appDecl.applicantId = sysRes.id;
                appDecl.kmbiRelative = data.appDeclaration.kmbiRelative;
                appDecl.kmbiRelativePosition = data.appDeclaration.kmbiRelativePosition?.ToUpper();
                appDecl.cCase = data.appDeclaration.cCase;
                appDecl.cCaseDetail = data.appDeclaration.cCaseDetail;
                appDecl.discharged = data.appDeclaration.discharged;
                appDecl.dischargedDetails = data.appDeclaration.dischargedDetails;
                appDecl.illness = data.appDeclaration.illness;
                appDecl.illnessDetails = data.appDeclaration.illnessDetails;
                appDecl.incaseEmergency = data.appDeclaration.incaseEmergency?.ToUpper();
                appDecl.incaseContact = data.appDeclaration.incaseContact?.ToUpper();
                appDecl.incaseRelationship = data.appDeclaration.incaseRelationship?.ToUpper();
                _db.appDeclaration.Add(appDecl);
                _db.SaveChanges();
            }

            //update REFERENCES
            var refer = _db.appReferences.Where(e => e.applicantId == id);
            var eRef = refer.FirstOrDefault();
            if (refer.Count() > 0)
            {
                eRef.ref1Name = data.appReferences.ref1Name?.ToUpper();
                eRef.ref1Occupation = data.appReferences.ref1Occupation?.ToUpper();
                eRef.ref1Contact = data.appReferences.ref1Contact?.ToUpper();
                eRef.ref2Name = data.appReferences.ref2Name?.ToUpper();
                eRef.ref2Occupation = data.appReferences.ref2Occupation?.ToUpper();
                eRef.ref2Contact = data.appReferences.ref2Contact?.ToUpper();
                eRef.ref3Name = data.appReferences.ref3Name?.ToUpper();
                eRef.ref3Occupation = data.appReferences.ref3Occupation?.ToUpper();
                eRef.ref3Contact = data.appReferences.ref3Contact?.ToUpper();
                _db.SaveChanges();
            }
            else
            {
                appReferences appRef = new appReferences();
                appRef.applicantId = sysRes.id;
                appRef.ref1Name = data.appReferences.ref1Name?.ToUpper();
                appRef.ref1Occupation = data.appReferences.ref1Occupation?.ToUpper();
                appRef.ref1Contact = data.appReferences.ref1Contact?.ToUpper();
                appRef.ref2Name = data.appReferences.ref2Name?.ToUpper();
                appRef.ref2Occupation = data.appReferences.ref2Occupation?.ToUpper();
                appRef.ref2Contact = data.appReferences.ref2Contact?.ToUpper();
                appRef.ref3Name = data.appReferences.ref3Name?.ToUpper();
                appRef.ref3Occupation = data.appReferences.ref3Occupation?.ToUpper();
                appRef.ref3Contact = data.appReferences.ref3Contact?.ToUpper();
                _db.appReferences.Add(appRef);
                _db.SaveChanges();
            }
        }

        public void updateAdminData(adminPersonalViewModel data, int id)
        {
            var sysUsers = _db.systemUsers.Where(e => e.id == id);
            var sysRes = sysUsers.FirstOrDefault();
            if (sysUsers.Count() > 0)
            {
                sysRes.lastName = data.systemUsers.lastName?.ToUpper();
                sysRes.firstName = data.systemUsers.firstName?.ToUpper();
                sysRes.middleName = data.systemUsers.middleName?.ToUpper();
                sysRes.email = data.systemUsers.email;
            }

            var model = _db.appPersonalData.Where(e => e.applicantId == id);
            var res = model.FirstOrDefault();
            if (model.Count() > 0)
            {
                res.picture = data.picture;
                _db.SaveChanges();
            }
            else
            {
                appPersonalData appPPdata = new appPersonalData();
                appPPdata.applicantId = sysRes.id;
                appPPdata.picture = data.picture;
                _db.appPersonalData.Add(appPPdata);
                _db.SaveChanges();
            }
        }

        public void changePassword(string password, int id)
        {
            var sysUsers = _db.systemUsers.Where(e => e.id == id);
            var sysRes = sysUsers.FirstOrDefault();
            if (sysUsers.Count() > 0)
            {
                sysRes.password = password;
                _db.SaveChanges();
            }
        }

        public void resetPassword(string password, string email)
        {
            var sysUsers = _db.systemUsers.Where(e => e.email == email);
            var sysRes = sysUsers.FirstOrDefault();
            if (sysUsers.Count() > 0)
            {
                sysRes.password = password;
                _db.SaveChanges();
            }
        }

        public bool checkPersonalData(int applicantId)
        {
            var res = _db.appPersonalData.Where(e => e.applicantId == applicantId);
            if (res.Count() > 0)
                return true;
            return false;
        }

        public List<applicationViewModel> getSkills(string searchString)
        {
            List<applicationViewModel> viewModel = new List<applicationViewModel>();
            //var res = _db.appOtherInformation.Where(e => e.hobbies.);
            var matches = from m in _db.appOtherInformation
                          where m.hobbies.Contains(searchString)
                          select m;

            systemUsers sysUsers = new systemUsers();
            appPersonalData appData = new appPersonalData();
            string name = "";
            if (matches != null)
            {
                foreach (var item in matches)
                {
                    sysUsers = _db.systemUsers.Where(e => e.id == item.applicantId).FirstOrDefault();
                    appData = _db.appPersonalData.Where(e => e.applicantId == item.applicantId).FirstOrDefault();
                    name = sysUsers.lastName + ", " + sysUsers.firstName + " " + sysUsers.middleName;
                    viewModel.Add(new applicationViewModel
                    {
                        systemUsers = new systemUsers {
                            id = sysUsers.id,
                            lastName = sysUsers.lastName.Trim(),
                            firstName = sysUsers.firstName.Trim(),
                            middleName = sysUsers.middleName.Trim()
                        },
                        appPersonalData = new appPersonalData {
                            picture = appData.picture,
                            resume = appData.resume
                        },
                        appOtherInformation = new appOtherInformation
                        {
                            hobbies = item.hobbies.Trim()
                        }
                    });
                }
            }
            
            
            return viewModel;
        }

        public applicationViewModel getInfoDetailsReport(int applicantId)
        {
            var id = applicantId;
            applicationViewModel viewModel = new applicationViewModel();

            //GET personal data
            var pUsers = _db.systemUsers.Where(e => e.id == id).FirstOrDefault();
            var pPersonal = _db.appPersonalData.Where(e => e.applicantId == id).FirstOrDefault();
            //GET education
            var pEducation = _db.appEducation.Where(e => e.applicantId == id).FirstOrDefault();
            //GET other
            var pOthers = _db.appOtherInformation.Where(e => e.applicantId == id).FirstOrDefault();
            //GET declaration
            var pDeclaration = _db.appDeclaration.Where(e => e.applicantId == id).FirstOrDefault();
            //GET references
            var pReferences = _db.appReferences.Where(e => e.applicantId == id).FirstOrDefault();
            //GET Job family
            List<appFamilyBackground> pFamily = new List<appFamilyBackground>();
            var family = _db.appFamilyBackground.Where(e => e.applicantId == id).ToList();
            if (family.Count() > 0)
            {
                foreach (var item in family)
                {
                    pFamily.Add(new appFamilyBackground
                    {
                        id = item.id,
                        applicantId = item.applicantId,
                        name = item.name,
                        birthDate = item.birthDate,
                        relationship = item.relationship,
                        occupation = item.occupation,
                        school = item.school,
                        course = item.course,
                        contact = item.contact
                    });
                }
                viewModel.appFamilyBackground = pFamily;
            }

            //GET Employment
            List<appEmployment> pEmploy = new List<appEmployment>();
            var employ = _db.appEmployment.Where(e => e.applicantId == id).ToList();
            if (employ.Count() > 0)
            {
                foreach (var item in employ)
                {
                    pEmploy.Add(new appEmployment
                    {
                        id = item.id,
                        applicantId = item.applicantId,
                        employer = item.employer,
                        position = item.position,
                        department = item.department,
                        address = item.address,
                        jobFunctions = item.jobFunctions,
                        isName = item.isName,
                        isPosition = item.isPosition,
                        contact = item.contact,
                        appointmentStatus = item.appointmentStatus,
                        empFrom = item.empFrom,
                        empTo = item.empTo,
                        empPresent = item.empPresent,
                        startSalary = item.startSalary,
                        endSalary = item.endSalary,
                        reasonLeaving = item.reasonLeaving
                    });
                }
                viewModel.appEmployment = pEmploy;
            }

            //GET Training
            List<appTraining> pTraining = new List<appTraining>();
            var training = _db.appTraining.Where(e => e.applicantId == id).ToList();
            if (training.Count() > 0)
            {
                foreach (var item in training)
                {
                    pTraining.Add(new appTraining
                    {
                        id = item.id,
                        applicantId = item.applicantId,
                        title = item.title,
                        dateFrom = item.dateFrom,
                        dateTo = item.dateTo,
                        noHours = item.noHours,
                        venue = item.venue,
                        conductedBy = item.conductedBy
                    });
                }
                viewModel.appTraining = pTraining;
            }

            //GET Training
            List<appMembership> pMem = new List<appMembership>();
            var mem = _db.appMembership.Where(e => e.applicantId == id).ToList();
            if (mem.Count() > 0)
            {
                foreach (var item in mem)
                {
                    pMem.Add(new appMembership
                    {
                        id = item.id,
                        applicantId = item.applicantId,
                        organizationName = item.organizationName,
                        dateFrom = item.dateFrom,
                        dateTo = item.dateTo,
                        memPresent = item.memPresent,
                        position = item.position
                    });
                }
                viewModel.appMembership = pMem;
            }

            //GET awards
            List<appAwards> pAwards = new List<appAwards>();
            var awards = _db.appAwards.Where(e => e.applicantId == id).ToList();
            if (awards.Count() > 0)
            {
                foreach (var item in awards)
                {
                    pAwards.Add(new appAwards
                    {
                        id = item.id,
                        applicantId = item.applicantId,
                        awardName = item.awardName,
                        dateReceived = item.dateReceived,
                        validUntil = item.validUntil,
                        licenseNumber = item.licenseNumber
                    });
                }
                viewModel.appAwards = pAwards;
            }

            //GET Job firstchoice
            jobVacancies pJobFirst = new jobVacancies();
            jobVacancies pJobSecond = new jobVacancies();
            jobVacancies pJobThird = new jobVacancies();
            if (pPersonal != null)
            {
                pJobFirst = _db.jobVacancies.Where(e => e.slug == pPersonal.firstChoice).FirstOrDefault();
                //GET Job secondchoice
                pJobSecond = _db.jobVacancies.Where(e => e.slug == pPersonal.secondChoice).FirstOrDefault();
                //GET Job thirdchoice
                pJobThird = _db.jobVacancies.Where(e => e.slug == pPersonal.thirdChoice).FirstOrDefault();
            }


            viewModel.systemUsers = new systemUsers
            {
                id = pUsers.id,
                slug = pUsers.slug?.Trim(),
                lastName = pUsers.lastName?.Trim(),
                firstName = pUsers.firstName?.Trim(),
                middleName = pUsers.middleName?.Trim(),
                email = pUsers.email?.Trim(),
            };

            if (pPersonal != null)
            {
                viewModel.appPersonalData = new appPersonalData
                {
                    dateFiled = pPersonal.dateFiled,
                    referredBy = pPersonal.referredBy?.Trim(),
                    aboutMe = pPersonal.aboutMe?.Trim(),
                    facebook = pPersonal.facebook?.Trim(),
                    instagram = pPersonal.instagram?.Trim(),
                    twitter = pPersonal.twitter?.Trim(),
                    desiredSalary = pPersonal.desiredSalary,
                    negotiable = pPersonal.negotiable,
                    firstChoice = pPersonal.firstChoice?.Trim(),
                    secondChoice = pPersonal.secondChoice?.Trim(),
                    thirdChoice = pPersonal.thirdChoice?.Trim(),
                    nickName = pPersonal.nickName?.Trim(),
                    sex = pPersonal.sex?.Trim(),
                    bloodType = pPersonal.bloodType?.Trim(),
                    picture = pPersonal.picture?.Trim(),
                    resume = pPersonal.resume?.Trim(),
                    birthPlace = pPersonal.birthPlace?.Trim(),
                    citizenship = pPersonal.citizenship?.Trim(),
                    weight = pPersonal.weight?.Trim(),
                    height = pPersonal.height?.Trim(),
                    civilStatus = pPersonal.civilStatus?.Trim(),
                    religiousAffiliation = pPersonal.religiousAffiliation?.Trim(),
                    sss = pPersonal.sss?.Trim(),
                    tin = pPersonal.tin?.Trim(),
                    taxStatus = pPersonal.taxStatus?.Trim(),
                    pagibig = pPersonal.pagibig?.Trim(),
                    philhealth = pPersonal.philhealth?.Trim(),
                    landline = pPersonal.landline?.Trim(),
                    mobile = pPersonal.mobile?.Trim(),
                    presentAddress = pPersonal.presentAddress?.Trim(),
                    permanentAddress = pPersonal.permanentAddress?.Trim(),
                    permanentAddressContact = pPersonal.permanentAddressContact?.Trim(),
                    permanentAddressZipCode = pPersonal.permanentAddressZipCode?.Trim(),
                    otherAddress = pPersonal.otherAddress?.Trim(),
                    otherAddressContact = pPersonal.otherAddressContact?.Trim(),
                    otherAddressZipCode = pPersonal.otherAddressZipCode?.Trim(),
                    yearsStay = pPersonal.yearsStay,
                    own = pPersonal.own,
                    rent = pPersonal.rent,
                    rentAmount = pPersonal.rentAmount,
                    liveWith = pPersonal.liveWith?.Trim(),
                    location = pPersonal.location?.Trim()
                };
            }

            //EDUCATION
            if (pEducation != null)
            {
                viewModel.appEducation = new appEducation
                {
                    elementary = pEducation.elementary?.Trim(),
                    elemAddress = pEducation.elemAddress?.Trim(),
                    elemDegree = pEducation.elemDegree?.Trim(),
                    elemFrom = pEducation.elemFrom,
                    elemTo = pEducation.elemTo,
                    elemAwards = pEducation.elemAwards?.Trim(),
                    highSchool = pEducation.highSchool?.Trim(),
                    highAddress = pEducation.highAddress?.Trim(),
                    highDegree = pEducation.highDegree?.Trim(),
                    highFrom = pEducation.highFrom,
                    highTo = pEducation.highTo,
                    highAwards = pEducation.highAwards?.Trim(),
                    college = pEducation.college?.Trim(),
                    collAddress = pEducation.collAddress?.Trim(),
                    collDegree = pEducation.collDegree?.Trim(),
                    collCourse = pEducation.collCourse?.Trim(),
                    collFrom = pEducation.collFrom,
                    collTo = pEducation.collTo,
                    collAwards = pEducation.collAwards?.Trim(),
                    others = pEducation.others?.Trim(),
                    othAddress = pEducation.othAddress?.Trim(),
                    othDegree = pEducation.othDegree?.Trim(),
                    othFrom = pEducation.othFrom,
                    othTo = pEducation.othTo,
                    othAwards = pEducation.othAwards?.Trim(),
                };
            }

            //OTHER INFO
            if (pOthers != null)
            {
                viewModel.appOtherInformation = new appOtherInformation
                {
                    slanguage1 = pOthers.slanguage1?.Trim(),
                    slanguage2 = pOthers.slanguage2?.Trim(),
                    slanguage3 = pOthers.slanguage3?.Trim(),
                    wlanguage1 = pOthers.wlanguage1?.Trim(),
                    wlanguage2 = pOthers.wlanguage2?.Trim(),
                    wlanguage3 = pOthers.wlanguage3?.Trim(),
                    officeMachine = pOthers.officeMachine?.Trim(),
                    hobbies = pOthers.hobbies?.Trim(),
                    vehicles = pOthers.vehicles?.Trim(),
                    workFromHome = pOthers.workFromHome
                };
            }

            //DECLARATION
            if (pDeclaration != null)
            {
                viewModel.appDeclaration = new appDeclaration
                {
                    kmbiRelative = pDeclaration.kmbiRelative,
                    kmbiRelativePosition = pDeclaration.kmbiRelativePosition?.Trim(),
                    cCase = pDeclaration.cCase,
                    cCaseDetail = pDeclaration.cCaseDetail?.Trim(),
                    discharged = pDeclaration.discharged,
                    dischargedDetails = pDeclaration.dischargedDetails?.Trim(),
                    illness = pDeclaration.illness,
                    illnessDetails = pDeclaration.illnessDetails?.Trim(),
                    incaseEmergency = pDeclaration.incaseEmergency?.Trim(),
                    incaseContact = pDeclaration.incaseContact?.Trim(),
                    incaseRelationship = pDeclaration.incaseRelationship?.Trim()
                };
            }

            //REFERENCES
            if (pReferences != null)
            {
                viewModel.appReferences = new appReferences
                {
                    ref1Name = pReferences.ref1Name?.Trim(),
                    ref1Occupation = pReferences.ref1Occupation?.Trim(),
                    ref1Contact = pReferences.ref1Contact?.Trim(),
                    ref2Name = pReferences.ref2Name?.Trim(),
                    ref2Occupation = pReferences.ref2Occupation?.Trim(),
                    ref2Contact = pReferences.ref2Contact?.Trim(),
                    ref3Name = pReferences.ref3Name?.Trim(),
                    ref3Occupation = pReferences.ref3Occupation?.Trim(),
                    ref3Contact = pReferences.ref3Contact?.Trim()
                };
            }

            //JOB VACANCIES
            if (pJobFirst != null)
            {
                viewModel.firstJobVacancies = new jobVacancies
                {
                    jobPosition = pJobFirst.jobPosition,
                    jobSummary = pJobFirst.jobSummary,
                    jobQualifications = pJobFirst.jobQualifications,
                    branches = pJobFirst.branches
                };
            }
            if (pJobSecond != null)
            {
                viewModel.secondJobVacancies = new jobVacancies
                {
                    jobPosition = pJobSecond.jobPosition,
                    jobSummary = pJobSecond.jobSummary,
                    jobQualifications = pJobSecond.jobQualifications,
                    branches = pJobSecond.branches
                };
            }
            if (pJobThird != null)
            {
                viewModel.thirdJobVacancies = new jobVacancies
                {
                    jobPosition = pJobThird.jobPosition,
                    jobSummary = pJobThird.jobSummary,
                    jobQualifications = pJobThird.jobQualifications,
                    branches = pJobThird.branches
                };
            }

            //JOB APPLIED


            return viewModel;


            ////FAMILY
            //List<appFamilyBackground> familyData = new List<appFamilyBackground>();
            //if (pFamily != null)
            //{
            //    foreach (var item in pFamily)
            //    {
            //        familyData.Add(new appFamilyBackground
            //        {
            //            name = item.name?.Trim(),
            //            birthDate = item.birthDate,
            //            relationship = item.relationship?.Trim(),
            //            occupation = item.occupation?.Trim(),
            //            school = item.school?.Trim(),
            //            course = item.course?.Trim(),
            //            contact = item.contact?.Trim()
            //        });
            //    }
            //    viewModel.appFamilyBackground = familyData;
            //}

        }

        public string retrieveToken(string email)
        {
            var v = _db.systemUsers.Where(e => e.email == email).FirstOrDefault();
            if (v != null)
                return v.token;
            return "";
        }

        
    }
}
