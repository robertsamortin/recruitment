﻿using Recruitment.Models.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Repo
{
    public class EmploymentRepository : iEmployment
    {
        private readonly SQLCon _db;
        public EmploymentRepository(SQLCon db)
        {
            _db = db;
        }

        public void insertEmployment(appEmployment data)
        {
            data.createdAt = DateTime.Now;
            data.updatedAt = DateTime.Now;
            data.employer = data.employer?.ToUpper();
            data.position = data.position?.ToUpper();
            data.department = data.department?.ToUpper();
            data.address = data.address?.ToUpper();
            data.isName = data.isName?.ToUpper();
            data.isPosition = data.isPosition?.ToUpper();
            data.appointmentStatus = data.appointmentStatus?.ToUpper();
            data.empFrom = data.empFrom;
            data.empTo = data.empTo;
            data.empPresent = data.empPresent;
            data.startSalary = data.startSalary?.ToUpper();
            data.endSalary = data.endSalary;
            _db.appEmployment.Add(data);
            _db.SaveChanges();
        }

        public void updateEmployment(appEmployment data)
        {
            var v = _db.appEmployment.Where(e => e.id == data.id).FirstOrDefault();
            
            if (v != null)
            {
                v.id = data.id;
                v.applicantId = data.applicantId;
                v.employer = data.employer?.ToUpper();
                v.position = data.position?.ToUpper();
                v.department = data.department?.ToUpper();
                v.address = data.address?.ToUpper();
                v.jobFunctions = data.jobFunctions;
                v.isName = data.isName.ToUpper();
                v.isPosition = data.isPosition?.ToUpper();
                v.contact = data.contact;
                v.appointmentStatus = data.appointmentStatus?.ToUpper();
                v.empFrom = data.empFrom;
                v.empTo = data.empTo;
                v.empPresent = data.empPresent;
                v.startSalary = data.startSalary.ToUpper();
                v.endSalary = data.endSalary;
                v.reasonLeaving = data.reasonLeaving?.ToUpper();
                v.createdAt = data.createdAt;
                v.updatedAt = DateTime.Now;
                _db.SaveChanges();
            }
        }

        public void deleteEmployment(int id)
        {
            var v = _db.appEmployment.Where(e => e.id == id).FirstOrDefault();

            if (v != null)
            {
                _db.Remove(v);
                _db.SaveChanges();
            }
        }

        public List<appEmploymentViewModel> getEmployBackground(int id)
        {
            List<appEmploymentViewModel> viewModel = new List<appEmploymentViewModel>();
            var pEduc = _db.appEmployment.Where(e => e.applicantId == id).ToList();
            foreach (var item in pEduc)
            {
                viewModel.Add(new appEmploymentViewModel
                {
                   id = item.id,
                   applicantId = item.applicantId,
                   employer = item.employer?.ToUpper(),
                   position = item.position?.ToUpper(),
                   department = item.department?.ToUpper(),
                   address = item.address?.ToUpper(),
                   jobFunctions = item.jobFunctions,
                   isName = item.isName.ToUpper(),
                   isPosition = item.isPosition?.ToUpper(),
                   contact = item.contact,
                   appointmentStatus = item.appointmentStatus?.ToUpper(),
                   empFrom = item.empFrom.ToString("MM/dd/yyyy"),
                   empTo = item.empTo.ToString("MM/dd/yyyy"),
                    empPresent = item.empPresent,
                   startSalary = item.startSalary?.ToUpper(),
                   endSalary = item.endSalary,
                   reasonLeaving = item.reasonLeaving?.ToUpper(),
                   createdAt = item.createdAt,
                   updatedAt = DateTime.Now
                });
            }
            return viewModel;
        }

        public appEmploymentViewModel getSingleEmployBackground(int id)
        {
            appEmploymentViewModel viewModel = new appEmploymentViewModel();
            var pEmploy = _db.appEmployment.Where(e => e.id == id).FirstOrDefault();
            viewModel.id = pEmploy.id;
            viewModel.applicantId = pEmploy.applicantId;
            viewModel.employer = pEmploy.employer;
            viewModel.position = pEmploy.position;
            viewModel.department = pEmploy.department;
            viewModel.address = pEmploy.address;
            viewModel.jobFunctions = pEmploy.jobFunctions;
            viewModel.isName = pEmploy.isName.ToUpper();
            viewModel.isPosition = pEmploy.isPosition;
            viewModel.contact = pEmploy.contact;
            viewModel.appointmentStatus = pEmploy.appointmentStatus;
            viewModel.empFrom = pEmploy.empFrom.ToString("MM/dd/yyyy");
            viewModel.empTo = pEmploy.empTo.ToString("MM/dd/yyyy");
            viewModel.empPresent = pEmploy.empPresent;
            viewModel.startSalary = pEmploy.startSalary.ToUpper();
            viewModel.endSalary = pEmploy.endSalary;
            viewModel.reasonLeaving = pEmploy.reasonLeaving;
            viewModel.createdAt = pEmploy.createdAt;
            viewModel.updatedAt = DateTime.Now;
            
            return viewModel;
        }
    }
}
