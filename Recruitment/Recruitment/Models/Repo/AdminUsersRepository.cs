﻿using Recruitment.Models.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Repo
{
    public class AdminUsersRepository : iAdminUsers
    {
        private readonly SQLCon _db;
        public AdminUsersRepository(SQLCon db)
        {
            _db = db;
        }

        public void insert(systemUsers data)
        {
            data.createdAt = DateTime.Now;
            data.updatedAt = DateTime.Now;
            data.slug = global.slugify(24);
            data.lastName = data.lastName?.ToUpper();
            data.firstName = data.firstName?.ToUpper();
            data.middleName = data.middleName?.ToUpper();
            data.position = data.position?.ToUpper();
            data.isVerified = true;
            data.isAdmin = true;
            data.referenceNo = global.slugify(12).ToUpper();
            _db.systemUsers.Add(data);
            _db.SaveChanges();
        }

        public void update(systemUsers data)
        {
            var v = _db.systemUsers.Where(e => e.id == data.id).FirstOrDefault();
            
            if (v != null)
            {
                v.id = data.id;
                v.slug = data.slug;
                v.lastName = data.lastName?.ToUpper();
                v.firstName = data.firstName?.ToUpper();
                v.middleName = data.middleName?.ToUpper();
                v.position = data.position;
                v.email = data.email;
                v.password = data.password;
                v.isVerified = true;
                v.isAdmin = data.isAdmin;
                v.branch = data.branch;
                v.contactNo = data.contactNo;
                v.department = data.department;
                v.createdAt = data.createdAt;
                v.updatedAt = DateTime.Now;
                _db.SaveChanges();
            }
        }

        public void delete(int id)
        {
            var v = _db.systemUsers.Where(e => e.id == id).FirstOrDefault();

            if (v != null)
            {
                _db.Remove(v);
                _db.SaveChanges();
            }
        }

        public List<systemUsers> getAll()
        {
            var result = _db.systemUsers.Where(e => e.isAdmin == true).ToList();
            return result;
        }

        public systemUsers getOne(int id)
        {
            var result = _db.systemUsers.Where(e => e.id == id).FirstOrDefault();
            return result;
        }

        public List<string> emailAddress(string branch)
        {
            List<string> viewModel = new List<string>();
            var result = _db.systemUsers.Where(e => e.branch == branch)
                .Where(e => e.position != "HCD DIRECTOR")
                .Where(e => e.branch != "HEAD OFFICE").ToList();
            foreach (var item in result)
            {
                viewModel.Add(item.email);
            }
            return viewModel;
        }

        public string email(string position)
        {
            var result = _db.systemUsers.Where(e => e.position == position).FirstOrDefault().email;
            if (result != null)
            {
                return result;
            }
            else
            {
                return "";
            }
            
        }
    }
}
