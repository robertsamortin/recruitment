﻿using Recruitment.Models.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Repo
{
    public class SkillsRepository : iSkills
    {
        private readonly SQLCon _db;
        public SkillsRepository(SQLCon db)
        {
            _db = db;
        }

        public void insert(skills data)
        {
            data.description = data.description?.ToUpper();
            _db.skills.Add(data);
            _db.SaveChanges();
        }

        public void update(skills data)
        {
            var v = _db.skills.Where(e => e.id == data.id).FirstOrDefault();
            
            if (v != null)
            {
                v.id = data.id;
                v.description = data.description?.ToUpper();
                v.status = data.status;
                _db.SaveChanges();
            }
        }

        public void delete(int id)
        {
            var v = _db.skills.Where(e => e.id == id).FirstOrDefault();

            if (v != null)
            {
                _db.Remove(v);
                _db.SaveChanges();
            }
        }

        public List<skills> getAll()
        {
            var result = _db.skills.OrderBy(e => e.description).ToList();
            return result;
        }

        public skills getOne(int id)
        {
            var result = _db.skills.Where(e => e.id == id).FirstOrDefault();
            return result;
        }
    }
}
