﻿using Recruitment.Models.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Repo
{
    public class Registration : iRegistration
    {
        private readonly SQLCon _db;
        public Registration(SQLCon db)
        {
            _db = db;
        }

        public void add(systemUsers p)
        {
            p.createdAt = DateTime.Now;
            _db.Add(p);
            _db.SaveChanges();
        }

        public bool checkAlreadyRegistered(string email)
        {
            var model = _db.systemUsers.Where(e => e.email == email);
            //model = model.Where(e => e.isVerified == true);
            if (model.Count() > 0)
                return true;
            return false;
        }

        public void confirmEmail(string token, string email)
        {
            var model = _db.systemUsers.Where(e => e.email == email);
            model = model.Where(e => e.token == token);
            var res = model.FirstOrDefault();
            if (model.Count() > 0)
            {
                res.isVerified = true;
                _db.SaveChanges();
            }
        }
    }
}
