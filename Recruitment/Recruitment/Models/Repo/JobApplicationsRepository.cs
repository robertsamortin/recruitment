﻿using Recruitment.Models.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Repo
{
    public class JobApplicationsRepository : iJobApplications
    {
        private readonly SQLCon _db;
        public JobApplicationsRepository(SQLCon db)
        {
            _db = db;
        }

        public void insert(jobApplications data)
        {
            data.createdAt = DateTime.Now;
            data.updatedAt = DateTime.Now;
            data.slug = global.slugify(24);
            data.dateFiled = DateTime.Now;
            _db.jobApplications.Add(data);
            

            _db.SaveChanges();
        }

        public void update(jobApplications data)
        {
            var v = _db.jobApplications.Where(e => e.id == data.id).FirstOrDefault();

            if (v != null)
            {
                v.id = data.id;
                v.slug = data.slug;
                v.applicantId = data.applicantId;
                v.jobSlug = data.jobSlug;
                v.dateFiled = data.dateFiled;
                v.status = data.status;
                v.remarks = data.remarks;
                v.createdAt = data.createdAt;
                v.updatedAt = DateTime.Now;
                _db.SaveChanges();
            }
        }

        public void delete(int id)
        {
            var v = _db.jobApplications.Where(e => e.id == id).FirstOrDefault();

            if (v != null)
            {
                _db.Remove(v);
                _db.SaveChanges();
            }
        }

        public List<jobApplications> getAll()
        {
            var result = _db.jobApplications.OrderByDescending(c => c.createdAt).ToList();
            return result;
        }

        public List<jobApplications> getAllActive()
        {
            var result = _db.jobApplications.Where(e => e.status == "active").OrderByDescending(c => c.createdAt).ToList();
            return result;
        }

        public jobApplications getOne(int id)
        {
            var result = _db.jobApplications.Where(e => e.id == id).FirstOrDefault();
            return result;
        }

        public jobApplications getOneBySlug(string slug)
        {
            var result = _db.jobApplications.Where(e => e.slug == slug).FirstOrDefault();
            return result;
        }

        public jobApplications getOneByJobSlug(string jobSlug, int applicantId)
        {
            var result = _db.jobApplications.Where(e => e.jobSlug == jobSlug).Where(e => e.applicantId == applicantId).Where(e => e.status == "active").FirstOrDefault();
            return result;
        }

        public List<jobApplications> getOneByApplicantId(int applicantId)
        {
            var result = _db.jobApplications.Where(e => e.applicantId == applicantId).ToList();
            return result;
        }

        public List<JobApplicationViewModel> GetAllAdmin(string branch, string department)
        {
            List<JobApplicationViewModel> viewModel = new List<JobApplicationViewModel>();
            List<jobApplications> jobApplications = new List<jobApplications>();
            if (branch.Trim() == "HEAD OFFICE" && department.Trim() == "HUMAN CAPITAL")
            {
                jobApplications = _db.jobApplications.OrderByDescending(c => c.dateFiled).ToList();
            }
            else
            {
                //jobApplications = _db.jobApplications.Where(e => e.department == department).OrderByDescending(c => c.dateFiled).ToList();
                var linq = from s in _db.jobApplications
                           join sa in _db.branch on s.branch equals sa.slug
                           where s.department == department &&
                           sa.name == branch
                           select s;
                jobApplications = linq.ToList();

            }
               
            if (jobApplications.Count() > 0)
            {
                List<string> passed = new List<string>();

                foreach (var item in jobApplications)
                {
                    var jobVac = _db.jobVacancies.Where(c => c.slug == item.jobSlug).FirstOrDefault();
                    var appData = _db.appPersonalData.Where(c => c.applicantId == item.applicantId).FirstOrDefault();
                    var sysUsers = _db.systemUsers.Where(c => c.id == item.applicantId).FirstOrDefault();
                    var stat = _db.statuses.Where(c => c.applicantId == item.applicantId)
                        .Where(c => c.jobSlug == item.jobSlug)
                        .Where(c => c.status == "hired").FirstOrDefault();

                    var currentStatus = "";
                    var jobApp = _db.statuses.Where(c => c.applicantId == item.applicantId)
                        .Where(c => c.currentStatus == true).FirstOrDefault();
                    if (jobApp != null)
                    {
                        currentStatus = jobApp.status;
                    }
                    else
                    {
                        currentStatus = item.status;
                    }
                    

                    var dateHired = "";
                   
                    if (stat != null)
                    {
                        dateHired = stat.inclusiveDate.ToString("MMM dd, yyyy");
                    }


                    //get branch name
                    List<string> slug = new List<string> { "" };
                    List<string> branches = new List<string>();

                    var branchName = "";
                    string Joined = "";
                    if (item.branch != null)
                    {
                        slug = item.branch.Split(',').ToList();
                        foreach (var i in slug)
                        {
                            branchName = _db.branch.Where(e => e.slug == i).FirstOrDefault().name;
                            branches.Add(branchName);
                        }
                        Joined = string.Join(",", branches).Replace(",", "\n");
                    }
                    //


                    passed.Add(item.status);
                    var p = true;
                    if (passed.Contains("failed"))
                    {
                        p = false;
                    }


                    //if (department != "HUMAN CAPITAL" && Joined == branch)
                    //{
                        viewModel.Add(new JobApplicationViewModel
                        {
                            id = item.id,
                            applicantId = item.applicantId,
                            slug = item.slug,
                            picture = appData.picture,
                            resume = appData.resume,
                            lastName = sysUsers.lastName,
                            firstName = sysUsers.firstName,
                            middleName = sysUsers.middleName,
                            jobSlug = item.jobSlug,
                            dateApplied = item.dateFiled.ToString("MMM, dd, yyyy"),
                            dateHired = dateHired,
                            jobPosition = jobVac.jobPosition,
                            jobSummary = jobVac.jobSummary,
                            jobQualifications = jobVac.jobSummary,
                            branches = Joined,
                            department = jobVac.department,
                            status = item.status,
                            remarks = item.remarks,
                            currentStatus = currentStatus,
                            passed = p

                        });
                    //}
                    //else
                    //{

                    //}
                    
                }
            }
            return viewModel;
        }

        public List<JobApplicationViewModel> GetAllApplicant(int id)
        {
            List<JobApplicationViewModel> viewModel = new List<JobApplicationViewModel>();
            var jobApplications = _db.jobApplications.Where(e => e.applicantId == id).OrderByDescending(c => c.dateFiled).ToList();
            if (jobApplications.Count() > 0)
            {
                foreach (var item in jobApplications)
                {
                    var jobVac = _db.jobVacancies.Where(c => c.slug == item.jobSlug).FirstOrDefault();
                    var appData = _db.appPersonalData.Where(c => c.applicantId == item.applicantId).FirstOrDefault();
                    var sysUsers = _db.systemUsers.Where(c => c.id == item.applicantId).FirstOrDefault();

                    //get branch name
                    List<string> slug = new List<string> { "" };
                    List<string> branches = new List<string>();

                    var branchName = "";
                    string Joined = "";
                    if (item.branch != null)
                    {
                        slug = item.branch.Split(',').ToList();
                        foreach (var i in slug)
                        {
                            branchName = _db.branch.Where(e => e.slug == i).FirstOrDefault().name;
                            branches.Add(branchName);
                        }
                        Joined = string.Join(",", branches).Replace(",","\n");
                    }
                    //

                    viewModel.Add(new JobApplicationViewModel
                    {
                        id = item.id,
                        applicantId = item.applicantId,
                        slug = item.slug,
                        picture = appData.picture,
                        resume = appData.resume,
                        lastName = sysUsers.lastName,
                        firstName = sysUsers.firstName,
                        middleName = sysUsers.middleName,
                        jobSlug = item.jobSlug,
                        dateApplied = item.dateFiled.ToString("MMM, dd, yyyy"),
                        jobPosition = jobVac.jobPosition,
                        jobSummary = jobVac.jobSummary,
                        jobQualifications = jobVac.jobSummary,
                        branches = Joined,
                        department = jobVac.department,
                        status = item.status,
                        remarks = item.remarks,
                    });
                }
            }
            return viewModel;
        }

        public JobApplicationViewModel GetOnApplicant(int id)
        {
            JobApplicationViewModel viewModel = new JobApplicationViewModel();
            var jobApplications = _db.jobApplications.Where(e => e.id == id).FirstOrDefault();
            if (jobApplications != null)
            {
                var jobVac = _db.jobVacancies.Where(c => c.slug == jobApplications.jobSlug).FirstOrDefault();
                var appData = _db.appPersonalData.Where(c => c.applicantId == jobApplications.applicantId).FirstOrDefault();
                var sysUsers = _db.systemUsers.Where(c => c.id == jobApplications.applicantId).FirstOrDefault();

                //get branch name
                List<string> slug = new List<string> { "" };
                List<string> branches = new List<string>();

                var branchName = "";
                string Joined = "";
                if (jobApplications.branch != null)
                {
                    slug = jobApplications.branch.Split(',').ToList();
                    foreach (var i in slug)
                    {
                        branchName = _db.branch.Where(e => e.slug == i).FirstOrDefault().name;
                        branches.Add(branchName);
                    }
                    Joined = string.Join(",", branches).Replace(",", "\n");
                }
                //

                viewModel.id = jobApplications.id;
                    viewModel.applicantId = jobApplications.applicantId;
                    viewModel.slug = jobApplications.slug;
                    viewModel.picture = appData.picture;
                    viewModel.resume = appData.resume;
                    viewModel.lastName = sysUsers.lastName;
                    viewModel.firstName = sysUsers.firstName;
                    viewModel.middleName = sysUsers.middleName;
                    viewModel.jobSlug = jobApplications.jobSlug;
                    viewModel.dateApplied = jobApplications.dateFiled.ToString("MMM; dd; yyyy");
                    viewModel.jobPosition = jobVac.jobPosition;
                    viewModel.jobSummary = jobVac.jobSummary;
                    viewModel.jobQualifications = jobVac.jobSummary;
                    viewModel.branches = Joined;
                viewModel.department = jobVac.department;
                    viewModel.status = jobApplications.status;
                    viewModel.remarks = jobApplications.remarks;
            }
            return viewModel;
        }
        
        public void updateStatus(string status, int id)
        {
            var v = _db.jobApplications.Where(e => e.id == id).FirstOrDefault();

            if (v != null)
            {
                v.status = status;
                v.updatedAt = DateTime.Now;
                _db.SaveChanges();
                
            }

            
        }

        public jobApplications getOneByJobSlugBranches(string branchSlug, string jobSlug, int applicantId)
        {
            var result = _db.jobApplications.Where(e => e.jobSlug == jobSlug).Where(e => e.applicantId == applicantId).Where(e => e.status == "active").FirstOrDefault();
            return result;
        }

        public jobApplications getOneByJobSlugBranch(string jobSlug, int applicantId, string branch)
        {
            var result = _db.jobApplications.Where(e => e.jobSlug == jobSlug).Where(e => e.applicantId == applicantId).Where(e => e.status == "active")
                .Where(e => e.branch == branch).FirstOrDefault();
            return result;
        }

        public void updateBranch(jobApplications data)
        {
            var v = _db.jobApplications.Where(e => e.jobSlug == data.jobSlug)
                .Where(e => e.applicantId == data.applicantId).Where(e => e.status == "active").FirstOrDefault();

            if (v != null)
            {
                v.branch = data.branch;
                v.department = data.department;
                v.updatedAt = DateTime.Now;
                _db.SaveChanges();
            }
            else
            {
                data.createdAt = DateTime.Now;
                data.updatedAt = DateTime.Now;
                data.slug = global.slugify(24);
                data.dateFiled = DateTime.Now;
                _db.jobApplications.Add(data);


                ////STATUSES
                //statuses statData = new statuses();
                //statData.applicantId = data.applicantId;
                //statData.status = "active";
                //statData.inclusiveDate = DateTime.Now;
                //statData.currentStatus = true;
                //_db.statuses.Add(statData);

                _db.SaveChanges();
            }
        }

        public statuses GetStatuses(int id)
        {
            List<statuses> viewModel = new List<statuses>();
            var v = _db.jobApplications.Where(e => e.id == id).FirstOrDefault();

            //GET STATUS FROM STATUS
            var status = _db.statuses.Where(e => e.applicantId == v.applicantId)
                .Where(e => e.jobSlug == v.jobSlug).ToList();

            bool currentStatus = false;
            if (status.Count() < 1)
            {
                currentStatus = true;
            }

            viewModel.Add(new statuses
            {
                applicantId = v.applicantId,
                jobSlug = v.jobSlug,
                status = v.status,
                inclusiveDate = v.createdAt,
                currentStatus = currentStatus
            });

            foreach (var item in status)
            {
                viewModel.Add(new statuses
                {
                    applicantId = item.applicantId,
                    jobSlug = item.jobSlug,
                    status = item.status,
                    inclusiveDate = item.inclusiveDate,
                    currentStatus = currentStatus
                });
            }

            return viewModel.Where(e => e.currentStatus == true).FirstOrDefault();
        }

        public void insertStatus(statuses data)
        {
            DateTime incDate;
            if (data.status == "initial examination")
            {
                incDate = DateTime.Now.Date;
            }
            else
            {
                incDate = data.inclusiveDate;
            }
            data.inclusiveDate = incDate;

            updateStatus(data.status, data.jobId);
            data.createdAt = DateTime.Now;
            _db.statuses.Add(data);
            _db.SaveChanges();

            var q = _db.statuses.Where(e => e.jobSlug == data.jobSlug)
                .Where(e => e.applicantId == data.applicantId).ToList();
            if (q != null)
            {
                foreach (var item in q)
                {
                    if (item.status.Trim() == data.status.Trim())
                    {
                        item.currentStatus = true;
                    }
                    else
                    {
                        item.currentStatus = false;
                    }
                    _db.SaveChanges();
                }
            }
        }

        public List<statuses> GetStat(int jobId, int applicantId)
        {
            var res = _db.statuses.Where(e => e.jobId == jobId)
                .Where(e => e.applicantId == applicantId).ToList();
            return res;
        }

        public void updatePassed(int jobId, int applicantId, bool passed, string status)
        {
            var res = _db.statuses.Where(e => e.jobId == jobId)
                .Where(e => e.applicantId == applicantId)
                .Where(e => e.status == status).ToList();

            if (res != null)
            {
                foreach (var item in res)
                {
                    item.passed = passed;
                    _db.SaveChanges();
                }
            }
        }

        public List<StatusesViewModel> GetApplicantStatus(int id)
        {
            List<StatusesViewModel> viewModel = new List<StatusesViewModel>();


            var getActive = GetOnApplicant(id);
            viewModel.Add(new StatusesViewModel
            {
                id = getActive.id,
                applicantId = getActive.applicantId,
                slug = getActive.slug,
                picture = getActive.picture,
                resume = getActive.resume,
                lastName = getActive.lastName,
                firstName = getActive.firstName,
                middleName = getActive.middleName,
                jobSlug = getActive.jobSlug,
                dateApplied = getActive.dateApplied,
                jobPosition = getActive.jobPosition,
                jobSummary = getActive.jobSummary,
                jobQualifications = getActive.jobQualifications,
                status = getActive.status,
                remarks = getActive.remarks,
                branches = getActive.branches,
                department =getActive.department,
                inclusiveDate = getActive.dateApplied,
                passed = false,
                sentInvitation = false,
                apointmentDone = false
            });

            var getStatus = _db.statuses.Where(e => e.jobId == id).ToList();
            foreach (var item in getStatus)
            {
                var getApplicant = GetAllApplicant(item.applicantId);

                foreach (var i in getApplicant)
                {
                    if (i.id == item.jobId)
                    {
                        viewModel.Add(new StatusesViewModel
                        {
                            id = item.jobId,
                            applicantId = i.applicantId,
                            slug = i.slug,
                            picture = i.picture,
                            resume = i.resume,
                            lastName = i.lastName,
                            firstName = i.firstName,
                            middleName = i.middleName,
                            jobSlug = i.jobSlug,
                            dateApplied = i.dateApplied,
                            jobPosition = i.jobPosition,
                            jobSummary = i.jobSummary,
                            jobQualifications = i.jobQualifications,
                            status = item.status,
                            remarks = i.remarks,
                            branches = i.branches,
                            department = i.department,
                            inclusiveDate = item.inclusiveDate.ToString("yyyy-MM-ddThh:mm"),
                            passed = item.passed,
                            sentInvitation =item.sentInvitation,
                            apointmentDone = item.apointmentDone
                        });
                    }
                }
            }

            return viewModel;
        }

        public void insertDocs(documents data)
        {
            var v = _db.documents.Where(e => e.applicantId == data.applicantId).Where(e => e.docNumber == data.docNumber).Where(e => e.filename == data.filename).FirstOrDefault();

            if (v != null)
            {
                _db.Remove(v);
                _db.SaveChanges();
            }

            _db.documents.Add(data);
            _db.SaveChanges();
        }

        public List<documentsViewModel> getAllDocs(int applicantId)
        {
            List<documentsViewModel> viewModel = new List<documentsViewModel>();
            var v = _db.documents.Where(e => e.applicantId == applicantId).ToList();
            foreach (var item in v)
            {
                viewModel.Add(new documentsViewModel
                {
                    id = item.id,
                    applicantId = item.applicantId,
                    docNumber = item.docNumber,
                    filePath  = item.filePath,
                    filename = item.filename,
                    comments = item.comments
                });
            }
            return viewModel;
        }

        public List<documentsViewModel> getAllDocsSameDocNumber(int applicantId, string docNumber)
        {
            List<documentsViewModel> viewModel = new List<documentsViewModel>();
            var v = _db.documents.Where(e => e.applicantId == applicantId).Where(e => e.docNumber == docNumber).ToList();
            foreach (var item in v)
            {
                viewModel.Add(new documentsViewModel
                {
                    id = item.id,
                    applicantId = item.applicantId,
                    docNumber = item.docNumber,
                    filePath = item.filePath,
                    filename = item.filename,
                    comments = item.comments
                });
            }
            return viewModel;
        }

        public statuses GetLastStat(int jobId, int applicantId)
        {
            statuses viewModel = new statuses();
            var res = _db.statuses.Where(e => e.jobId == jobId)
                .Where(e => e.applicantId == applicantId).LastOrDefault();
            if (res != null)
            {
                return res;
            }
            else
            {
                viewModel.status = "active";
                return viewModel;
            }
        }

        public JobApplicationViewModel getOneBySlugApplicant(string jobSlug, int applicantId)
        {
            JobApplicationViewModel viewmodel = new JobApplicationViewModel();
            var result = _db.jobApplications.Where(e => e.jobSlug == jobSlug).Where(e => e.applicantId == applicantId).FirstOrDefault();
            var jobVac = _db.jobVacancies.Where(e => e.slug == jobSlug).FirstOrDefault();
            List<string> slugs = new List<string> { "" };
            List<string> branch = new List<string>();
            var branchName = "";
            string Joined = "";
            if (result != null)
            {
                slugs = result.branch.Split(',').ToList();
                foreach (var i in slugs)
                {
                    branchName = _db.branch.Where(e => e.slug == i).FirstOrDefault().name;
                    branch.Add(branchName);
                    Joined = string.Join(",", branch);
                }
                
                viewmodel.id = result.id;
                viewmodel.slug = result.slug;
                viewmodel.jobPosition = jobVac.jobPosition;
                viewmodel.jobSummary = jobVac.jobSummary;
                viewmodel.jobQualifications = jobVac.jobQualifications;
                viewmodel.branches = Joined;
                viewmodel.department = jobVac.department;
            }
            return viewmodel;
        }

        public void deleteDocs(int applicantId, string docNumber, string filename)
        {
            var v = _db.documents.Where(e => e.applicantId == applicantId).Where(e => e.docNumber == docNumber).Where(e => e.filename == filename).FirstOrDefault();

            if (v != null)
            {
                _db.Remove(v);
                _db.SaveChanges();
            }
        }

        public void updateDocs(int applicantID, string docNumber, string comments)
        {
            var v = _db.documents.Where(e => e.applicantId == applicantID)
                .Where(e => e.docNumber.Trim() == docNumber).ToList();

            if (v != null)
            {
                foreach (var item in v)
                {
                    item.comments = comments;
                    _db.SaveChanges();
                }
            }
        }
    }
}
