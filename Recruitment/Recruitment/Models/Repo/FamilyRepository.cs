﻿using Recruitment.Models.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Repo
{
    public class FamilyRepository : iFamily
    {
        private readonly SQLCon _db;
        public FamilyRepository(SQLCon db)
        {
            _db = db;
        }

        public void insertFamily(appFamilyBackground data)
        {
            data.createdAt = DateTime.Now;
            data.updatedAt = DateTime.Now;
            data.name = data.name?.ToUpper();
            data.relationship = data.relationship?.ToUpper();
            data.occupation = data.occupation?.ToUpper();
            data.school = data.school?.ToUpper();
            data.course = data.course?.ToUpper();
            _db.appFamilyBackground.Add(data);
            _db.SaveChanges();
        }

        public void updateFamily(appFamilyBackground data)
        {
            var v = _db.appFamilyBackground.Where(e => e.id == data.id).FirstOrDefault();
            
            if (v != null)
            {
                v.id = data.id;
                v.applicantId = data.applicantId;
                v.name = data.name?.ToUpper();
                v.birthDate = data.birthDate;
                v.relationship = data.relationship?.ToUpper();
                v.occupation = data.occupation?.ToUpper();
                v.school = data.school?.ToUpper();
                v.course = data.course?.ToUpper();
                v.contact = data.contact;
                v.createdAt = data.createdAt;
                v.updatedAt = DateTime.Now;
                _db.SaveChanges();
            }
        }

        public void deleteFamily(int id)
        {
            var v = _db.appFamilyBackground.Where(e => e.id == id).FirstOrDefault();

            if (v != null)
            {
                _db.Remove(v);
                _db.SaveChanges();
            }
        }

        public List<appFamilyViewModel> getFamBackground(int id)
        {
            List<appFamilyViewModel> viewModel = new List<appFamilyViewModel>();
            var pFamily = _db.appFamilyBackground.Where(e => e.applicantId == id).ToList();
            foreach (var item in pFamily)
            {
                viewModel.Add(new appFamilyViewModel
                {
                    id = item.id,
                    applicantId = item.applicantId,
                    name = item.name,
                    birthDate = item.birthDate.ToString("MM/dd/yyyy"),
                    age = global.CalculateAge(item.birthDate),
                    relationship = item.relationship,
                    occupation = item.occupation,
                    school = item.school,
                    course = item.course,
                    contact = item.contact,
                    createdAt = item.createdAt,
                    updatedAt = item.updatedAt
                });
            }
            return viewModel;
        }

        public appFamilyViewModel getSingleFamBackground(int id)
        {
            appFamilyViewModel viewModel = new appFamilyViewModel();
            var pFamily = _db.appFamilyBackground.Where(e => e.id == id).FirstOrDefault();
            viewModel.id = pFamily.id;
            viewModel.applicantId = pFamily.applicantId;
            viewModel.name = pFamily.name;
            viewModel.birthDate = pFamily.birthDate.ToString("MM/dd/yyyy");
            viewModel.age = global.CalculateAge(pFamily.birthDate);
            viewModel.relationship = pFamily.relationship;
            viewModel.occupation = pFamily.occupation;
            viewModel.school = pFamily.school;
            viewModel.course = pFamily.course;
            viewModel.contact = pFamily.contact;
            viewModel.createdAt = pFamily.createdAt;
            viewModel.updatedAt = pFamily.updatedAt;
            return viewModel;
        }
    }
}
