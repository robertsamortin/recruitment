﻿using Recruitment.Models.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Repo
{
    public class BranchRepository : iBranch
    {
        private readonly SQLCon _db;
        public BranchRepository(SQLCon db)
        {
            _db = db;
        }

        public void insert(branch data)
        {
            data.slug = global.slugify(16);
            data.name = data.name?.ToUpper();
            data.code = data.code?.ToUpper();
            _db.branch.Add(data);
            _db.SaveChanges();
        }

        public void update(branch data)
        {
            var v = _db.branch.Where(e => e.id == data.id).FirstOrDefault();
            
            if (v != null)
            {
                v.id = data.id;
                v.slug = data.slug;
                v.name = data.name?.ToUpper();
                v.code = data.code;
                _db.SaveChanges();
            }
        }

        public void delete(int id)
        {
            var v = _db.branch.Where(e => e.id == id).FirstOrDefault();

            if (v != null)
            {
                _db.Remove(v);
                _db.SaveChanges();
            }
        }

        public List<branch> getAll()
        {
            var result = _db.branch.OrderBy(e => e.name).ToList();
            return result;
        }

        public branch getOne(int id)
        {
            var result = _db.branch.Where(e => e.id == id).FirstOrDefault();
            return result;
        }
    }
}
