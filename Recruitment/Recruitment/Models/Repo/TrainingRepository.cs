﻿using Recruitment.Models.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Repo
{
    public class TrainingRepository : iTraining
    {
        private readonly SQLCon _db;
        public TrainingRepository(SQLCon db)
        {
            _db = db;
        }

        public void insertTraining(appTraining data)
        {
            data.createdAt = DateTime.Now;
            data.updatedAt = DateTime.Now;
            data.title = data.title?.ToUpper();
            data.venue = data.venue?.ToUpper();
            data.dateFrom = data.dateFrom;
            data.dateTo = data.dateTo;
            data.noHours = data.noHours;
            data.conductedBy = data.conductedBy?.ToUpper();
            _db.appTraining.Add(data);
            _db.SaveChanges();
        }

        public void updateTraining(appTraining data)
        {
            var v = _db.appTraining.Where(e => e.id == data.id).FirstOrDefault();
            
            if (v != null)
            {
                v.id = data.id;
                v.applicantId = data.applicantId;
                v.title = data.title?.ToUpper();
                v.venue = data.venue?.ToUpper();
                v.conductedBy = data.conductedBy?.ToUpper();
                v.dateFrom = data.dateFrom;
                v.dateTo = data.dateTo;
                v.noHours = data.noHours;
                v.createdAt = data.createdAt;
                v.updatedAt = DateTime.Now;
                _db.SaveChanges();
            }
        }

        public void deleteTraining(int id)
        {
            var v = _db.appTraining.Where(e => e.id == id).FirstOrDefault();

            if (v != null)
            {
                _db.Remove(v);
                _db.SaveChanges();
            }
        }

        public List<appTrainingViewModel> getTraining(int id)
        {
            List<appTrainingViewModel> viewModel = new List<appTrainingViewModel>();
            var pEduc = _db.appTraining.Where(e => e.applicantId == id).ToList();
            foreach (var item in pEduc)
            {
                viewModel.Add(new appTrainingViewModel
                {
                    id = item.id,
                    applicantId = item.applicantId,
                    title = item.title?.ToUpper(),
                    dateFrom = item.dateFrom.ToString("MM/dd/yyyy"),
                    dateTo = item.dateTo.ToString("MM/dd/yyyy"),
                    venue = item.venue?.ToUpper(),
                    noHours = item.noHours,
                    conductedBy = item.conductedBy,
                    createdAt = item.createdAt,
                    updatedAt = DateTime.Now
                });
            }
            return viewModel;
        }

        public appTrainingViewModel getSingleTraining(int id)
        {
            appTrainingViewModel viewModel = new appTrainingViewModel();
            var pTrain = _db.appTraining.Where(e => e.id == id).FirstOrDefault();
            viewModel.id = pTrain.id;
            viewModel.applicantId = pTrain.applicantId;
            viewModel.title = pTrain.title;
            viewModel.dateFrom = pTrain.dateFrom.ToString("MM/dd/yyyy");
            viewModel.dateTo = pTrain.dateTo.ToString("MM/dd/yyyy");
            viewModel.venue = pTrain.venue;
            viewModel.noHours = pTrain.noHours;
            viewModel.conductedBy = pTrain.conductedBy;
            viewModel.createdAt = pTrain.createdAt;
            viewModel.updatedAt = DateTime.Now;
            
            return viewModel;
        }
    }
}
