﻿using Recruitment.Models.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Repo
{
    public class MembershipRepository : iMembership
    {
        private readonly SQLCon _db;
        public MembershipRepository(SQLCon db)
        {
            _db = db;
        }

        public void insertMembership(appMembership data)
        {
            data.createdAt = DateTime.Now;
            data.updatedAt = DateTime.Now;
            data.organizationName = data.organizationName?.ToUpper();
            data.position = data.position?.ToUpper();
            data.memPresent = data.memPresent;
            _db.appMembership.Add(data);
            _db.SaveChanges();
        }

        public void updateMembership(appMembership data)
        {
            var v = _db.appMembership.Where(e => e.id == data.id).FirstOrDefault();
            
            if (v != null)
            {
                v.id = data.id;
                v.applicantId = data.applicantId;
                v.organizationName = data.organizationName?.ToUpper();
                v.position = data.position?.ToUpper();
                v.dateFrom = data.dateFrom;
                v.dateTo = data.dateTo;
                v.memPresent = data.memPresent;
                v.createdAt = data.createdAt;
                v.updatedAt = DateTime.Now;
                _db.SaveChanges();
            }
        }

        public void deleteMembership(int id)
        {
            var v = _db.appMembership.Where(e => e.id == id).FirstOrDefault();

            if (v != null)
            {
                _db.Remove(v);
                _db.SaveChanges();
            }
        }

        public List<appMembershipViewModel> getMembership(int id)
        {
            List<appMembershipViewModel> viewModel = new List<appMembershipViewModel>();
            var pEduc = _db.appMembership.Where(e => e.applicantId == id).ToList();
            foreach (var item in pEduc)
            {
                viewModel.Add(new appMembershipViewModel
                {
                    id = item.id,
                    applicantId = item.applicantId,
                    organizationName = item.organizationName?.ToUpper(),
                    dateFrom = item.dateFrom.ToString("MM/dd/yyyy"),
                    dateTo = item.dateTo.ToString("MM/dd/yyyy"),
                    position = item.position?.ToUpper(),
                    memPresent = item.memPresent,
                    createdAt = item.createdAt,
                    updatedAt = DateTime.Now
                });
            }
            return viewModel;
        }

        public appMembershipViewModel getSingleMembership(int id)
        {
            appMembershipViewModel viewModel = new appMembershipViewModel();
            var pTrain = _db.appMembership.Where(e => e.id == id).FirstOrDefault();
            viewModel.id = pTrain.id;
            viewModel.applicantId = pTrain.applicantId;
            viewModel.organizationName = pTrain.organizationName;
            viewModel.dateFrom = pTrain.dateFrom.ToString("MM/dd/yyyy");
            viewModel.dateTo = pTrain.dateTo.ToString("MM/dd/yyyy");
            viewModel.position = pTrain.position;
            viewModel.memPresent = pTrain.memPresent;
            viewModel.createdAt = pTrain.createdAt;
            viewModel.updatedAt = DateTime.Now;
            
            return viewModel;
        }
    }
}
