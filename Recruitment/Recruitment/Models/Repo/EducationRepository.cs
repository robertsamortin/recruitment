﻿using Recruitment.Models.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Repo
{
    public class EducationRepository : iEducation
    {
        private readonly SQLCon _db;
        public EducationRepository(SQLCon db)
        {
            _db = db;
        }

        public void insertEducation(appEducation data)
        {
            data.createdAt = DateTime.Now;
            data.updatedAt = DateTime.Now;
            data.elementary = data.elementary?.ToUpper();
            data.elemAddress = data.elemAddress?.ToUpper();
            data.elemDegree = data.elemDegree?.ToUpper();
            data.elemAwards = data.elemAwards?.ToUpper();
            data.highSchool = data.highSchool?.ToUpper();
            data.highAddress = data.highAddress?.ToUpper();
            data.highDegree = data.highDegree?.ToUpper();
            data.highAwards = data.highAwards?.ToUpper();
            data.college = data.college?.ToUpper();
            data.collAddress = data.collAddress?.ToUpper();
            data.collDegree = data.collDegree?.ToUpper();
            data.collAwards = data.collAwards?.ToUpper();
            data.others = data.others?.ToUpper();
            data.othAddress = data.othAddress?.ToUpper();
            data.othDegree = data.othDegree?.ToUpper();
            data.othAwards = data.othAwards?.ToUpper();
            _db.appEducation.Add(data);
            _db.SaveChanges();
        }

        public void updateEducation(appEducation data)
        {
            var v = _db.appEducation.Where(e => e.id == data.id).FirstOrDefault();
            
            if (v != null)
            {
                v.id = data.id;
                v.applicantId = data.applicantId;
                v.elementary = data.elementary?.ToUpper();
                v.elemAddress = data.elemAddress?.ToUpper();
                v.elemDegree = data.elemDegree?.ToUpper();
                v.elemAwards = data.elemAwards?.ToUpper();
                v.elemFrom = data.elemFrom;
                v.elemTo = data.elemTo;
                v.highSchool = data.highSchool?.ToUpper();
                v.highAddress = data.highAddress?.ToUpper();
                v.highDegree = data.highDegree?.ToUpper();
                v.highAwards = data.highAwards?.ToUpper();
                v.highFrom = data.highFrom;
                v.highTo = data.highTo;
                v.college = data.college?.ToUpper();
                v.collAddress = data.collAddress?.ToUpper();
                v.collDegree = data.collDegree?.ToUpper();
                v.collAwards = data.collAwards?.ToUpper();
                v.collFrom = data.collFrom;
                v.collTo = data.collTo;
                v.others = data.others?.ToUpper();
                v.othAddress = data.othAddress?.ToUpper();
                v.othDegree = data.othDegree?.ToUpper();
                v.othAwards = data.othAwards?.ToUpper();
                v.othFrom = data.othFrom;
                v.othTo = data.othTo;
                v.createdAt = data.createdAt;
                v.updatedAt = DateTime.Now;
                _db.SaveChanges();
            }
        }

        public void deleteEducation(int id)
        {
            var v = _db.appEducation.Where(e => e.id == id).FirstOrDefault();

            if (v != null)
            {
                _db.Remove(v);
                _db.SaveChanges();
            }
        }

        public List<appEducationViewModel> getEducBackground(int id)
        {
            List<appEducationViewModel> viewModel = new List<appEducationViewModel>();
            var pEduc = _db.appEducation.Where(e => e.applicantId == id).ToList();
            foreach (var item in pEduc)
            {
                viewModel.Add(new appEducationViewModel
                {
                    elementary = item.elementary,
                    elemAddress =item.elemAddress,
                    elemDegree = item.elemDegree,
                    elemAwards = item.elemAwards,
                    elemFrom = item.elemFrom.ToString("MM/dd/yyyy"),
                    elemTo = item.elemTo.ToString("MM/dd/yyyy"),
                    highSchool = item.highSchool,
                    highAddress = item.highAddress,
                    highDegree = item.highDegree,
                    highAwards = item.highAwards,
                    highFrom = item.highFrom.ToString("MM/dd/yyyy"),
                    highTo = item.highTo.ToString("MM/dd/yyyy"),
                    college = item.college,
                    collAddress = item.collAddress,
                    collDegree = item.collDegree,
                    collAwards = item.collAwards,
                    collFrom = item.collFrom.ToString("MM/dd/yyyy"),
                    collTo = item.collTo.ToString("MM/dd/yyyy"),
                    others = item.others,
                    othAddress = item.othAddress,
                    othDegree = item.othDegree,
                    othAwards = item.othAwards,
                    othFrom = item.othFrom.ToString("MM/dd/yyyy"),
                    othTo = item.othTo.ToString("MM/dd/yyyy"),
                    createdAt = item.createdAt,
                    updatedAt = item.updatedAt
                });
            }
            return viewModel;
        }

        public appEducationViewModel getSingleEducBackground(int id)
        {
            appEducationViewModel viewModel = new appEducationViewModel();
            var pEduc = _db.appEducation.Where(e => e.id == id).FirstOrDefault();
            viewModel.elementary = pEduc.elementary;
            viewModel.elemAddress = pEduc.elemAddress;
            viewModel.elemDegree = pEduc.elemDegree;
            viewModel.elemAwards = pEduc.elemAwards;
            viewModel.elemFrom = pEduc.elemFrom.ToString("MM/dd/yyyy");
            viewModel.elemTo = pEduc.elemTo.ToString("MM/dd/yyyy");
            viewModel.highSchool = pEduc.highSchool;
            viewModel.highAddress = pEduc.highAddress;
            viewModel.highDegree = pEduc.highDegree;
            viewModel.highAwards = pEduc.highAwards;
            viewModel.highFrom = pEduc.highFrom.ToString("MM/dd/yyyy");
            viewModel.highTo = pEduc.highTo.ToString("MM/dd/yyyy");
            viewModel.college = pEduc.college;
            viewModel.collAddress = pEduc.collAddress;
            viewModel.collDegree = pEduc.collDegree;
            viewModel.collAwards = pEduc.collAwards;
            viewModel.collFrom = pEduc.collFrom.ToString("MM/dd/yyyy");
            viewModel.collTo = pEduc.collTo.ToString("MM/dd/yyyy");
            viewModel.others = pEduc.others;
            viewModel.othAddress = pEduc.othAddress;
            viewModel.othDegree = pEduc.othDegree;
            viewModel.othAwards = pEduc.othAwards;
            viewModel.othFrom = pEduc.othFrom.ToString("MM/dd/yyyy");
            viewModel.othTo = pEduc.othTo.ToString("MM/dd/yyyy");
            viewModel.createdAt = pEduc.createdAt;
            viewModel.updatedAt = pEduc.updatedAt;
            return viewModel;
        }
    }
}
