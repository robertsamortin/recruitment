﻿using Recruitment.Models.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Repo
{
    public class JobVancaciesRepository : iJobVacancies
    {
        private readonly SQLCon _db;
        public JobVancaciesRepository(SQLCon db)
        {
            _db = db;
        }

        public void insert(jobVacancies data)
        {
            data.createdAt = DateTime.Now;
            data.updatedAt = DateTime.Now;
            data.slug = global.slugify(24);
            data.jobPosition = data.jobPosition?.ToUpper();
            _db.jobVacancies.Add(data);
            _db.SaveChanges();
        }

        public void update(jobVacancies data)
        {
            var v = _db.jobVacancies.Where(e => e.id == data.id).FirstOrDefault();
            
            if (v != null)
            {
                v.id = data.id;
                v.slug = data.slug;
                v.jobPosition = data.jobPosition?.ToUpper();
                v.jobSummary = data.jobSummary;
                v.jobQualifications = data.jobQualifications;
                v.jobActive = data.jobActive;
                v.branches = data.branches;
                v.department = data.department;
                v.createdAt = data.createdAt;
                v.updatedAt = DateTime.Now;
                _db.SaveChanges();
            }
        }

        public void delete(int id)
        {
            var v = _db.jobVacancies.Where(e => e.id == id).FirstOrDefault();

            if (v != null)
            {
                _db.Remove(v);
                _db.SaveChanges();
            }
        }

        public List<jobVacancies> getAll()
        {
            var result = _db.jobVacancies.OrderByDescending(c => c.createdAt).ToList();
            return result;
        }

        public List<jobVacancies> getAllActive()
        {
            var result = _db.jobVacancies.Where(e => e.jobActive == true).OrderByDescending(c => c.createdAt).ToList();
            return result;
        }

        public jobVacancies getOne(int id)
        {
            var result = _db.jobVacancies.Where(e => e.id == id).FirstOrDefault();
            return result;
        }

        public jobVacancies getOneBySlug(string slug)
        {
            var result = _db.jobVacancies.Where(e => e.slug == slug).FirstOrDefault();
            return result;
        }

        public List<JobVacanciesViewModel> getAllViewModel()
        {
            List<JobVacanciesViewModel> viewmodel = new List<JobVacanciesViewModel>();
            var result = _db.jobVacancies.ToList();
            foreach (var item in result)
            {
                List<string> slug = new List<string> {""};
                List<string> branches = new List<string>();

                var branchName = "";
                string Joined = "";
                if (item.branches != null)
                {
                    slug = item.branches.Split(',').ToList();
                    foreach (var i in slug)
                    {
                        branchName = _db.branch.Where(e => e.slug == i).FirstOrDefault().name;
                        branches.Add(branchName);
                    }
                    Joined = string.Join(",", branches); 
                }
                
                
                viewmodel.Add(new JobVacanciesViewModel
                {
                    id = item.id,
                    slug = item.slug,
                    jobPosition = item.jobPosition,
                    jobSummary = item.jobSummary,
                    jobQualifications = item.jobQualifications,
                    jobActive = item.jobActive,
                    branchSlugs = item.branches,
                    branches = Joined,
                    department = item.department,
                    createdAt = item.createdAt,
                    updatedAt = item.updatedAt
                });
            }
            return viewmodel;
        }

        public List<JobVacanciesViewModelApplicant> getAllActiveViewModel()
        {
            List<JobVacanciesViewModelApplicant> viewmodel = new List<JobVacanciesViewModelApplicant>();
            var result = _db.jobVacancies.Where(e => e.jobActive == true).OrderByDescending(c => c.createdAt).ToList();
            foreach (var item in result)
            {
                var resJobApp = _db.jobApplications.Where(e => e.jobSlug == item.slug).FirstOrDefault();
                List<string> JobAppSlugs = new List<string> { "" };
                if (resJobApp != null)
                {
                    JobAppSlugs = resJobApp.branch.Split(',').ToList();
                }

                List<string> slug = new List<string> { "" };
                //List<string> branches = new List<string>();
                List<branch> branch = new List<branch>();
                var branchName = "";
                string Joined = "";
                if (item.branches != null)
                {
                    slug = item.branches.Split(',').ToList();
                    foreach (var i in slug)
                    {
                        branchName = _db.branch.Where(e => e.slug == i).FirstOrDefault().name;
                        branch.Add(new branch {
                            id = 0,
                            slug = i,
                            name = branchName,
                            code = ""
                        });
                    }
                    Joined = string.Join(",", branch);
                }

                List<branch> branches = new List<branch>();
                viewmodel.Add(new JobVacanciesViewModelApplicant
                {
                    id = item.id,
                    slug = item.slug,
                    jobPosition = item.jobPosition,
                    jobSummary = item.jobSummary,
                    jobQualifications = item.jobQualifications,
                    jobActive = item.jobActive,
                    branches = branch,
                    department = item.department,
                    applicantSlug = JobAppSlugs,
                    createdAt = item.createdAt,
                    updatedAt = item.updatedAt
                });
            }
            return viewmodel;
        }

        public JobVacanciesViewModelApplicant getOneBySlugApplicant(string slug)
        {
            JobVacanciesViewModelApplicant viewmodel = new JobVacanciesViewModelApplicant();
            var result = _db.jobVacancies.Where(e => e.slug == slug).FirstOrDefault();
            var resJobApp = _db.jobApplications.Where(e => e.jobSlug == result.slug).FirstOrDefault();
            List<string> JobAppSlugs = new List<string> {""};
            if (resJobApp != null)
            {
                if (resJobApp.branch != null)
                {
                    JobAppSlugs = resJobApp.branch.Split(',').ToList();
                }
            }

            List<string> slugs = new List<string> { "" };
            List<branch> branch = new List<branch>();
            var branchName = "";
            if (result != null)
            {
                slugs = result.branches.Split(',').ToList();
                foreach (var i in slugs)
                {
                    branchName = _db.branch.Where(e => e.slug == i).FirstOrDefault().name;
                    branch.Add(new branch
                    {
                        id = 0,
                        slug = i,
                        name = branchName,
                        code = ""
                    });
                }
                viewmodel.id = result.id;
                viewmodel.slug = result.slug;
                viewmodel.jobPosition = result.jobPosition;
                viewmodel.jobSummary = result.jobSummary;
                viewmodel.jobQualifications = result.jobQualifications;
                viewmodel.jobActive = result.jobActive;
                viewmodel.branches = branch;
                viewmodel.department = result.department;
                viewmodel.applicantSlug = JobAppSlugs;
                viewmodel.createdAt = result.createdAt;
                viewmodel.updatedAt = result.updatedAt;
            }
            return viewmodel;
        }
    }
}
