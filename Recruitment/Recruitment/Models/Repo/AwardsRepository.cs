﻿using Recruitment.Models.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.Repo
{
    public class AwardsRepository : iAwards
    {
        private readonly SQLCon _db;
        public AwardsRepository(SQLCon db)
        {
            _db = db;
        }

        public void insertAwards(appAwards data)
        {
            data.createdAt = DateTime.Now;
            data.updatedAt = DateTime.Now;
            data.awardName = data.awardName?.ToUpper();
            data.licenseNumber = data.licenseNumber?.ToUpper();
            _db.appAwards.Add(data);
            _db.SaveChanges();
        }

        public void updateAwards(appAwards data)
        {
            var v = _db.appAwards.Where(e => e.id == data.id).FirstOrDefault();
            
            if (v != null)
            {
                v.id = data.id;
                v.applicantId = data.applicantId;
                v.awardName = data.awardName?.ToUpper();
                v.licenseNumber = data.licenseNumber?.ToUpper();
                v.dateReceived = data.dateReceived;
                v.validUntil = data.validUntil;
                v.createdAt = data.createdAt;
                v.updatedAt = DateTime.Now;
                _db.SaveChanges();
            }
        }

        public void deleteAwards(int id)
        {
            var v = _db.appAwards.Where(e => e.id == id).FirstOrDefault();

            if (v != null)
            {
                _db.Remove(v);
                _db.SaveChanges();
            }
        }

        public List<appAwardsViewModel> getAwards(int id)
        {
            List<appAwardsViewModel> viewModel = new List<appAwardsViewModel>();
            var pEduc = _db.appAwards.Where(e => e.applicantId == id).ToList();
            foreach (var item in pEduc)
            {
                viewModel.Add(new appAwardsViewModel
                {
                    id = item.id,
                    applicantId = item.applicantId,
                    awardName = item.awardName?.ToUpper(),
                    dateReceived = item.dateReceived.ToString("MM/dd/yyyy"),
                    validUntil = item.validUntil.ToString("MM/dd/yyyy"),
                    licenseNumber = item.licenseNumber?.ToUpper(),
                    createdAt = item.createdAt,
                    updatedAt = DateTime.Now
                });
            }
            return viewModel;
        }

        public appAwardsViewModel getSingleAwards(int id)
        {
            appAwardsViewModel viewModel = new appAwardsViewModel();
            var pTrain = _db.appAwards.Where(e => e.id == id).FirstOrDefault();
            viewModel.id = pTrain.id;
            viewModel.applicantId = pTrain.applicantId;
            viewModel.awardName = pTrain.awardName;
            viewModel.dateReceived = pTrain.dateReceived.ToString("MM/dd/yyyy");
            viewModel.validUntil = pTrain.validUntil.ToString("MM/dd/yyyy");
            viewModel.licenseNumber = pTrain.licenseNumber;
            viewModel.createdAt = pTrain.createdAt;
            viewModel.updatedAt = DateTime.Now;
            
            return viewModel;
        }
    }
}
