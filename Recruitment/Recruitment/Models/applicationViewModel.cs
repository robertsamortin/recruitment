﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models
{
    public class applicationViewModel
    {
        public systemUsers systemUsers { get; set; }
        public appPersonalData appPersonalData { get; set; }
        public List<appFamilyBackground> appFamilyBackground { get; set; }
        public appEducation appEducation { get; set; }
        public List<appEmployment> appEmployment { get; set; }
        public List<appTraining> appTraining { get; set; }
        public List<appMembership> appMembership { get; set; }
        public List<appAwards> appAwards { get; set; }
        public appOtherInformation appOtherInformation { get; set; }
        public appDeclaration appDeclaration { get; set; }
        public appReferences appReferences { get; set; }
        public jobVacancies firstJobVacancies { get; set; }
        public jobVacancies secondJobVacancies { get; set; }
        public jobVacancies thirdJobVacancies { get; set; }
        public string firstBranchSlug { get; set; }
        public string secondBranchSlug { get; set; }
        public string thirdBranchSlug { get; set; }
        //public List<JobApplicationViewModel> jobApplications { get; set; }
        public IFormFile pictureFile { get; set; }
        public IFormFile resumeFile { get; set; }
    }
}
