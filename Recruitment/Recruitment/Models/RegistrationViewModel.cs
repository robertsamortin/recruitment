﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace Recruitment.Models
{
    public class RegistrationViewModel
    {
        [Key]
        public int id { get; set; }

        [Required(ErrorMessage = "First Name must not be empty")]
        public string firstName { get; set; }

        [Required(ErrorMessage = "Middle Name must not be empty")]
        public string middleName { get; set; }

        [Required(ErrorMessage = "Last Name must not be empty")]
        public string lastName { get; set; }

        [Required(ErrorMessage = "Email must not be empty")]
        [Remote("checkAlreadyRegistered", "Account", ErrorMessage = "Email already exist")]
        public string email { get; set; }

        [Required(ErrorMessage = "Password must not be empty")]
        public string password { get; set; }

        [Compare("password", ErrorMessage = "Password mismatch")]
        public string confirmPassword { get; set; }
    }
}
