﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models
{
    public class documents
    {
        [Key]
        public int id { get; set; }
        public int applicantId { get; set; }
        public string docNumber { get; set; }
        public string filePath { get; set; }
        public string filename { get; set; }
        public string comments { get; set; }
    }
}
