﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models
{
    public class JobVacanciesViewModel
    {
        public int id { get; set; }
        public string slug { get; set; }
        public string jobPosition { get; set; }
        public string jobSummary { get; set; }
        public string jobQualifications { get; set; }
        public bool jobActive { get; set; }
        public string branchSlugs { get; set; }
        public string branches { get; set; }
        public string department { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }

    public class JobVacanciesViewModelApplicant
    {
        public int id { get; set; }
        public string slug { get; set; }
        public string jobPosition { get; set; }
        public string jobSummary { get; set; }
        public string jobQualifications { get; set; }
        public bool jobActive { get; set; }
        public List<branch> branches { get; set; }
        public List<string> applicantSlug { get; set; }
        public string department { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }
}
