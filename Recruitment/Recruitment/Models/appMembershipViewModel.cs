﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models
{
    public class appMembershipViewModel
    {
        public int id { get; set; }
        public int applicantId { get; set; }
        public string organizationName { get; set; }
        public string dateFrom { get; set; }
        public string dateTo { get; set; }
        public bool memPresent { get; set; }
        public string position { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }
}
