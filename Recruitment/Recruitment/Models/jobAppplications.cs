﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models
{
    public class jobApplications
    {
        [Key]
        public int id { get; set; }
        public string slug { get; set; }
        public int applicantId { get; set; }
        public string jobSlug { get; set; }
        public DateTime dateFiled { get; set; }
        public string branch { get; set; }
        public string department { get; set; }
        public string status { get; set; }
        public string remarks { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }
}
