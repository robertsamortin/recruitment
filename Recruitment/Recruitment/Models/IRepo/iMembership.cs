﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.IRepo
{
    public interface iMembership
    {
        void insertMembership(appMembership data);
        void updateMembership(appMembership data);
        void deleteMembership(int id);

        List<appMembershipViewModel> getMembership(int id);

        appMembershipViewModel getSingleMembership(int id);

        
    }
}
