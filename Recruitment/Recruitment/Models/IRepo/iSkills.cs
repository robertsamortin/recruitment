﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.IRepo
{
    public interface iSkills
    {
        void insert(skills data);
        void update(skills data);
        void delete(int id);

        List<skills> getAll();

        skills getOne(int id);
    }
}
