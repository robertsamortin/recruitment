﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.IRepo
{
    public interface iBranch
    {
        void insert(branch data);
        void update(branch data);
        void delete(int id);

        List<branch> getAll();

        branch getOne(int id);
    }
}
