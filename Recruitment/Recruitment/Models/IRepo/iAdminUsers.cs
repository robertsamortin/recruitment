﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.IRepo
{
    public interface iAdminUsers
    {
        void insert(systemUsers data);
        void update(systemUsers data);
        void delete(int id);

        List<systemUsers> getAll();

        systemUsers getOne(int id);
        List<string> emailAddress(string branch);
        string email(string position);
    }
}
