﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.IRepo
{
    public interface iEducation
    {
        void insertEducation(appEducation data);
        void updateEducation(appEducation data);
        void deleteEducation(int id);

        List<appEducationViewModel> getEducBackground(int id);

        appEducationViewModel getSingleEducBackground(int id);

        
    }
}
