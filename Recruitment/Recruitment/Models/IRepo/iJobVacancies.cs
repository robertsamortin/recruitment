﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.IRepo
{
    public interface iJobVacancies
    {
        void insert(jobVacancies data);
        void update(jobVacancies data);
        void delete(int id);

        List<jobVacancies> getAll();
        List<jobVacancies> getAllActive();

        jobVacancies getOne(int id);

        jobVacancies getOneBySlug(string slug);

        List<JobVacanciesViewModel> getAllViewModel();
        List<JobVacanciesViewModelApplicant> getAllActiveViewModel();
        JobVacanciesViewModelApplicant getOneBySlugApplicant(string slug);
    }
}
