﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.IRepo
{
    public interface iJobApplications
    {
        void insert(jobApplications data);
        void update(jobApplications data);
        void delete(int id);

        List<jobApplications> getAll();
        List<jobApplications> getAllActive();

        jobApplications getOne(int id);

        jobApplications getOneBySlug(string slug);
        jobApplications getOneByJobSlug(string jobSlug, int applicantId);
        List<jobApplications> getOneByApplicantId(int applicantId);
        jobApplications getOneByJobSlugBranches(string branchSlug, string jobSlug, int applicantId);

        List<JobApplicationViewModel> GetAllAdmin(string branch, string department);
        List<JobApplicationViewModel> GetAllApplicant(int id);
        JobApplicationViewModel getOneBySlugApplicant(string jobSlug, int applicantId);
        void updateStatus(string status, int id);

        jobApplications getOneByJobSlugBranch(string jobSlug, int applicantId, string branch);
        void updateBranch(jobApplications data);

        statuses GetStatuses(int id);

        void insertStatus(statuses data);

        List<statuses> GetStat(int jobId, int applicantId);
        void updatePassed(int jobId, int applicantId, bool passed, string status);

        List<StatusesViewModel> GetApplicantStatus(int id);

        void insertDocs(documents data);
        List<documentsViewModel> getAllDocs(int applicantId);
        List<documentsViewModel> getAllDocsSameDocNumber(int applicantId, string docNumber);

        statuses GetLastStat(int jobId, int applicantId);
        void deleteDocs(int applicantId, string docNumber, string filename);
        void updateDocs(int applicantID, string docNumber, string comments);
    }
}
