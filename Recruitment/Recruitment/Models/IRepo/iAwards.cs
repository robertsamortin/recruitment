﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.IRepo
{
    public interface iAwards
    {
        void insertAwards(appAwards data);
        void updateAwards(appAwards data);
        void deleteAwards(int id);

        List<appAwardsViewModel> getAwards(int id);

        appAwardsViewModel getSingleAwards(int id);

        
    }
}
