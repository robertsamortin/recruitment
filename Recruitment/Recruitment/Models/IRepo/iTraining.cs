﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.IRepo
{
    public interface iTraining
    {
        void insertTraining(appTraining data);
        void updateTraining(appTraining data);
        void deleteTraining(int id);

        List<appTrainingViewModel> getTraining(int id);

        appTrainingViewModel getSingleTraining(int id);

        
    }
}
