﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.IRepo
{
    public interface iEmployment
    {
        void insertEmployment(appEmployment data);
        void updateEmployment(appEmployment data);
        void deleteEmployment(int id);

        List<appEmploymentViewModel> getEmployBackground(int id);

        appEmploymentViewModel getSingleEmployBackground(int id);

        
    }
}
