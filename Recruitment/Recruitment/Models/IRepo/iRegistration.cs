﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.IRepo
{
    public interface iRegistration
    {
        void add(systemUsers p);
        bool checkAlreadyRegistered(string email);
        void confirmEmail(string token, string email);
    }
}
