﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.IRepo
{
    public interface iFamily
    {
        void insertFamily(appFamilyBackground data);
        void updateFamily(appFamilyBackground data);
        void deleteFamily(int id);

        List<appFamilyViewModel> getFamBackground(int id);

        appFamilyViewModel getSingleFamBackground(int id);

        
    }
}
