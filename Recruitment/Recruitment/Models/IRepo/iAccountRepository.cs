﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models.IRepo
{
    public interface iAccountRepository
    {
        systemUsers login(string email);

        applicationViewModel getInfoDetails(int id);

        adminPersonalViewModel getInfoAdminDetails(int id);

        void updatePData(applicationViewModel data, int id);

        void updateAdminData(adminPersonalViewModel data, int id);

        void changePassword(string password, int id);

        void resetPassword(string password, string email);

        bool checkPersonalData(int applicantId);

        List<applicationViewModel> getSkills(string searchString);

        applicationViewModel getInfoDetailsReport(int applicantId);

        string retrieveToken(string email);

        
    }
}
