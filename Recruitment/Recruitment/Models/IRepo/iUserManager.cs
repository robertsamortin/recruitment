﻿using Microsoft.AspNetCore.Http;

namespace Recruitment.Models.IRepo
{
    public interface iUserManager
    {
        systemUsers Validate(string username);
        void SignIn(HttpContext httpContext, systemUsers user, bool isPersistent = false);
        void SignOut(HttpContext httpContext);
        string GetCurrentUserId(HttpContext httpContext);
        systemUsers GetCurrentUser(HttpContext httpContext);
    }
}
