﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models
{
    public class appTrainingViewModel
    {
        public int id { get; set; }
        public int applicantId { get; set; }
        public string title { get; set; }
        public string dateFrom { get; set; }
        public string dateTo { get; set; }
        public string venue { get; set; }
        public int noHours { get; set; }
        public string conductedBy { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }
}
