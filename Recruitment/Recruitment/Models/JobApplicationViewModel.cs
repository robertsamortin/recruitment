﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models
{
    public class JobApplicationViewModel
    {
        public int id { get; set; }
        public int applicantId { get; set; }
        public string slug { get; set; }
        public string picture { get; set; }
        public string resume { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string jobSlug { get; set; }
        public string dateApplied { get; set; }
        public string dateHired { get; set; }
        public string jobPosition { get; set; }
        public string jobSummary { get; set; }
        public string jobQualifications { get; set; }
        public string status { get; set; }
        public string remarks { get; set; }
        public string branches { get; set; }
        public string department { get; set; }
        public string currentStatus { get; set; }
        public bool passed { get; set; }
    }
}
