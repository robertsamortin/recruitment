﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Recruitment.Models
{
    public class appPersonalData
    {
        [Key]
        public int id { get; set; }
        public int applicantId { get; set; }
        public string referredBy { get; set; }
        public string aboutMe { get; set; }
        public DateTime dateFiled { get; set; }
        public string facebook { get; set; }
        public string instagram { get; set; }
        public string twitter { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public double desiredSalary { get; set; }
        public bool negotiable { get; set; }
        public string firstChoice { get; set; }
        public string secondChoice { get; set; }
        public string thirdChoice { get; set; }
        public string nickName { get; set; }
        public string sex { get; set; }
        public string bloodType { get; set; }
        public string picture { get; set; }
        public string resume { get; set; }
        public string birthPlace { get; set; }
        public string citizenship { get; set; }
        public string weight { get; set; }
        public string height { get; set; }
        public string civilStatus { get; set; }
        public string religiousAffiliation { get; set; }
        public string sss { get; set; }
        public string tin { get; set; }
        public string taxStatus { get; set; }
        public string pagibig { get; set; }
        public string philhealth { get; set; }
        public string landline { get; set; }
        public string mobile { get; set; }
        public string presentAddress { get; set; }
        public string permanentAddress { get; set; }
        public string permanentAddressContact { get; set; }
        public string permanentAddressZipCode { get; set; }
        public string otherAddress { get; set; }
        public string otherAddressContact { get; set; }
        public string otherAddressZipCode { get; set; }
        public int yearsStay { get; set; }
        public bool own { get; set; }
        public bool rent { get; set; }
        public double rentAmount { get; set; }
        public string liveWith { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public string location { get; set; }
    }

    public class appFamilyBackground
    {
        [Key]
        public int id { get; set; }
        public int applicantId { get; set; }
        public string name { get; set; }
        public DateTime birthDate { get; set; }
        public string relationship { get; set; }
        public string occupation { get; set; }
        public string school { get; set; }
        public string course { get; set; }
        public string contact { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }

    public class appEducation
    {
        [Key]
        public int id { get; set; }
        public int applicantId { get; set; }
        public string elementary { get; set; }
        public string elemAddress { get; set; }
        public string elemDegree { get; set; }
        public int elemFrom { get; set; }
        public int elemTo { get; set; }
        public string elemAwards { get; set; }
        public string highSchool { get; set; }
        public string highAddress { get; set; }
        public string highDegree { get; set; }
        public int highFrom { get; set; }
        public int highTo { get; set; }
        public string highAwards { get; set; }
        public string college { get; set; }
        public string collAddress { get; set; }
        public string collDegree { get; set; }
        public string collCourse { get; set; }
        public int collFrom { get; set; }
        public int collTo { get; set; }
        public string collAwards { get; set; }
        public string others { get; set; }
        public string othAddress { get; set; }
        public string othDegree { get; set; }
        public int othFrom { get; set; }
        public int othTo { get; set; }
        public string othAwards { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }

    public class appEmployment
    {
        [Key]
        public int id { get; set; }
        public int applicantId { get; set; }
        public string employer { get; set; }
        public string position { get; set; }
        public string department { get; set; }
        public string address { get; set; }
        public string jobFunctions { get; set; }
        public string isName { get; set; }
        public string isPosition { get; set; }
        public string contact { get; set; }
        public string appointmentStatus { get; set; }
        public DateTime empFrom { get; set; }
        public DateTime empTo { get; set; }
        public bool empPresent { get; set; }
        public string startSalary { get; set; }
        public double endSalary { get; set; }
        public string reasonLeaving { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }

    public class appTraining
    {
        [Key]
        public int id { get; set; }
        public int applicantId { get; set; }
        public string title { get; set; }
        public DateTime dateFrom { get; set; }
        public DateTime dateTo { get; set; }
        public int noHours { get; set; }
        public string venue { get; set; }
        public string conductedBy { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }

    public class appMembership
    {
        [Key]
        public int id { get; set; }
        public int applicantId { get; set; }
        public string organizationName { get; set; }
        public DateTime dateFrom { get; set; }
        public DateTime dateTo { get; set; }
        public bool memPresent { get; set; }
        public string position { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }

    public class appAwards
    {
        [Key]
        public int id { get; set; }
        public int applicantId { get; set; }
        public string awardName { get; set; }
        public DateTime dateReceived { get; set; }
        public DateTime validUntil { get; set; }
        public string licenseNumber { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }

    public class appOtherInformation
    {
        [Key]
        public int id { get; set; }
        public int applicantId { get; set; }
        public string slanguage1 { get; set; }
        public string slanguage2 { get; set; }
        public string slanguage3 { get; set; }
        public string wlanguage1 { get; set; }
        public string wlanguage2 { get; set; }
        public string wlanguage3 { get; set; }
        public string officeMachine { get; set; }
        public string hobbies { get; set; }
        public string vehicles { get; set; }
        public bool workFromHome { get; set; }
    }

    public class appDeclaration
    {
        [Key]
        public int id { get; set; }
        public int applicantId { get; set; }
        public bool kmbiRelative { get; set; }
        public string kmbiRelativePosition { get; set; }
        public bool cCase { get; set; }
        public string cCaseDetail { get; set; }
        public bool discharged { get; set; }
        public string dischargedDetails { get; set; }
        public bool illness { get; set; }
        public string illnessDetails { get; set; }
        public string incaseEmergency { get; set; }
        public string incaseContact { get; set; }
        public string incaseRelationship { get; set; }
    }

    public class appReferences
    {
        [Key]
        public int id { get; set; }
        public int applicantId { get; set; }
        public string ref1Name { get; set; }
        public string ref1Occupation { get; set; }
        public string ref1Contact { get; set; }
        public string ref2Name { get; set; }
        public string ref2Occupation { get; set; }
        public string ref2Contact { get; set; }
        public string ref3Name { get; set; }
        public string ref3Occupation { get; set; }
        public string ref3Contact { get; set; }
    }
}
