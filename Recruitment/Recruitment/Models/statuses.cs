﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models
{
    public class statuses
    {
        [Key]
        public int id { get; set; }
        public int jobId { get; set; }
        public int applicantId { get; set; }
        public string jobSlug { get; set; }
        public string status { get; set; }
        public DateTime inclusiveDate { get; set; }
        public bool currentStatus { get; set; }
        public bool passed { get; set; }
        public bool sentInvitation { get; set; }
        public bool apointmentDone { get; set; }
        public DateTime createdAt { get; set; }
    }
}
