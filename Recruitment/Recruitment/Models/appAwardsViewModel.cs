﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models
{
    public class appAwardsViewModel
    {
        public int id { get; set; }
        public int applicantId { get; set; }
        public string awardName { get; set; }
        public string dateReceived { get; set; }
        public string validUntil { get; set; }
        public string licenseNumber { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }
}
