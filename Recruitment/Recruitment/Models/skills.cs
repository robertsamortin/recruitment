﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models
{
    public class skills
    {
        [Key]
        public int id { get; set; }
        public string description { get; set; }
        public bool status { get; set; }
    }
}
