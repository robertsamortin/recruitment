﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models
{
    public class appFamilyViewModel
    {
        public int id { get; set; }
        public int applicantId { get; set; }
        public string name { get; set; }
        public string birthDate { get; set; }
        public int age { get; set; }
        public string relationship { get; set; }
        public string occupation { get; set; }
        public string school { get; set; }
        public string course { get; set; }
        public string contact { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }
}
