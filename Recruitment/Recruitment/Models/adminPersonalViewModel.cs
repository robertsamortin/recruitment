﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models
{
    public class adminPersonalViewModel
    {
        public systemUsers systemUsers { get; set; }
        public string picture { get; set; }
        public IFormFile pictureFile { get; set; }
    }
}
