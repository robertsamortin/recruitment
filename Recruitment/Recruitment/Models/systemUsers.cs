﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment.Models
{
    public class systemUsers
    {
        [Key]
        public int id { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string position { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string token { get; set; }
        public string slug { get; set; }
        public bool isVerified { get; set; }
        public bool isAdmin { get; set; }
        public string branch { get; set; }
        public string department { get; set; }
        public string referenceNo { get; set; }
        public string contactNo { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }
}
