﻿//CHANGE PASSWORD
$("#reveal").on('click', function () {
    $(this).find('.fa').toggleClass('fa-eye-slash fa-eye');
    var $pwd = $("#cNewPassword");
    if ($pwd.attr('type') === 'password') {
        $pwd.attr('type', 'text');
    } else {
        $pwd.attr('type', 'password');
    }
});
$("#cReveal").on('click', function () {
    $(this).find('.fa').toggleClass('fa-eye-slash fa-eye');
    var $pwd = $("#cConfirmPassword");
    if ($pwd.attr('type') === 'password') {
        $pwd.attr('type', 'text');
    } else {
        $pwd.attr('type', 'password');
    }
});

$('#saveNewPassword').click(function () {
    if ($("#cNewPassword").val().trim() == "") {
        swal("Error", "New Password is required", "error");
    }
    else if ($("#cConfirmPassword").val().trim() == "" || $("#cConfirmPassword").val().trim() != $("#cNewPassword").val().trim()) {
        swal("Error", "Please confirm your new password", "error");
    }
    else {
        console.log($("#cNewPassword").val());
        console.log("@id");
        swal({
            title: "Are you sure you want to change your password?",
            text: "Confirmation",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false
        }, function () {
            swal({
                title: 'Please Wait..!',
                text: 'Saving Record',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: false,
                showConfirmButton: false,
                onOpen: () => {
                    swal.showLoading()
                }
            })
            $.ajax({
                type: "POST",
                url: "@Url.Action('ChangePassword', 'Account')",
                data: {
                    password: $("#cNewPassword").val(),
                    id: "@id"
                },
                success: function (data) {
                    swal({
                        title: "Data successfully saved. ",
                        text: "It will autmatically logout and please use your new password to login",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    }, function () {
                        $('#logoutForm').submit();
                    });

                }
            })
        });
    }
});