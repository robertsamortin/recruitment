﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Recruitment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitment
{
    public class SQLCon : IdentityDbContext<IdentityUser>
    {
        public SQLCon(DbContextOptions<SQLCon> options) : base(options)
        {

        }
        public DbSet<appAwards> appAwards { get; set; }
        public DbSet<appDeclaration> appDeclaration { get; set; }
        public DbSet<appEducation> appEducation { get; set; }
        public DbSet<appEmployment> appEmployment { get; set; }
        public DbSet<appFamilyBackground> appFamilyBackground { get; set; }
        public DbSet<appTraining> appTraining { get; set; }
        public DbSet<appMembership> appMembership { get; set; }
        public DbSet<appOtherInformation> appOtherInformation { get; set; }
        public DbSet<appPersonalData> appPersonalData { get; set; }
        public DbSet<appReferences> appReferences { get; set; }
        public DbSet<systemUsers> systemUsers { get; set; }
        public DbSet<jobVacancies> jobVacancies { get; set; }
        public DbSet<branch> branch { get; set; }
        public DbSet<jobApplications> jobApplications { get; set; }
        public DbSet<skills> skills { get; set; }
        public DbSet<statuses> statuses { get; set; }
        public DbSet<documents> documents { get; set; }
    }
}
