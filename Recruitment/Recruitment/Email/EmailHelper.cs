﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Recruitment.Email
{
    public class EmailHelper
    {
        //public bool SendEmail(string userEmail, string confirmationLink)
        //{
        //    MailMessage mailMessage = new MailMessage();
        //    mailMessage.From = new MailAddress("no-reply@kmbi.org.ph");
        //    mailMessage.To.Add(new MailAddress(userEmail));

        //    mailMessage.Subject = "Confirm your email";
        //    mailMessage.IsBodyHtml = true;
        //    mailMessage.Body = confirmationLink;

        //    using (SmtpClient smtp = new SmtpClient())
        //    {
        //        smtp.Host = "smtp.office365.com";
        //        smtp.EnableSsl = true;
        //        NetworkCredential NetworkCred = new NetworkCredential("no-reply@kmbi.org.ph", "_vl;-J99@a");
        //        smtp.UseDefaultCredentials = true;
        //        smtp.Credentials = NetworkCred;
        //        smtp.Port = 587;
        //        smtp.Send(mailMessage);

        //    }
        //    return true;
        //    //try
        //    //{
        //    //    smtp.Send(mailMessage);
        //    //    return true;
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    // log exception
        //    //}
        //    //return false;
        //}

        public static bool SendEmail(string to, string subject, string body, string cc)
        {
            ///EMAIL IS
            string from = "hcd-staffing@kmbi.org.ph";
            using (MailMessage mm = new MailMessage(from, to))
            {
                mm.Subject = subject;
                string[] CCId = cc.Split(',');
                foreach (string CCEmail in CCId)
                {
                    mm.CC.Add(new MailAddress(CCEmail)); //Adding Multiple CC email Id  
                }

                var strTargetString = body + "\n\n\n" +
                    "** This Electronic Mail is system-generated. Please do not reply. **";

                string NewString = strTargetString;
                mm.Body = NewString;
                
                //if (model.fromAttachment.Length > 0)
                //{
                //    string fileName = Path.GetFileName(model.fromAttachment.FileName);
                //    mm.Attachments.Add(new Attachment(model.fromAttachment.OpenReadStream(), fileName));
                //}
                mm.IsBodyHtml = true;
                using (SmtpClient smtp = new SmtpClient())
                {
                    smtp.Host = "smtp.office365.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential(from, "Low53925");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }
                return true;
            }
        }

        public bool SendEmailPasswordReset(string userEmail, string link)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress("care@yogihosting.com");
            mailMessage.To.Add(new MailAddress(userEmail));

            mailMessage.Subject = "Password Reset";
            mailMessage.IsBodyHtml = true;
            mailMessage.Body = link;

            SmtpClient client = new SmtpClient();
            client.Credentials = new System.Net.NetworkCredential("info@rainpuddleslabradoodles.com", "Mydoodles!");
            client.Host = "smtpout.secureserver.net";
            client.Port = 80;

            try
            {
                client.Send(mailMessage);
                return true;
            }
            catch (Exception ex)
            {
                // log exception
            }
            return false;
        }

        public bool SendEmailTwoFactorCode(string userEmail, string code)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress("care@yogihosting.com");
            mailMessage.To.Add(new MailAddress(userEmail));

            mailMessage.Subject = "Two Factor Code";
            mailMessage.IsBodyHtml = true;
            mailMessage.Body = code;

            SmtpClient client = new SmtpClient();
            client.Credentials = new System.Net.NetworkCredential("info@rainpuddleslabradoodles.com", "Mydoodles!");
            client.Host = "smtpout.secureserver.net";
            client.Port = 80;

            try
            {
                client.Send(mailMessage);
                return true;
            }
            catch (Exception ex)
            {
                // log exception
            }
            return false;
        }
    }
}
